/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.Stream;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.papyrus.uml.domain.services.drop.diagrams.DeploymentInternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DeploymentSpecification;
import org.eclipse.uml2.uml.Device;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ExecutionEnvironment;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Node;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLPackage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Test class for
 * {@link DeploymentInternalSourceToRepresentationDropBehaviorProvider}.
 *
 * @author <a href="mailto:florian.barbin@obeo.fr">Florian Barbin</a>
 */
public class DeploymentInternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    private DeploymentInternalSourceToRepresentationDropBehaviorProvider deploymentInternalSourceToRepresentationDropBehaviorProvider;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        this.deploymentInternalSourceToRepresentationDropBehaviorProvider = new DeploymentInternalSourceToRepresentationDropBehaviorProvider();
    }

    @ParameterizedTest
    @MethodSource("getDragAndDropParameters")
    public void testElementDrop(Class<? extends Element> elementTypeToDrop, Class<? extends Element> newContainerType,
            EReference expectedNewContainingFeature, boolean isDoneExpected) {
        Package pck = this.create(Package.class);
        Element elementToDrop = this.createIn(elementTypeToDrop, pck);
        Element newContainer = this.create(newContainerType);

        Status status = this.deploymentInternalSourceToRepresentationDropBehaviorProvider.drop(elementToDrop, pck,
                newContainer, this.getCrossRef(), this.getEditableChecker());
        if (isDoneExpected) {
            assertEquals(State.DONE, status.getState());
            assertTrue(pck.eContents().isEmpty());
            assertTrue(newContainer.eContents().contains(elementToDrop));
            assertEquals(expectedNewContainingFeature, elementToDrop.eContainingFeature());
        } else {
            assertEquals(State.FAILED, status.getState());
            assertTrue(pck.eContents().contains(elementToDrop));
            assertTrue(newContainer.eContents().isEmpty());
        }
    }

    public static Stream<Arguments> getDragAndDropParameters() {
        // @formatter:off
        return Stream.of(
                Arguments.of(Artifact.class, Artifact.class, UMLPackage.eINSTANCE.getArtifact_NestedArtifact(), true),
                Arguments.of(Artifact.class, ExecutionEnvironment.class, UMLPackage.eINSTANCE.getClass_NestedClassifier(), true),
                Arguments.of(Artifact.class, Model.class, UMLPackage.eINSTANCE.getPackage_PackagedElement(), true),
                Arguments.of(Artifact.class, Node.class, UMLPackage.eINSTANCE.getClass_NestedClassifier(), true),
                Arguments.of(Artifact.class, Package.class, UMLPackage.eINSTANCE.getPackage_PackagedElement(), true),
                Arguments.of(Artifact.class, Constraint.class, null, false),
                Arguments.of(Comment.class, Package.class, UMLPackage.eINSTANCE.getElement_OwnedComment(), true),
                Arguments.of(Comment.class, Model.class, UMLPackage.eINSTANCE.getElement_OwnedComment(), true),
                Arguments.of(Constraint.class, Package.class, UMLPackage.eINSTANCE.getNamespace_OwnedRule(), true),
                Arguments.of(Constraint.class, Model.class, UMLPackage.eINSTANCE.getNamespace_OwnedRule(), true),
                Arguments.of(Constraint.class, Constraint.class, null, false),
                Arguments.of(DeploymentSpecification.class, Artifact.class, UMLPackage.eINSTANCE.getArtifact_NestedArtifact(), true),
                Arguments.of(DeploymentSpecification.class, Device.class, UMLPackage.eINSTANCE.getClass_NestedClassifier(), true),
                Arguments.of(DeploymentSpecification.class, ExecutionEnvironment.class, UMLPackage.eINSTANCE.getClass_NestedClassifier(), true),
                Arguments.of(DeploymentSpecification.class, Model.class, UMLPackage.eINSTANCE.getPackage_PackagedElement(), true),
                Arguments.of(DeploymentSpecification.class, Node.class, UMLPackage.eINSTANCE.getClass_NestedClassifier(), true),
                Arguments.of(DeploymentSpecification.class, Package.class, UMLPackage.eINSTANCE.getPackage_PackagedElement(), true),
                Arguments.of(DeploymentSpecification.class, Constraint.class, null, false),
                Arguments.of(Device.class, Device.class, UMLPackage.eINSTANCE.getNode_NestedNode(), true),
                Arguments.of(Device.class, Model.class, UMLPackage.eINSTANCE.getPackage_PackagedElement(), true),
                Arguments.of(Device.class, Node.class, UMLPackage.eINSTANCE.getNode_NestedNode(), true),
                Arguments.of(Device.class, Package.class, UMLPackage.eINSTANCE.getPackage_PackagedElement(), true),
                Arguments.of(Device.class, Constraint.class, null, false),
                Arguments.of(ExecutionEnvironment.class, Device.class, UMLPackage.eINSTANCE.getNode_NestedNode(), true),
                Arguments.of(ExecutionEnvironment.class, ExecutionEnvironment.class, UMLPackage.eINSTANCE.getNode_NestedNode(), true),
                Arguments.of(ExecutionEnvironment.class, Model.class, UMLPackage.eINSTANCE.getPackage_PackagedElement(), true),
                Arguments.of(ExecutionEnvironment.class, Node.class, UMLPackage.eINSTANCE.getNode_NestedNode(), true),
                Arguments.of(ExecutionEnvironment.class, Package.class, UMLPackage.eINSTANCE.getPackage_PackagedElement(), true),
                Arguments.of(ExecutionEnvironment.class, Constraint.class, null, false),
                Arguments.of(Model.class, Model.class, UMLPackage.eINSTANCE.getPackage_PackagedElement(), true),
                Arguments.of(Model.class, Package.class, UMLPackage.eINSTANCE.getPackage_PackagedElement(), true),
                Arguments.of(Model.class, Constraint.class, null, false),
                Arguments.of(Node.class, Device.class, UMLPackage.eINSTANCE.getNode_NestedNode(), true),
                Arguments.of(Node.class, Model.class, UMLPackage.eINSTANCE.getPackage_PackagedElement(), true),
                Arguments.of(Node.class, Node.class, UMLPackage.eINSTANCE.getNode_NestedNode(), true),
                Arguments.of(Node.class, Package.class, UMLPackage.eINSTANCE.getPackage_PackagedElement(), true),
                Arguments.of(Node.class, Constraint.class, null, false),
                Arguments.of(Package.class, Model.class, UMLPackage.eINSTANCE.getPackage_PackagedElement(), true),
                Arguments.of(Package.class, Package.class, UMLPackage.eINSTANCE.getPackage_PackagedElement(), true),
                Arguments.of(Package.class, Constraint.class, null, false)
                );
        // @formatter:on
    }
}
