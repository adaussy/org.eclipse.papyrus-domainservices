/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.DeploymentExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.drop.diagrams.DeploymentInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Clause;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DeploymentSpecification;
import org.eclipse.uml2.uml.Device;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ExecutionEnvironment;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Node;
import org.eclipse.uml2.uml.Package;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Test class for {@link DeploymentExternalSourceToRepresentationDropChecker}.
 *
 * @author <a href="mailto:florian.barbin@obeo.fr">Florian Barbin</a>
 */
public class DeploymentExternalAndInternalSourceToRepresentationDropCheckerTest extends AbstractUMLTest {

    private DeploymentExternalSourceToRepresentationDropChecker deploymentExternalSourceToRepresentationDropChecker;
    private DeploymentInternalSourceToRepresentationDropChecker deploymentInternalSourceToRepresentationDropChecker;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        this.deploymentExternalSourceToRepresentationDropChecker = new DeploymentExternalSourceToRepresentationDropChecker();
        this.deploymentInternalSourceToRepresentationDropChecker = new DeploymentInternalSourceToRepresentationDropChecker();
    }

    @ParameterizedTest
    @MethodSource("getCanDragAndDropParameters")
    public void testExternalCanDragAndDrop(Class<? extends Element> elementToDropClazz,
            Class<? extends Element> semanticContainerClazz, boolean expectedCheckStatus) {
        Element elementToDrop = this.create(elementToDropClazz);
        Element semanticContainer = this.create(semanticContainerClazz);

        CheckStatus canDragAndDropStatus = this.deploymentExternalSourceToRepresentationDropChecker
                .canDragAndDrop(elementToDrop, semanticContainer);
        assertEquals(expectedCheckStatus, canDragAndDropStatus.isValid());
    }

    @ParameterizedTest
    @MethodSource("getCanDragAndDropParameters")
    public void testInternalCanDragAndDrop(Class<? extends Element> elementToDropClazz,
            Class<? extends Element> semanticContainerClazz, boolean expectedCheckStatus) {
        Element elementToDrop = this.create(elementToDropClazz);
        Element semanticContainer = this.create(semanticContainerClazz);

        CheckStatus canDragAndDropStatus = this.deploymentInternalSourceToRepresentationDropChecker
                .canDragAndDrop(elementToDrop, semanticContainer);
        assertEquals(expectedCheckStatus, canDragAndDropStatus.isValid());
    }

    public static Stream<Arguments> getCanDragAndDropParameters() {
        // @formatter:off
        return Stream.of(
                Arguments.of(Artifact.class, Artifact.class, true),
                Arguments.of(Artifact.class, Comment.class, false),
                Arguments.of(Artifact.class, Constraint.class, false),
                Arguments.of(Artifact.class, DeploymentSpecification.class, false),
                Arguments.of(Artifact.class, Device.class, false),
                Arguments.of(Artifact.class, ExecutionEnvironment.class, true),
                Arguments.of(Artifact.class, Model.class, true),
                Arguments.of(Artifact.class, Node.class, true),
                Arguments.of(Artifact.class, Package.class, true),
                Arguments.of(Comment.class, Artifact.class, false),
                Arguments.of(Comment.class, Comment.class, false),
                Arguments.of(Comment.class, Constraint.class, false),
                Arguments.of(Comment.class, DeploymentSpecification.class, false),
                Arguments.of(Comment.class, Device.class, false),
                Arguments.of(Comment.class, ExecutionEnvironment.class, false),
                Arguments.of(Comment.class, Model.class, true),
                Arguments.of(Comment.class, Node.class, false),
                Arguments.of(Comment.class, Package.class, true),
                Arguments.of(Constraint.class, Artifact.class, false),
                Arguments.of(Constraint.class, Comment.class, false),
                Arguments.of(Constraint.class, Constraint.class, false),
                Arguments.of(Constraint.class, DeploymentSpecification.class, false),
                Arguments.of(Constraint.class, Device.class, false),
                Arguments.of(Constraint.class, ExecutionEnvironment.class, false),
                Arguments.of(Constraint.class, Model.class, true),
                Arguments.of(Constraint.class, Node.class, false),
                Arguments.of(Constraint.class, Package.class, true),
                Arguments.of(DeploymentSpecification.class, Artifact.class, true),
                Arguments.of(DeploymentSpecification.class, Comment.class, false),
                Arguments.of(DeploymentSpecification.class, Constraint.class, false),
                Arguments.of(DeploymentSpecification.class, DeploymentSpecification.class, false),
                Arguments.of(DeploymentSpecification.class, Device.class, true),
                Arguments.of(DeploymentSpecification.class, ExecutionEnvironment.class, true),
                Arguments.of(DeploymentSpecification.class, Model.class, true),
                Arguments.of(DeploymentSpecification.class, Node.class, true),
                Arguments.of(DeploymentSpecification.class, Package.class, true),
                Arguments.of(Device.class, Artifact.class, false),
                Arguments.of(Device.class, Comment.class, false),
                Arguments.of(Device.class, Constraint.class, false),
                Arguments.of(Device.class, DeploymentSpecification.class, false),
                Arguments.of(Device.class, Device.class, true),
                Arguments.of(Device.class, ExecutionEnvironment.class, false),
                Arguments.of(Device.class, Model.class, true),
                Arguments.of(Device.class, Node.class, true),
                Arguments.of(Device.class, Package.class, true),
                Arguments.of(ExecutionEnvironment.class, Artifact.class, false),
                Arguments.of(ExecutionEnvironment.class, Comment.class, false),
                Arguments.of(ExecutionEnvironment.class, Constraint.class, false),
                Arguments.of(ExecutionEnvironment.class, DeploymentSpecification.class, false),
                Arguments.of(ExecutionEnvironment.class, Device.class, true),
                Arguments.of(ExecutionEnvironment.class, ExecutionEnvironment.class, true),
                Arguments.of(ExecutionEnvironment.class, Model.class, true),
                Arguments.of(ExecutionEnvironment.class, Node.class, true),
                Arguments.of(ExecutionEnvironment.class, Package.class, true),
                Arguments.of(Model.class, Artifact.class, false),
                Arguments.of(Model.class, Comment.class, false),
                Arguments.of(Model.class, Constraint.class, false),
                Arguments.of(Model.class, DeploymentSpecification.class, false),
                Arguments.of(Model.class, Device.class, false),
                Arguments.of(Model.class, ExecutionEnvironment.class, false),
                Arguments.of(Model.class, Model.class, true),
                Arguments.of(Model.class, Node.class, false),
                Arguments.of(Model.class, Package.class, true),
                Arguments.of(Node.class, Artifact.class, false),
                Arguments.of(Node.class, Comment.class, false),
                Arguments.of(Node.class, Constraint.class, false),
                Arguments.of(Node.class, DeploymentSpecification.class, false),
                Arguments.of(Node.class, Device.class, true),
                Arguments.of(Node.class, ExecutionEnvironment.class, false),
                Arguments.of(Node.class, Model.class, true),
                Arguments.of(Node.class, Node.class, true),
                Arguments.of(Node.class, Package.class, true),
                Arguments.of(Package.class, Artifact.class, false),
                Arguments.of(Package.class, Comment.class, false),
                Arguments.of(Package.class, Constraint.class, false),
                Arguments.of(Package.class, DeploymentSpecification.class, false),
                Arguments.of(Package.class, Device.class, false),
                Arguments.of(Package.class, ExecutionEnvironment.class, false),
                Arguments.of(Package.class, Model.class, true),
                Arguments.of(Package.class, Node.class, false),
                Arguments.of(Package.class, Package.class, true),
                // Test default case
                Arguments.of(Clause.class, Package.class, false)
                );
        // @formatter:on
    }

}
