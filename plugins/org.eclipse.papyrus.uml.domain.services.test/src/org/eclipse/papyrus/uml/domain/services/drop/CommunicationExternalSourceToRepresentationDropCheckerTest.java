/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.CommunicationExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.DurationObservation;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.TimeObservation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for
 * {@link CommunicationExternalSourceToRepresentationDropChecker}.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 *
 */
public class CommunicationExternalSourceToRepresentationDropCheckerTest extends AbstractUMLTest {

    private CommunicationExternalSourceToRepresentationDropChecker communicationExternalSourceToRepresentationDropChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        this.communicationExternalSourceToRepresentationDropChecker = new CommunicationExternalSourceToRepresentationDropChecker();
    }

    /**
     * Test dropping a {@link Activity} on {@link Comment} => not authorized.
     */
    @Test
    public void testActivityDropOnComment() {
        Activity activity = this.create(Activity.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(activity, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Activity} on {@link Lifeline} => authorized.
     */
    @Test
    public void testActivityDropOnInteraction() {
        Activity activity = this.create(Activity.class);
        Interaction interaction = this.create(Interaction.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(activity, interaction);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Activity} on {@link Lifeline} => authorized.
     */
    @Test
    public void testActivityDropOnLifeline() {
        Activity activity = this.create(Activity.class);
        Lifeline lifeline = this.create(Lifeline.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(activity, lifeline);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment} on {@link Interaction} => authorized.
     */
    @Test
    public void testCommentDropOnInteraction() {
        Comment comment = this.create(Comment.class);
        Interaction interaction = this.create(Interaction.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(comment, interaction);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment} on {@link Lifeline} => not authorized.
     */
    @Test
    public void testCommentDropOnLifeline() {
        Comment comment = this.create(Comment.class);
        Lifeline lifeline = this.create(Lifeline.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(comment, lifeline);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Constraint} on {@link Interaction} => authorized.
     */
    @Test
    public void testConstraintDropOnInteraction() {
        Constraint constraint = this.create(Constraint.class);
        Interaction interaction = this.create(Interaction.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraint, interaction);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Constraint} on {@link Lifeline} => not authorized.
     */
    @Test
    public void testConstraintDropOnLifeline() {
        Constraint constraint = this.create(Constraint.class);
        Lifeline lifeline = this.create(Lifeline.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraint, lifeline);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Deployment} on {@link Lifeline} => not authorized.
     */
    @Test
    public void testDeploymentDropOnLifeline() {
        Deployment deployment = this.create(Deployment.class);
        Lifeline lifeline = this.create(Lifeline.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(deployment, lifeline);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link DurationObservation} on {@link Interaction} =>
     * authorized.
     */
    @Test
    public void testDurationObservationDropOnInteraction() {
        DurationObservation durationObservation = this.create(DurationObservation.class);
        Interaction interaction = this.create(Interaction.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(durationObservation, interaction);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link DurationObservation} on {@link Lifeline} => not
     * authorized.
     */
    @Test
    public void testDurationObservationDropOnLifeline() {
        DurationObservation durationObservation = this.create(DurationObservation.class);
        Lifeline lifeline = this.create(Lifeline.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(durationObservation, lifeline);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Lifeline} on {@link Interaction} => authorized.
     */
    @Test
    public void testLifelineDropOnInteraction() {
        Lifeline lifeline = this.create(Lifeline.class);
        Interaction interaction = this.create(Interaction.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(lifeline, interaction);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Lifeline} on {@link Lifeline} => not authorized.
     */
    @Test
    public void testLifelineDropOnLifeline() {
        Lifeline lifelineToDrop = this.create(Lifeline.class);
        Lifeline lifeline = this.create(Lifeline.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(lifelineToDrop, lifeline);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link Interaction} => not authorized.
     */
    @Test
    public void testPropertyDropOnInteraction() {
        Property property = this.create(Property.class);
        Interaction interaction = this.create(Interaction.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(property, interaction);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link Lifeline} => authorized.
     */
    @Test
    public void testPropertyDropOnLifeline() {
        Property property = this.create(Property.class);
        Lifeline lifeline = this.create(Lifeline.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(property, lifeline);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link TimeObservation} on {@link Interaction} => authorized.
     */
    @Test
    public void testTimeObservationDropOnInteraction() {
        TimeObservation timeObservation = this.create(TimeObservation.class);
        Interaction interaction = this.create(Interaction.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(timeObservation, interaction);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link TimeObservation} on {@link Lifeline} => not
     * authorized.
     */
    @Test
    public void testTimeObservationDropOnLifeline() {
        TimeObservation timeObservation = this.create(TimeObservation.class);
        Lifeline lifeline = this.create(Lifeline.class);

        CheckStatus canDragAndDropStatus = this.communicationExternalSourceToRepresentationDropChecker
                .canDragAndDrop(timeObservation, lifeline);
        assertFalse(canDragAndDropStatus.isValid());
    }

}
