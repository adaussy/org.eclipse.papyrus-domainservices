/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.edges;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.papyrus.uml.domain.services.utils.MockedViewQuerier;
import org.eclipse.papyrus.uml.domain.services.utils.VisualNode;
import org.eclipse.papyrus.uml.domain.services.utils.VisualTree;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.ComponentRealization;
import org.eclipse.uml2.uml.ControlFlow;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.ExpansionRegion;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.ForkNode;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.JoinNode;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Node;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Realization;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementDomainBasedEdgeContainerProvider}.
 *
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 *
 */
public class ElementDomainBasedEdgeContainerProviderTest extends AbstractUMLTest {

    /**
     * The container of DirectedRelationship is the most common package ancestor to
     * the source and the target is the source if its can <b>not</b> semantically
     * contains the edge.
     */
    @Test
    public void testAbstractionWithContainingCapableSource() {
        Package source = this.create(Package.class);
        Class target = this.create(Class.class);

        Abstraction abstraction = this.create(Abstraction.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        // In this case the most common ancestor
        EObject container = containerProvider.getContainer(source, target, abstraction, null, null, null);
        assertEquals(source, container);

    }

    /**
     * The container of DirectedRelationship is the source if its can semantically
     * contains the edge.
     */
    @Test
    public void testAbstractionWithNonCapableContainingSource() {

        Package pack = this.create(Package.class);
        Class source = this.createIn(Class.class, pack);
        Class target = this.createIn(Class.class, pack);

        Abstraction abstraction = this.create(Abstraction.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        // In this case the most common ancestor
        EObject container = containerProvider.getContainer(source, target, abstraction, null, null, null);
        assertEquals(pack, container);

    }

    /**
     * Test association between two {@link Class} of {@link Model} => container
     * should be the {@link Model}.
     */
    @Test
    public void testAssociationInModel() {
        Model model = this.create(Model.class);
        Class source = this.create(Class.class);
        Class target = this.create(Class.class);
        Association association = this.create(Association.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode rootNode = visualTree.addChildren(model);
        VisualNode visualClass1 = rootNode.addChildren(source);
        VisualNode visualClass2 = rootNode.addChildren(target);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(source, target, association, new MockedViewQuerier(rootNode),
                visualClass1, visualClass2);

        assertEquals(model, container);
    }

    /**
     * Test association between two {@link Class} of {@link Package} in
     * {@link Model} => container should be the {@link Model}.
     */
    @Test
    public void testAssociationInSubPackage() {
        Model model = this.create(Model.class);
        Package pack = this.create(Package.class);
        Class source = this.create(Class.class);
        Class target = this.create(Class.class);
        Association association = this.create(Association.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode rootNode = visualTree.addChildren(model);
        VisualNode packNode = rootNode.addChildren(pack);
        VisualNode visualClass1 = packNode.addChildren(source);
        VisualNode visualClass2 = packNode.addChildren(target);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(source, target, association, new MockedViewQuerier(rootNode),
                visualClass1, visualClass2);

        assertEquals(model, container);
    }

    /**
     * A ComponentRealization should be contained by its target.
     */
    @Test
    public void testComponentRealizationContainer() {
        Interface sourceInterface = this.create(Interface.class);
        Component targetComponent = this.create(Component.class);
        ComponentRealization componentRealization = this.create(ComponentRealization.class);
        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);
        EObject container = containerProvider.getContainer(sourceInterface, targetComponent, componentRealization, null,
                null, null);
        assertEquals(targetComponent, container);
    }

    @Test
    public void testControlFlowContainer() {
        this.checkActivityEdgeContainer(this.create(ControlFlow.class));
    }

    @Test
    public void testControlFlowContainerWithStructuredActivityNodeAsSourceContainerOnly() {
        this.checkActivityEdgeContainerWithStructuredActivityNodeAsSourceContainerOnly(this.create(ControlFlow.class));
    }

    @Test
    public void testControlFlowContainerWithStructuredActivityNodeContainer() {
        this.testActivityEdgeContainerWithStructuredActivityNodeContainer(this.create(ControlFlow.class));
    }

    /**
     * Test basic uses case for dependency creation (all element are editable) when
     * the source and the target have a common package ancestor.
     */
    @Test
    public void testDependencyDirectCommonAncerstor() {

        Package pack = this.create(Package.class);
        Class c1 = this.createIn(Class.class, pack);
        Class c2 = this.createIn(Class.class, pack);

        Dependency dependency = this.create(Dependency.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                IEditableChecker.TRUE);

        // Visual element are not used for this use case
        EObject container = containerProvider.getContainer(c1, c2, dependency, null, null, null);

        assertEquals(pack, container);
    }

    /**
     * Checks the different fallback cases for container computation.
     */
    @Test
    public void testDependencyFallbackCases() {
        Package pack1 = this.create(Package.class);
        Class c1 = this.createIn(Class.class, pack1);
        Package pack2 = this.create(Package.class);
        Class c2 = this.createIn(Class.class, pack2);

        // No common ancestor => source package

        Dependency dependency = this.create(Dependency.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(c1, c2, dependency, null, null, null);

        assertEquals(pack1, container);

        // If C1 is uneditable => C2

        containerProvider = new ElementDomainBasedEdgeContainerProvider(e -> pack1 != e);

        container = containerProvider.getContainer(c1, c2, dependency, null, null, null);

        assertEquals(pack2, container);

        // If bith C1 and C2 are uneditable then null

        containerProvider = new ElementDomainBasedEdgeContainerProvider(e -> pack1 != e && e != pack2);

        container = containerProvider.getContainer(c1, c2, dependency, null, null, null);

        assertNull(container);

        // Test that if the source is a CollaborationUse, the container is the
        // CollaborationUse

        CollaborationUse collaborationUse = this.create(CollaborationUse.class);
        c2.getCollaborationUses().add(collaborationUse);
        Dependency dependency2 = this.create(Dependency.class);
        EObject container2 = containerProvider.getContainer(collaborationUse, c1, dependency2, null, null, null);
        assertEquals(collaborationUse, container2);
    }

    /**
     * Test basic uses case for dependency creation (all element are editable) when
     * the source and the target have a common package ancestor.
     */
    @Test
    public void testDependencyNoDirectCommonAncestor() {

        Package pack = this.create(Package.class);
        Class c1 = this.createIn(Class.class, pack);
        Class c2 = this.createIn(Class.class, pack);
        Class c1c1 = this.createIn(Class.class, c1);
        Class c2c2 = this.createIn(Class.class, c2);

        Dependency dependency = this.create(Dependency.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        // Visual element are not used for this use case
        EObject container = containerProvider.getContainer(c1c1, c2c2, dependency, null, null, null);

        assertEquals(pack, container);
    }

    /**
     * Test that uneditable element can't be used as container.
     *
     */
    @Test
    public void testDependencyUneditableNotDirectCommonAncestor() {
        Package pack = this.create(Package.class);
        Class c1 = this.createIn(Class.class, pack);
        Class c2 = this.createIn(Class.class, pack);
        Class c1c1 = this.createIn(Class.class, c1);
        Class c2c2 = this.createIn(Class.class, c2);

        Dependency dependency = this.create(Dependency.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> e != pack);

        // Visual element are not used for this use case
        EObject container = containerProvider.getContainer(c1c1, c2c2, dependency, null, null, null);

        assertNull(container);
    }

    /**
     * The container of DirectedRelationship is the most common package ancestor to
     * the source and the target is the source if its can <b>not</b> semantically
     * contains the edge.
     */
    @Test
    public void testDependencyWithContainingCapableSource() {
        Package source = this.create(Package.class);
        Class target = this.create(Class.class);

        Dependency dependency = this.create(Dependency.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        // In this case the most common ancestor
        EObject container = containerProvider.getContainer(source, target, dependency, null, null, null);
        assertEquals(source, container);

    }

    /**
     * The container of DirectedRelationship is the source if its can semantically
     * contains the edge.
     */
    @Test
    public void testDependencyWithNonCapableContainingSource() {

        Package pack = this.create(Package.class);
        Class source = this.createIn(Class.class, pack);
        Class target = this.createIn(Class.class, pack);

        Dependency dependency = this.create(Dependency.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        // In this case the most common ancestor
        EObject container = containerProvider.getContainer(source, target, dependency, null, null, null);
        assertEquals(pack, container);

    }

    /**
     * A Deployment should be contained by its target.
     */
    @Test
    public void testDeployment() {
        Deployment deployment = this.create(Deployment.class);
        Artifact sourceArtifact = this.create(Artifact.class);
        Node targetNode = this.create(Node.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(sourceArtifact, targetNode, deployment, null, null, null);
        assertEquals(targetNode, container);
    }

    /**
     * Test the basic case for {@link Extend}.
     */
    @Test
    public void testExtend() {
        Extend extend = this.create(Extend.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(source, target, extend, null, null, null);

        assertEquals(source, container);
    }

    /**
     * Test the container provider for {@link Extension}.
     */
    @Test
    public void testExtension() {
        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                IEditableChecker.TRUE);
        Package package1 = this.create(Package.class);
        Stereotype stereotype = this.createIn(Stereotype.class, package1);
        Class class1 = this.create(Class.class);
        Extension extension = this.create(Extension.class);
        EObject container = containerProvider.getContainer(stereotype, class1, extension, new MockedViewQuerier(), null,
                null);
        assertEquals(package1, container);
    }

    /**
     * Test the container provider for {@link Extension} without container.
     */
    @Test
    public void testExtensionWithoutContainer() {
        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                IEditableChecker.TRUE);
        Stereotype stereotype = this.create(Stereotype.class);
        Stereotype stereotype2 = this.createIn(Stereotype.class, stereotype);
        Class class1 = this.create(Class.class);
        Extension extension = this.create(Extension.class);
        EObject container = containerProvider.getContainer(stereotype2, class1, extension, new MockedViewQuerier(),
                null, null);
        assertNull(container);
    }

    /**
     * Test the basic case for {@link Generalization}.
     */
    @Test
    public void testGeneralization() {
        Class source = this.create(Class.class);
        Class target = this.create(Class.class);
        Generalization generalization = this.create(Generalization.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(source, target, generalization, null, null, null);

        assertEquals(source, container);
    }

    /**
     * Test the basic case for {@link Include}.
     */
    @Test
    public void testInclude() {
        Include include = this.create(Include.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(source, target, include, null, null, null);

        assertEquals(source, container);
    }

    /**
     * An InterfaceRealization should be contained by its source.
     */
    @Test
    public void testInterfaceRealizationContainer() {
        Class sourceClass = this.create(Class.class);
        Interface targetInterface = this.create(Interface.class);
        InterfaceRealization interfaceRealization = this.create(InterfaceRealization.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(sourceClass, targetInterface, interfaceRealization, null,
                null, null);

        assertEquals(sourceClass, container);
    }

    /**
     * Test the basic case for {@link Message}.
     */
    @Test
    public void testMessage() {
        Message message = this.create(Message.class);
        Lifeline source = this.create(Lifeline.class);
        Lifeline target = this.create(Lifeline.class);
        Interaction interaction = this.create(Interaction.class);
        source.setInteraction(interaction);
        interaction.getLifelines().add(source);
        target.setInteraction(interaction);
        interaction.getLifelines().add(target);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(source, target, message, null, null, null);

        assertEquals(interaction, container);
    }

    @Test
    public void testObjectFlowContainer() {
        this.checkActivityEdgeContainer(this.create(ObjectFlow.class));
    }

    @Test
    public void testObjectFlowContainerWithStructuredActivityNodeAsSourceContainerOnly() {
        this.checkActivityEdgeContainerWithStructuredActivityNodeAsSourceContainerOnly(this.create(ObjectFlow.class));
    }

    @Test
    public void testObjectFlowContainerWithStructuredActivityNodeContainer() {
        this.testActivityEdgeContainerWithStructuredActivityNodeContainer(this.create(ObjectFlow.class));
    }

    /**
     * Test the basic case for {@link Realization}.
     */
    @Test
    public void testRealization() {
        Model model = this.create(Model.class);
        Class source = this.create(Class.class);
        Class target = this.create(Class.class);
        CollaborationUse collaborationUse = this.create(CollaborationUse.class);
        model.getPackagedElements().add(source);
        model.getPackagedElements().add(target);
        source.getCollaborationUses().add(collaborationUse);
        Realization realization = this.create(Realization.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(source, target, realization, null, null, null);

        assertEquals(model, container);

        // Test that if the source is a CollaborationUse, the container is the
        // CollaborationUse
        Realization realization2 = this.create(Realization.class);
        EObject container2 = containerProvider.getContainer(collaborationUse, target, realization2, null, null, null);
        assertEquals(collaborationUse, container2);
    }

    /**
     * Test the basic case for {@link Substitution}.
     */
    @Test
    public void testSubstitution() {
        Class source = this.create(Class.class);
        Class target = this.create(Class.class);
        Substitution substitution = this.create(Substitution.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(source, target, substitution, null, null, null);

        assertEquals(source, container);
    }

    @Test
    public void testTransition() {
        StateMachine stateMachine = this.create(StateMachine.class);
        Region regionSM = this.createIn(Region.class, stateMachine);
        State state1 = this.createIn(State.class, regionSM);
        Pseudostate entryPoint = this.createIn(Pseudostate.class, state1);
        Region region = this.createIn(Region.class, state1);
        Pseudostate fork = this.createIn(Pseudostate.class, region);

        Transition transition = this.create(Transition.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(entryPoint, fork, transition, null, null, null);
        assertEquals(regionSM, container);

        container = containerProvider.getContainer(fork, entryPoint, transition, null, null, null);
        assertEquals(region, container);
    }

    private void checkActivityEdgeContainer(ActivityEdge activityEdge) {
        Activity activity = this.create(Activity.class);
        Activity subActivity = this.createIn(Activity.class, activity);
        ActivityPartition activityPartition = this.createIn(ActivityPartition.class, subActivity);
        ForkNode forkNode = this.createIn(ForkNode.class, subActivity);
        forkNode.getInPartitions().add(activityPartition);
        JoinNode joinNode = this.createIn(JoinNode.class, subActivity);
        joinNode.getInPartitions().add(activityPartition);
        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(forkNode, joinNode, activityEdge, null, null, null);
        assertEquals(subActivity, container);
    }

    private void checkActivityEdgeContainerWithStructuredActivityNodeAsSourceContainerOnly(ActivityEdge activityEdge) {
        Activity activity = this.create(Activity.class);
        Activity subActivity = this.createIn(Activity.class, activity);
        ExpansionRegion expansionRegion = this.createIn(ExpansionRegion.class, subActivity);
        ForkNode forkNode = this.createIn(ForkNode.class, expansionRegion);
        JoinNode joinNode = this.createIn(JoinNode.class, activity);
        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(forkNode, joinNode, activityEdge, null, null, null);
        assertEquals(subActivity, container);
    }

    private void testActivityEdgeContainerWithStructuredActivityNodeContainer(ActivityEdge activityEdge) {
        Activity activity = this.create(Activity.class);
        Activity subActivity = this.createIn(Activity.class, activity);
        ExpansionRegion expansionRegion = this.createIn(ExpansionRegion.class, subActivity);
        ForkNode forkNode = this.createIn(ForkNode.class, expansionRegion);
        JoinNode joinNode = this.createIn(JoinNode.class, expansionRegion);
        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(forkNode, joinNode, activityEdge, null, null, null);
        assertEquals(expansionRegion, container);
    }
}
