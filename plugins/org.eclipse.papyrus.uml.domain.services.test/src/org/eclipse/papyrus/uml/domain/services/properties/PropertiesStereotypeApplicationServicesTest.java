/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.properties;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.papyrus.uml.domain.services.EMFUtils;
import org.eclipse.papyrus.uml.domain.services.properties.mock.MockEditableChecker;
import org.eclipse.papyrus.uml.domain.services.properties.mock.MockLogger;
import org.eclipse.uml2.uml.Class;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link PropertiesStereotypeApplicationServices} service class.
 * 
 * @author <a href="mailto:jerome.gout@obeosoft.fr">Jerome Gout</a>
 */
public class PropertiesStereotypeApplicationServicesTest extends AbstractPropertiesServicesTest {
    
    private static final String FEATURE_MONO_STRING = "testString";

    private static final String FEATURE_MONO_INT = "testInt";

    private static final String FEATURE_MONO_REAL = "testReal";

    private static final String FEATURE_MONO_BOOLEAN = "testBoolean";

    private static final String FEATURE_MONO_BOOLEAN_OBJECT = "testBooleanObject";

    private static final String FEATURE_MONO_ENUMERATION = "testRefToEnumeration";

    private static final String FEATURE_MULTI_STRING = "testMultiString";

    private static final String FEATURE_MULTI_INT = "testMultiInt";

    private static final String FEATURE_MULTI_REAL = "testMultiReal";

    private static final String FEATURE_MULTI_BOOLEAN = "testMultiBoolean";

    private static final String FEATURE_MULTI_ENUMERATION = "testMultiEnumeration";

    private static final String NOT_A_NUMBER = "notANumber";
    
    private static final String BOOLEAN_OBJECT_VALUE_TRUE = "true";
    
    private static final String BOOLEAN_OBJECT_VALUE_FALSE = "false";
    
    private static final String BOOLEAN_OBJECT_VALUE_NULL = "null";

    private static final String ENUMERATION_LITERAL1 = "EnumerationLiteral1";
    
    private static final String ENUMERATION_LITERAL2 = "EnumerationLiteral2";
    
    private static final String ENUMERATION_LITERAL3 = "EnumerationLiteral3";

    private static final String CLASS_STEREOTYPE1 = "ClassStereotype1";
    
    private static final URI INSTANCE_UML_MODEL = URI
            .createPlatformPluginURI("/org.eclipse.papyrus.uml.domain.services.test/profile/InstanceDynProfile.uml", false);
    
    private PropertiesStereotypeApplicationServices propertiesService;
    
    private PropertiesCrudServices crudService;

    private Resource instanceModel;

    @BeforeEach
    public void setUp() {
        MockLogger logger = new MockLogger();
        propertiesService = new PropertiesStereotypeApplicationServices(logger);
        crudService = new PropertiesCrudServices(logger, new MockEditableChecker());
        ResourceSet rs = new ResourceSetImpl();
        instanceModel = rs.getResource(INSTANCE_UML_MODEL, true);
    }
    
    private EObject getStereotypedApplicationOf(String className) {
        assertNotNull(className);
        Optional<Class> optional = EMFUtils.allContainedObjectOfType(instanceModel, Class.class)//
                .filter(c -> className.equals(c.getName()))//
                .findFirst();
        return optional.get().getStereotypeApplications().get(0);
    }
    
    private EStructuralFeature getFeature(String className, String featureName) {
        assertNotNull(featureName);
        var stereotype = getStereotypedApplicationOf(className);
        return propertiesService.getAllFeatures(stereotype).stream().filter(f -> featureName.equals(f.getName())).findFirst().orElse(null);
    }
    
    @Test
    public void testGetAllFeatures() {
        var clazz = getStereotypedApplicationOf(CLASS_STEREOTYPE1);
        var features = propertiesService.getAllFeatures(clazz);
        assertEquals(16, features.size());
        long baseElementFeature = features.stream().filter(f -> f.getName().startsWith("base_")).count();
        assertEquals(0, baseElementFeature, "The base feature element should be excluded from the feature to render");
    }

    @Test
    public void testSetMonoStringValue() {
        var stereotype = getStereotypedApplicationOf(CLASS_STEREOTYPE1);
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MONO_STRING);
        var changedStereotype = propertiesService.setStereotypeFeatureValue(stereotype, feature, "newValue");
        assertEquals("newValue", propertiesService.getStereotypeFeatureValue((EObject) changedStereotype, feature));
    }
    
    @Test
    public void testSetMonoIntegerValue() {
        var stereotype = getStereotypedApplicationOf(CLASS_STEREOTYPE1);
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MONO_INT);
        var changedStereotype = propertiesService.setStereotypeFeatureValue(stereotype, feature, "12");
        assertEquals(12, propertiesService.getStereotypeFeatureValue((EObject) changedStereotype, feature));
        changedStereotype = propertiesService.setStereotypeFeatureValue(stereotype, feature, "notIntValue");
        assertEquals(12, propertiesService.getStereotypeFeatureValue((EObject) changedStereotype, feature));
    }
    
    @Test
    public void testSetMonoDoubleValue() {
        var stereotype = getStereotypedApplicationOf(CLASS_STEREOTYPE1);
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MONO_REAL);
        var changedStereotype = propertiesService.setStereotypeFeatureValue(stereotype, feature, "1.2");
        assertEquals(1.2, propertiesService.getStereotypeFeatureValue((EObject) changedStereotype, feature));
        changedStereotype = propertiesService.setStereotypeFeatureValue(stereotype, feature, "notDoubleValue");
        assertEquals(1.2, propertiesService.getStereotypeFeatureValue((EObject) changedStereotype, feature));
    }
    
    @Test
    public void testSetMonoBooleanValue() {
        var stereotype = getStereotypedApplicationOf(CLASS_STEREOTYPE1);
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MONO_BOOLEAN);
        var changedStereotype = propertiesService.setStereotypeFeatureValue(stereotype, feature, true);
        assertEquals(true, propertiesService.getStereotypeFeatureValue((EObject) changedStereotype, feature));
        changedStereotype = propertiesService.setStereotypeFeatureValue(stereotype, feature, null); // default value is false
        assertEquals(false, propertiesService.getStereotypeFeatureValue((EObject) changedStereotype, feature));
    }
    
    @Test
    public void testSetMonoBooleanObjectValue() {
        var stereotype = getStereotypedApplicationOf(CLASS_STEREOTYPE1);
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MONO_BOOLEAN_OBJECT);
        var changedStereotype = propertiesService.setStereotypeFeatureValue(stereotype, feature, BOOLEAN_OBJECT_VALUE_TRUE);
        assertEquals(true, propertiesService.getStereotypeFeatureValue((EObject) changedStereotype, feature));
        changedStereotype = propertiesService.setStereotypeFeatureValue(stereotype, feature, BOOLEAN_OBJECT_VALUE_NULL);
        assertNull(propertiesService.getStereotypeFeatureValue((EObject) changedStereotype, feature));
        changedStereotype = propertiesService.setStereotypeFeatureValue(stereotype, feature, null); // default value is false
        assertEquals(false, propertiesService.getStereotypeFeatureValue((EObject) changedStereotype, feature));
    }
    
    private String getEnumerationValueLiteral(Object enumValue) {
        if (enumValue instanceof EEnumLiteral literal) {
            return literal.getLiteral();
        }
        return null;
    }
    
    @Test
    public void testSetMonoEnumerationValue() {
        var stereotype = getStereotypedApplicationOf(CLASS_STEREOTYPE1);
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MONO_ENUMERATION);
        var changedStereotype = propertiesService.setStereotypeFeatureValue(stereotype, feature, ENUMERATION_LITERAL2);
        assertEquals(ENUMERATION_LITERAL2, getEnumerationValueLiteral(propertiesService.getStereotypeFeatureValue((EObject) changedStereotype, feature)));
        changedStereotype = propertiesService.setStereotypeFeatureValue(stereotype, feature, "unknownEnumerationLiteral"); // default value is ENUMERATION_LITERAL1
        assertEquals(ENUMERATION_LITERAL1, getEnumerationValueLiteral(propertiesService.getStereotypeFeatureValue((EObject) changedStereotype, feature)));
        changedStereotype = propertiesService.setStereotypeFeatureValue(stereotype, feature, ENUMERATION_LITERAL3);
        assertEquals(ENUMERATION_LITERAL3, getEnumerationValueLiteral(propertiesService.getStereotypeFeatureValue((EObject) changedStereotype, feature)));
        changedStereotype = propertiesService.setStereotypeFeatureValue(stereotype, feature, null); // default value is ENUMERATION_LITERAL1
        assertEquals(ENUMERATION_LITERAL1, getEnumerationValueLiteral(propertiesService.getStereotypeFeatureValue((EObject) changedStereotype, feature)));
    }

    @Test
    public void testSetMultiStringAttribute() {
        var stereotype = getStereotypedApplicationOf(CLASS_STEREOTYPE1);
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MULTI_STRING);
        crudService.addToAttribute(stereotype, feature.getName(), "newString1");
        crudService.addToAttribute(stereotype, feature.getName(), "newString2");
        crudService.addToAttribute(stereotype, feature.getName(), "newString3");
        Object values = propertiesService.getStereotypeFeatureValue(stereotype, feature);
        assertInstanceOf(List.class, values);
        assertEquals(3, ((List<?>) values).size());
        assertEquals("newString3", ((List<?>) values).get(2));
    }
    
    @Test
    public void testSetMultiIntegerAttribute() {
        var stereotype = getStereotypedApplicationOf(CLASS_STEREOTYPE1);
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MULTI_INT);
        crudService.addToAttribute(stereotype, feature.getName(), "-12");
        crudService.addToAttribute(stereotype, feature.getName(), "99");
        crudService.addToAttribute(stereotype, feature.getName(), "2147483647"); // MAXINT
        crudService.addToAttribute(stereotype, feature.getName(), "9.9");
        crudService.addToAttribute(stereotype, feature.getName(), NOT_A_NUMBER);
        Object values = propertiesService.getStereotypeFeatureValue(stereotype, feature);
        assertInstanceOf(List.class, values);
        assertEquals(3, ((List<?>) values).size());
        assertEquals(2147483647, ((List<?>) values).get(2));
    }
    
    @Test
    public void testSetMultiDoubleAttribute() {
        var stereotype = getStereotypedApplicationOf(CLASS_STEREOTYPE1);
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MULTI_REAL);
        crudService.addToAttribute(stereotype, feature.getName(), "-0.1");
        crudService.addToAttribute(stereotype, feature.getName(), "99");
        crudService.addToAttribute(stereotype, feature.getName(), String.valueOf(Double.MAX_VALUE));
        crudService.addToAttribute(stereotype, feature.getName(), NOT_A_NUMBER);
        Object values = propertiesService.getStereotypeFeatureValue(stereotype, feature);
        assertInstanceOf(List.class, values);
        assertEquals(3, ((List<?>) values).size());
        assertEquals(Double.MAX_VALUE, ((List<?>) values).get(2));
    }
    
    @Test
    public void testSetMultiBooleanAttribute() {
        var stereotype = getStereotypedApplicationOf(CLASS_STEREOTYPE1);
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MULTI_BOOLEAN);
        crudService.addToAttribute(stereotype, feature.getName(), BOOLEAN_OBJECT_VALUE_TRUE);
        crudService.addToAttribute(stereotype, feature.getName(), BOOLEAN_OBJECT_VALUE_FALSE);
        crudService.addToAttribute(stereotype, feature.getName(), NOT_A_NUMBER); // default value used => false
        Object values = propertiesService.getStereotypeFeatureValue(stereotype, feature);
        assertInstanceOf(List.class, values);
        assertEquals(3, ((List<?>) values).size());
        assertEquals(false, ((List<?>) values).get(2));
    }
    
    @Test
    public void testSetMultiEnumerationAttribute() {
        var stereotype = getStereotypedApplicationOf(CLASS_STEREOTYPE1);
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MULTI_ENUMERATION);
        crudService.addToAttribute(stereotype, feature.getName(), ENUMERATION_LITERAL2);
        crudService.addToAttribute(stereotype, feature.getName(), ENUMERATION_LITERAL3);
        crudService.addToAttribute(stereotype, feature.getName(), ENUMERATION_LITERAL1);
        crudService.addToAttribute(stereotype, feature.getName(), "unknownLiteralValue"); // default value used => false
        crudService.addToAttribute(stereotype, feature.getName(), null); // default value used => false
        Object values = propertiesService.getStereotypeFeatureValue(stereotype, feature);
        assertInstanceOf(List.class, values);
        assertEquals(3, ((List<?>) values).size());
        assertEquals(ENUMERATION_LITERAL1, getEnumerationValueLiteral(((List<?>) values).get(2)));
    }
    
    @Test
    public void testIsMonoStringAttribute() {
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MONO_STRING);
        assertTrue(propertiesService.isEditable(feature));
        assertTrue(propertiesService.isMonoStringAttribute(feature));
        assertFalse(propertiesService.isMultiStringAttribute(feature));
        assertFalse(propertiesService.isMonoBooleanAttribute(feature));
    }

    @Test
    public void testIsMultiStringAttribute() {
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MULTI_STRING);
        assertTrue(propertiesService.isEditable(feature));
        assertFalse(propertiesService.isMonoStringAttribute(feature));
        assertTrue(propertiesService.isMultiStringAttribute(feature));
        assertFalse(propertiesService.isMonoBooleanAttribute(feature));
    }

    @Test
    public void testIsMonoBooleanAttribute() {
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MONO_BOOLEAN);
        assertTrue(propertiesService.isEditable(feature));
        assertTrue(propertiesService.isMonoBooleanAttribute(feature));
        assertFalse(propertiesService.isMonoBooleanObjectAttribute(feature));
        assertFalse(propertiesService.isMultiStringAttribute(feature));
    }
    
    @Test
    public void testIsMonoBooleanObjectAttribute() {
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MONO_BOOLEAN_OBJECT);
        assertTrue(propertiesService.isEditable(feature));
        assertTrue(propertiesService.isMonoBooleanObjectAttribute(feature));
        assertFalse(propertiesService.isMonoBooleanAttribute(feature));
        assertFalse(propertiesService.isMultiStringAttribute(feature));
    }

    @Test
    public void testIsMultiBooleanAttribute() {
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MULTI_BOOLEAN);
        assertTrue(propertiesService.isEditable(feature));
        assertTrue(propertiesService.isMultiBooleanAttribute(feature));
        assertFalse(propertiesService.isMonoBooleanObjectAttribute(feature));
        assertFalse(propertiesService.isMultiStringAttribute(feature));
    }
    
    @Test
    public void testIsMonoIntegerAttribute() {
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MONO_INT);
        assertTrue(propertiesService.isEditable(feature));
        assertTrue(propertiesService.isMonoIntegerAttribute(feature));
        assertFalse(propertiesService.isMultiIntegerAttribute(feature));
        assertFalse(propertiesService.isMultiStringAttribute(feature));
    }

    @Test
    public void testIsMultiIntegerAttribute() {
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MULTI_INT);
        assertTrue(propertiesService.isEditable(feature));
        assertTrue(propertiesService.isMultiIntegerAttribute(feature));
        assertFalse(propertiesService.isMonoIntegerAttribute(feature));
        assertFalse(propertiesService.isMultiStringAttribute(feature));
    }

    @Test
    public void testIsMonoDoubleAttribute() {
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MONO_REAL);
        assertTrue(propertiesService.isEditable(feature));
        assertTrue(propertiesService.isMonoDoubleAttribute(feature));
        assertFalse(propertiesService.isMultiDoubleAttribute(feature));
        assertFalse(propertiesService.isMonoFloatAttribute(feature));
    }

    @Test
    public void testIsMultiDoubleAttribute() {
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MULTI_REAL);
        assertTrue(propertiesService.isEditable(feature));
        assertTrue(propertiesService.isMultiDoubleAttribute(feature));
        assertFalse(propertiesService.isMonoDoubleAttribute(feature));
        assertFalse(propertiesService.isMultiFloatAttribute(feature));
    }

    @Test
    public void testIsMonoEnumeration() {
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MONO_ENUMERATION);
        assertTrue(propertiesService.isEditable(feature));
        assertTrue(propertiesService.isMonoEnumeration(feature));
        assertFalse(propertiesService.isMultiEnumeration(feature));
        assertFalse(propertiesService.isMonoStringAttribute(feature));
    }

    @Test
    public void testIsMultiEnumeration() {
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MULTI_ENUMERATION);
        assertTrue(propertiesService.isEditable(feature));
        assertTrue(propertiesService.isMultiEnumeration(feature));
        assertFalse(propertiesService.isMonoEnumeration(feature));
        assertFalse(propertiesService.isMultiStringAttribute(feature));
    }

    @Test
    public void testGetStereotypeEnumerationLiterals() {
        var feature = getFeature(CLASS_STEREOTYPE1, FEATURE_MULTI_ENUMERATION);
        List<String> enumLiterals = propertiesService.getStereotypeEnumerationLiterals(feature);
        assertEquals(3, enumLiterals.size());
        assertEquals(ENUMERATION_LITERAL1, enumLiterals.get(0));
        assertEquals(ENUMERATION_LITERAL2, enumLiterals.get(1));
        assertEquals(ENUMERATION_LITERAL3, enumLiterals.get(2));
    }

    @Test
    public void testGetStereotypeBooleanObjectLiterals() {
        List<String> boolLiterals = propertiesService.getStereotypeBooleanObjectLiterals(null);
        assertEquals(3, boolLiterals.size());
        assertEquals(BOOLEAN_OBJECT_VALUE_TRUE, boolLiterals.get(0));
        assertEquals(BOOLEAN_OBJECT_VALUE_FALSE, boolLiterals.get(1));
        assertEquals(BOOLEAN_OBJECT_VALUE_NULL, boolLiterals.get(2));
    }
}
