/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.edges;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.List;

import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.ActionExecutionSpecification;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.ComponentRealization;
import org.eclipse.uml2.uml.ControlFlow;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.ExecutionOccurrenceSpecification;
import org.eclipse.uml2.uml.ExecutionSpecification;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import org.eclipse.uml2.uml.Node;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementDomainBasedEdgeTargetsProvider}.
 *
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 *
 */
public class ElementDomainBasedEdgeTargetsProviderTest extends AbstractUMLTest {

    private ElementDomainBasedEdgeTargetsProvider targetProvider = new ElementDomainBasedEdgeTargetsProvider();

    @Test
    public void testAssociation() {
        Association association = this.create(Association.class);
        Actor source = this.create(Actor.class);
        Actor target = this.create(Actor.class);
        new ElementDomainBasedEdgeInitializer().initialize(association, source, target, null, null, null);

        assertEquals(List.of(target), this.targetProvider.getTargets(association));

        // if type of source Property is null, none target is found
        association.getMemberEnds().get(0).setType(null);
        assertEquals(Collections.emptyList(), this.targetProvider.getTargets(association));
    }

    /**
     * Tests target provider on {@link ComponentRealization}.
     */
    @Test
    public void testComponentRealization() {
        ComponentRealization componentRealization = this.create(ComponentRealization.class);
        assertTrue(this.targetProvider.getTargets(componentRealization).isEmpty());

        Component targetComponent = this.create(Component.class);
        componentRealization.setAbstraction(targetComponent);
        assertEquals(List.of(targetComponent), this.targetProvider.getTargets(componentRealization));
    }

    /**
     * Checks the target provider for {@link ControlFlow}.
     */
    @Test
    public void testControlFlow() {
        ControlFlow controlFlow = this.create(ControlFlow.class);
        InputPin inputPin = this.create(InputPin.class);
        OutputPin outputPin = this.create(OutputPin.class);
        controlFlow.setSource(outputPin);
        controlFlow.setTarget(inputPin);
        assertEquals(List.of(inputPin), this.targetProvider.getTargets(controlFlow));
    }

    /**
     * Tests source provider on {link Dependency}.
     */
    @Test
    public void testDependency() {
        Dependency dependency = this.create(Dependency.class);
        assertTrue(this.targetProvider.getTargets(dependency).isEmpty());

        Class class1 = this.create(org.eclipse.uml2.uml.Class.class);
        dependency.getSuppliers().add(class1);
        assertEquals(List.of(class1), this.targetProvider.getTargets(dependency));
    }

    @Test
    public void testDeployment() {
        Deployment deployment = this.create(Deployment.class);
        assertTrue(this.targetProvider.getTargets(deployment).isEmpty());

        Node node = this.create(Node.class);
        deployment.setLocation(node);
        assertEquals(List.of(node), this.targetProvider.getTargets(deployment));
    }

    @Test
    public void testExtend() {
        Extend extend = this.create(Extend.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);
        new ElementDomainBasedEdgeInitializer().initialize(extend, source, target, null, null, null);

        assertEquals(List.of(target), this.targetProvider.getTargets(extend));
    }

    /**
     * Tests target provider for {@link Extension}.
     */
    @Test
    public void testExtension() {
        Extension extension = this.create(Extension.class);
        Stereotype stereotype = this.create(Stereotype.class);
        Property property = this.create(Property.class);
        Class class1 = this.create(org.eclipse.uml2.uml.Class.class);
        property.setType(class1);
        property.setAssociation(extension);
        extension.getMemberEnds().add(property);
        stereotype.getOwnedAttributes().add(property);
        assertEquals(List.of(class1), this.targetProvider.getTargets(extension));
    }

    /**
     * Tests source provider on {link Generalization}.
     */
    @Test
    public void testGeneralization() {
        Generalization generalization = this.create(Generalization.class);
        assertTrue(this.targetProvider.getTargets(generalization).isEmpty());

        Class class1 = this.create(org.eclipse.uml2.uml.Class.class);
        generalization.setGeneral(class1);
        assertEquals(List.of(class1), this.targetProvider.getTargets(generalization));
    }

    @Test
    public void testInclude() {
        Include include = this.create(Include.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);
        new ElementDomainBasedEdgeInitializer().initialize(include, source, target, null, null, null);

        assertEquals(List.of(target), this.targetProvider.getTargets(include));
    }

    /**
     * Tests source provider on {link InformationFlow}.
     */
    @Test
    public void testInformationFlow() {
        InformationFlow informationFlow = this.create(InformationFlow.class);
        assertTrue(this.targetProvider.getTargets(informationFlow).isEmpty());

        Class class1 = this.create(org.eclipse.uml2.uml.Class.class);
        informationFlow.getInformationTargets().add(class1);
        assertEquals(List.of(class1), this.targetProvider.getTargets(informationFlow));
    }

    /**
     * Tests source provider on {link InterfaceRealization}.
     */
    @Test
    public void testInterfaceRealization() {
        InterfaceRealization interfaceRealization = this.create(InterfaceRealization.class);
        assertTrue(this.targetProvider.getTargets(interfaceRealization).isEmpty());

        Interface interface1 = this.create(org.eclipse.uml2.uml.Interface.class);
        interfaceRealization.setContract(interface1);
        assertEquals(List.of(interface1), this.targetProvider.getTargets(interfaceRealization));
    }

    /**
     * Tests source provider on {link Manifestation}.
     */
    @Test
    public void testManifestation() {
        Manifestation manifestation = this.create(Manifestation.class);
        assertTrue(this.targetProvider.getTargets(manifestation).isEmpty());

        Class class1 = this.create(org.eclipse.uml2.uml.Class.class);
        manifestation.setUtilizedElement(class1);
        assertEquals(List.of(class1), this.targetProvider.getTargets(manifestation));
    }

    /**
     * Tests target provider on {@link Message} when
     * {@link Message#getReceiveEvent()} is after an {@link ExecutionSpecification}.
     * <p>
     * The expected result is the {@link Lifeline} covered by
     * {@link Message#getReceiveEvent()}.
     */
    @Test
    public void testMessageExecutionSpecificationAfterSendEvent() {
        Interaction interaction = this.create(Interaction.class);
        Message message = this.create(Message.class);
        MessageOccurrenceSpecification messageReceiveEvent = this.create(MessageOccurrenceSpecification.class);
        message.setReceiveEvent(messageReceiveEvent);
        Lifeline sourceLifeline = this.create(Lifeline.class);
        messageReceiveEvent.setMessage(message);
        messageReceiveEvent.getCovereds().add(sourceLifeline);

        ExecutionSpecification executionSpecification = this.create(ActionExecutionSpecification.class);
        executionSpecification.getCovereds().add(sourceLifeline);
        ExecutionOccurrenceSpecification executionSpecificationStart = this
                .create(ExecutionOccurrenceSpecification.class);
        executionSpecificationStart.getCovereds().add(sourceLifeline);
        executionSpecification.setStart(executionSpecificationStart);
        executionSpecificationStart.setExecution(executionSpecification);
        ExecutionOccurrenceSpecification executionSpecificationFinish = this
                .create(ExecutionOccurrenceSpecification.class);
        executionSpecificationFinish.getCovereds().add(sourceLifeline);
        executionSpecification.setFinish(executionSpecificationFinish);
        executionSpecificationFinish.setExecution(executionSpecification);
        interaction.getFragments().addAll(List.of(messageReceiveEvent, executionSpecificationStart,
                executionSpecification, executionSpecificationFinish));

        Lifeline target = this.create(Lifeline.class);

        new ElementDomainBasedEdgeInitializer().initialize(message, executionSpecification, target, null, null, null);

        assertEquals(List.of(sourceLifeline), this.targetProvider.getTargets(message));
    }

    /**
     * Tests target provider on {@link Message} when
     * {@link Message#getReceiveEvent()} is before an
     * {@link ExecutionSpecification}.
     * <p>
     * The expected result is the {@link Lifeline} covered by
     * {@link Message#getReceiveEvent()}.
     */
    @Test
    public void testMessageExecutionSpecificationBeforeSendEvent() {
        Interaction interaction = this.create(Interaction.class);
        Message message = this.create(Message.class);
        MessageOccurrenceSpecification messageReceiveEvent = this.create(MessageOccurrenceSpecification.class);
        message.setReceiveEvent(messageReceiveEvent);
        Lifeline sourceLifeline = this.create(Lifeline.class);
        messageReceiveEvent.setMessage(message);
        messageReceiveEvent.getCovereds().add(sourceLifeline);

        ExecutionSpecification executionSpecification = this.create(ActionExecutionSpecification.class);
        executionSpecification.getCovereds().add(sourceLifeline);
        ExecutionOccurrenceSpecification executionSpecificationStart = this
                .create(ExecutionOccurrenceSpecification.class);
        executionSpecificationStart.getCovereds().add(sourceLifeline);
        executionSpecification.setStart(executionSpecificationStart);
        executionSpecificationStart.setExecution(executionSpecification);
        ExecutionOccurrenceSpecification executionSpecificationFinish = this
                .create(ExecutionOccurrenceSpecification.class);
        executionSpecificationFinish.getCovereds().add(sourceLifeline);
        executionSpecification.setFinish(executionSpecificationFinish);
        executionSpecificationFinish.setExecution(executionSpecification);
        interaction.getFragments().addAll(List.of(executionSpecificationStart, executionSpecification,
                executionSpecificationFinish, messageReceiveEvent));

        Lifeline target = this.create(Lifeline.class);

        new ElementDomainBasedEdgeInitializer().initialize(message, executionSpecification, target, null, null, null);

        assertEquals(List.of(sourceLifeline), this.targetProvider.getTargets(message));
    }

    /**
     * Tests target provider on {@link Message} when
     * {@link Message#getReceiveEvent()} is enclosed in an
     * {@link ExecutionSpecification}.
     * <p>
     * The expected result is the {@link ExecutionSpecification} enclosing the
     * {@link Message#getReceiveEvent()}.
     */
    @Test
    public void testMessageExecutionSpecificationEnclosingSendEvent() {
        Interaction interaction = this.create(Interaction.class);
        Message message = this.create(Message.class);
        MessageOccurrenceSpecification messageReceiveEvent = this.create(MessageOccurrenceSpecification.class);
        message.setReceiveEvent(messageReceiveEvent);
        Lifeline sourceLifeline = this.create(Lifeline.class);
        messageReceiveEvent.setMessage(message);
        messageReceiveEvent.getCovereds().add(sourceLifeline);

        ExecutionSpecification executionSpecification = this.create(ActionExecutionSpecification.class);
        executionSpecification.getCovereds().add(sourceLifeline);
        ExecutionOccurrenceSpecification executionSpecificationStart = this
                .create(ExecutionOccurrenceSpecification.class);
        executionSpecificationStart.getCovereds().add(sourceLifeline);
        executionSpecification.setStart(executionSpecificationStart);
        executionSpecificationStart.setExecution(executionSpecification);
        ExecutionOccurrenceSpecification executionSpecificationFinish = this
                .create(ExecutionOccurrenceSpecification.class);
        executionSpecificationFinish.getCovereds().add(sourceLifeline);
        executionSpecification.setFinish(executionSpecificationFinish);
        executionSpecificationFinish.setExecution(executionSpecification);
        interaction.getFragments().addAll(List.of(executionSpecificationStart, executionSpecification,
                messageReceiveEvent, executionSpecificationFinish));

        Lifeline target = this.create(Lifeline.class);

        new ElementDomainBasedEdgeInitializer().initialize(message, executionSpecification, target, null, null, null);

        assertEquals(List.of(executionSpecification), this.targetProvider.getTargets(message));
    }

    /**
     * Tests target provider on {@link Message} when
     * {@link Message#getReceiveEvent()} is the only element of its
     * {@link Lifeline}.
     * <p>
     * The expected result is the {@link Lifeline} covered by
     * {@link Message#getReceiveEvent()}.
     */
    @Test
    public void testMessageLifelineSource() {
        Interaction interaction = this.create(Interaction.class);
        Message message = this.create(Message.class);
        MessageOccurrenceSpecification messageReceiveEvent = this.create(MessageOccurrenceSpecification.class);
        message.setReceiveEvent(messageReceiveEvent);
        messageReceiveEvent.setMessage(message);
        Lifeline source = this.create(Lifeline.class);
        messageReceiveEvent.getCovereds().add(source);
        Lifeline target = this.create(Lifeline.class);
        interaction.getFragments().add(messageReceiveEvent);

        new ElementDomainBasedEdgeInitializer().initialize(message, source, target, null, null, null);

        assertEquals(List.of(source), this.targetProvider.getTargets(message));
    }

    /**
     * Tests target provider on {@link Message} when
     * {@link Message#getReceiveEvent()} is {@code null}.
     * <p>
     * The expected result is an empty collection.
     */
    @Test
    public void testMessageNullSendEvent() {
        Message message = this.create(Message.class);
        Lifeline source = this.create(Lifeline.class);
        Lifeline target = this.create(Lifeline.class);

        new ElementDomainBasedEdgeInitializer().initialize(message, source, target, null, null, null);

        assertEquals(Collections.emptyList(), this.targetProvider.getTargets(message));
    }

    /**
     * Checks the target provider for {@link ObjectFlow}.
     */
    @Test
    public void testObjectFlow() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        InputPin inputPin = this.create(InputPin.class);
        OutputPin outputPin = this.create(OutputPin.class);
        objectFlow.setSource(outputPin);
        objectFlow.setTarget(inputPin);
        assertEquals(List.of(inputPin), this.targetProvider.getTargets(objectFlow));
    }

    /**
     * Tests source provider on {link PackageImport}.
     */
    @Test
    public void testPackageImport() {
        PackageImport element = this.create(PackageImport.class);
        assertEquals(Collections.emptyList(), this.targetProvider.getTargets(element));

        Package target = this.create(org.eclipse.uml2.uml.Package.class);
        element.setImportedPackage(target);
        assertEquals(List.of(target), this.targetProvider.getTargets(element));
    }

    /**
     * Tests source provider on {link PackageImport}.
     */
    @Test
    public void testPackageMerge() {
        PackageMerge element = this.create(PackageMerge.class);
        assertEquals(Collections.emptyList(), this.targetProvider.getTargets(element));

        Package target = this.create(org.eclipse.uml2.uml.Package.class);
        element.setMergedPackage(target);
        assertEquals(List.of(target), this.targetProvider.getTargets(element));
    }

    /**
     * Tests source provider on {link Substitution}.
     */
    @Test
    public void testSubstitution() {
        Substitution substitution = this.create(Substitution.class);
        assertTrue(this.targetProvider.getTargets(substitution).isEmpty());

        Class class1 = this.create(org.eclipse.uml2.uml.Class.class);
        substitution.setContract(class1);
        assertEquals(List.of(class1), this.targetProvider.getTargets(substitution));
    }

    @Test
    public void testTransition() {
        Pseudostate source = this.create(Pseudostate.class);
        Pseudostate target = this.create(Pseudostate.class);

        Transition transition = this.create(Transition.class);
        transition.setSource(source);
        transition.setTarget(target);

        assertEquals(List.of(target), this.targetProvider.getTargets(transition));
    }
}
