/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collections;
import java.util.Set;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.ComponentExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Reception;
import org.eclipse.uml2.uml.Relationship;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for
 * {@link ComponentExternalSourceToRepresentationDropBehaviorProvider}.
 *
 * @author <a href="mailto:gwendal.daniel@obeosoft.com">Gwendal Daniel</a>
 */
public class ComponentExternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    private ComponentExternalSourceToRepresentationDropBehaviorProvider componentExternalSourceToRepresentationDropBehaviorProvider;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        this.componentExternalSourceToRepresentationDropBehaviorProvider = new ComponentExternalSourceToRepresentationDropBehaviorProvider();
    }

    /**
     * Test dropping a {@link Comment} on a {@link Comment}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testCommentDropOnComment() {
        Comment comment1 = this.create(Comment.class);
        Comment comment2 = this.create(Comment.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(comment1, comment2,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());

    }

    /**
     * Test dropping a {@link Comment} on a {@link Package}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testCommentDropOnPackage() {
        Comment comment = this.create(Comment.class);
        Package pack = this.create(Package.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(comment, pack,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(comment), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Component} on a {@link Comment}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testComponentDropOnComment() {
        Component component = this.create(Component.class);
        Comment comment = this.create(Comment.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(component, comment,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Component} on a {@link Component}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testComponentDropOnComponent() {
        Component component1 = this.create(Component.class);
        Component component2 = this.create(Component.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(component1, component2,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(component1), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Component} on a {@link Package}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testComponentDropOnPackage() {
        Component component = this.create(Component.class);
        Package pack = this.create(Package.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(component, pack,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(component), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Component} on a {@link Port}.
     *
     * Expected result : the {@link Port}'s type is updated to the provided
     * {@link Component}, but no graphical element should be created.
     */
    @Test
    public void testComponentDropOnPort() {
        Component component = this.create(Component.class);
        Port port = this.create(Port.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(component, port,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
        assertEquals(component, port.getType());
    }

    /**
     * Test dropping a {@link Component} on a {@link Property}.
     *
     * Expected result : the {@link Property}'s type is updated to the provided
     * {@link Component}, but no graphical element should be created.
     */
    @Test
    public void testComponentDropOnProperty() {
        Component component = this.create(Component.class);
        Property property = this.create(Property.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(component, property,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
        assertEquals(component, property.getType());
    }

    /**
     * Test dropping a {@link Connector} on a {@link Comment}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testConnectorDropOnComment() {
        Connector connector = this.create(Connector.class);
        Comment comment = this.create(Comment.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(connector, comment,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(connector), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Constraint} on a {@link Comment}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testConstraintDropOnComment() {
        Constraint constraint = this.create(Constraint.class);
        Comment comment = this.create(Comment.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(constraint, comment,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Constraint} on a {@link Package}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testConstraintDropOnPackage() {
        Constraint constraint = this.create(Constraint.class);
        Package pack = this.create(Package.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(constraint, pack,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(constraint), status.getElementsToDisplay());
    }

    /**
     * Test dropping an {@link Interface} on a {@link Comment}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testInterfaceDropOnComment() {
        Interface interfaceElement = this.create(Interface.class);
        Comment comment = this.create(Comment.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(interfaceElement,
                comment, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping an {@link Interface} on a {@link Package}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testInterfaceDropOnPackage() {
        Interface interfaceElement = this.create(Interface.class);
        Package pack = this.create(Package.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(interfaceElement, pack,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(interfaceElement), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Interface} on a {@link Port}.
     *
     * Expected result : the {@link Port}'s type is updated to the provided
     * {@link Interface}, but no graphical element should be created.
     */
    @Test
    public void testInterfaceDropOnPort() {
        Interface interfaceElement = this.create(Interface.class);
        Port port = this.create(Port.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(interfaceElement, port,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
        assertEquals(interfaceElement, port.getType());
    }

    /**
     * Test dropping an {@link Interface} on a {@link Property}.
     *
     * Expected result : the {@link Property}'s type is updated to the provided
     * {@link Interface}, but no graphical element should be created.
     */
    @Test
    public void testInterfaceDropOnProperty() {
        Interface interfaceElement = this.create(Interface.class);
        Property property = this.create(Property.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(interfaceElement,
                property, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
        assertEquals(interfaceElement, property.getType());
    }

    /**
     * Test dropping an {@link Operation} on a {@link Comment}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testOperationDropOnComment() {
        Operation operation = this.create(Operation.class);
        Comment comment = this.create(Comment.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(operation, comment,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping an {@link Operation} on a {@link Interface}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testOperationDropOnInterface() {
        Operation operation = this.create(Operation.class);
        Interface interfaceElement = this.create(Interface.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(operation,
                interfaceElement, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(operation), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Package} on a {@link Comment}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testPackageDropOnComment() {
        Package pack = this.create(Package.class);
        Comment comment = this.create(Comment.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(pack, comment,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Package} on a {@link Package}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testPackageDropOnPackage() {
        Package pack1 = this.create(Package.class);
        Package pack2 = this.create(Package.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(pack1, pack2,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(pack1), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Port} on a {@link Comment}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testPortDropOnComment() {
        Port port = this.create(Port.class);
        Comment comment = this.create(Comment.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(port, comment,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Port} on a {@link Component}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testPortDropOnComponent() {
        Port port = this.create(Port.class);
        Component component = this.create(Component.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(port, component,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(port), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Port} on a {@link Property}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testPortDropOnProperty() {
        Port port = this.create(Port.class);
        Property property = this.create(Property.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(port, property,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(port), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Property} on a {@link Comment}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testPropertyDropOnComment() {
        Property property = this.create(Property.class);
        Comment comment = this.create(Comment.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(property, comment,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Property} on a {@link Component}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testPropertyDropOnComponent() {
        Property property = this.create(Property.class);
        Component component = this.create(Component.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(property, component,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(property), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Property} on a {@link Interface}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testPropertyDropOnInterface() {
        Property property = this.create(Property.class);
        Interface interfaceElement = this.create(Interface.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(property,
                interfaceElement, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(property), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Property} on a typed {@link Property}.
     *
     * Expected result: nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testPropertyDropOnTypedProperty() {
        Property property = this.create(Property.class);
        Property typedProperty = this.create(Property.class);
        Component component = this.create(Component.class);
        typedProperty.setType(component);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(property,
                typedProperty, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(property), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Reception} on a {@link Comment}.
     *
     * Expected result: DnD failed.
     */
    @Test
    public void testReceptionDropOnComment() {
        Reception reception = this.create(Reception.class);
        Comment comment = this.create(Comment.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(reception, comment,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Reception} on an {@link Interface}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testReceptionDropOnComponent() {
        Reception reception = this.create(Reception.class);
        Interface interfaceElement = this.create(Interface.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(reception,
                interfaceElement, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(reception), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Relationship} on a {@link Comment}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testRelationshipDropOnComment() {
        Relationship relationship = this.create(Abstraction.class);
        Comment comment = this.create(Comment.class);

        DnDStatus status = this.componentExternalSourceToRepresentationDropBehaviorProvider.drop(relationship, comment,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(relationship), status.getElementsToDisplay());
    }
}
