/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.create.ElementConfigurer;
import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeInitializer;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.papyrus.uml.domain.services.utils.MockedViewQuerier;
import org.eclipse.papyrus.uml.domain.services.utils.VisualNode;
import org.eclipse.papyrus.uml.domain.services.utils.VisualTree;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.ComponentRealization;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.ControlFlow;
import org.eclipse.uml2.uml.DecisionNode;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.DeployedArtifact;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.ForkNode;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.JoinNode;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Node;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.StructuredActivityNode;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementDomainBasedEdgeReconnectSourceBehaviorProvider}.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 *
 */
public class ElementDomainBasedEdgeReconnectSourceBehaviorProviderTest extends AbstractUMLTest {

    /* Name class2 */
    private static final String CLASS2 = "class2";

    /* Name class1 */
    private static final String CLASS1 = "class1";

    private static final String STEREOTYPE = "stereotype";

    private static final String STEREOTYPE2 = "stereotype2";

    /**
     * Test reconnect source of association on an other source (source property is
     * not owned by the association).
     */
    @Test
    public void testAssociationSourceReconnectWithPropertyNotOwnedByAssociation() {
        Model model = this.create(Model.class);
        Class classOldSource = this.createIn(Class.class, model);
        classOldSource.setName(CLASS1);
        Class classTarget = this.createIn(Class.class, model);
        Association association = this.createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classOldSource, classTarget, null, null, null);

        // change owner of source Property
        Property sourceProperty = association.getMemberEnds().get(0);
        classOldSource.getOwnedAttributes().add(sourceProperty);
        Class classNewSource = this.createIn(Class.class, model);
        classNewSource.setName(CLASS2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(association, classOldSource, classNewSource, null);

        assertTrue(status.isValid());
        assertEquals(2, association.getMemberEnds().size());
        assertEquals(1, association.getOwnedMembers().size());
        assertFalse(classOldSource.getOwnedAttributes().contains(sourceProperty));
        assertTrue(classNewSource.getOwnedAttributes().contains(sourceProperty));
        Property targetProperty = association.getMemberEnds().get(1);
        assertEquals(classNewSource, targetProperty.getType());
        assertEquals(classOldSource.getName(), targetProperty.getName());
    }

    /**
     * Test reconnect source of association on an other source (source property is
     * owned by the association).
     */
    @Test
    public void testAssociationSourceReconnectWithPropertyOwnedByAssociation() {
        Model model = this.create(Model.class);
        Class classOldSource = this.createIn(Class.class, model);
        classOldSource.setName(CLASS1);
        Class classTarget = this.createIn(Class.class, model);
        Association association = this.createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classOldSource, classTarget, null, null, null);
        Class classNewSource = this.createIn(Class.class, model);
        classNewSource.setName(CLASS2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(association, classOldSource, classNewSource, null);

        assertTrue(status.isValid());
        assertEquals(2, association.getMemberEnds().size());
        assertEquals(2, association.getOwnedMembers().size());
        Property sourceProperty = association.getMemberEnds().get(0);
        assertEquals(classTarget, sourceProperty.getType());
        Property targetProperty = association.getMemberEnds().get(1);
        assertEquals(classNewSource, targetProperty.getType());
        assertEquals(classOldSource.getName(), targetProperty.getName());
    }

    /**
     * Default source reconnection for {@link ComponentRealization}.
     */
    @Test
    public void testComponentRealizationSourceReconnect() {
        Interface sourceInterface = this.create(Interface.class);
        Component targetComponent = this.create(Component.class);
        ComponentRealization componentRealization = this.createIn(ComponentRealization.class, targetComponent);
        componentRealization.getRealizingClassifiers().add(sourceInterface);
        componentRealization.setAbstraction(targetComponent);
        Interface newSourceInterface = this.create(Interface.class);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier())
                .reconnectSource(componentRealization, sourceInterface, newSourceInterface, null);

        assertTrue(status.isValid());
        assertEquals(List.of(newSourceInterface), componentRealization.getRealizingClassifiers());
        assertEquals(List.of(newSourceInterface), componentRealization.getClients());
        // Make sure the provider didn't produce unexpected side effects on the
        // component realization target.
        assertEquals(List.of(targetComponent), componentRealization.getSuppliers());
        assertEquals(targetComponent, componentRealization.getAbstraction());
        // ComponentRealization are contained in their target, so the container
        // shouldn't change when reconnecting the source.
        assertEquals(targetComponent, componentRealization.eContainer());
    }

    /**
     * Test the source reconnection for a connector on a classifier port. This test
     * aims to check whether the partWithPort attribute is set at null.
     */
    @Test
    public void testConnectorSourceReconnectOnClassifierPort() {
        // Create semantic elements
        Class class1 = this.create(Class.class);
        Class class2 = this.create(Class.class);
        Port oldSourcePort = this.createIn(Port.class, class1);
        Port newSourcePort = this.createIn(Port.class, class2);
        Port targetPort = this.createIn(Port.class, class1);
        Property property = this.create(Property.class);
        property.setType(class1);
        Connector connector = this.create(Connector.class);
        ConnectorEnd connectorEndSource = this.createIn(ConnectorEnd.class, connector);
        connectorEndSource.setRole(oldSourcePort);
        connectorEndSource.setPartWithPort(property);
        ConnectorEnd connectorEndTarget = this.createIn(ConnectorEnd.class, connector);
        connectorEndTarget.setRole(targetPort);
        connectorEndTarget.setPartWithPort(property);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode class2Node = visualTree.addChildren(class2);
        VisualNode visualNewSourcePort = class2Node.addChildren(newSourcePort);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(connector, oldSourcePort, newSourcePort, visualNewSourcePort);
        assertTrue(status.isValid());
        assertEquals(newSourcePort, connectorEndSource.getRole(),
                "The new source port should have changed after the reconnect.");
        assertNull(connectorEndSource.getPartWithPort(), "The old part with port should have been removed");
    }

    /**
     * Test the source reconnection for a connector on a property port.
     */
    @Test
    public void testConnectorSourceReconnectOnPropertyPort() {
        // Create semantic elements
        Class class1 = this.create(Class.class);
        Port oldSourcePort = this.createIn(Port.class, class1);
        Port newSourcePort = this.createIn(Port.class, class1);
        Port targetPort = this.createIn(Port.class, class1);
        Property property = this.create(Property.class);
        property.setType(class1);
        Connector connector = this.create(Connector.class);
        ConnectorEnd connectorEndSource = this.createIn(ConnectorEnd.class, connector);
        connectorEndSource.setRole(oldSourcePort);
        connectorEndSource.setPartWithPort(property);
        ConnectorEnd connectorEndTarget = this.createIn(ConnectorEnd.class, connector);
        connectorEndTarget.setRole(targetPort);
        connectorEndTarget.setPartWithPort(property);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode propertyNode = visualTree.addChildren(property);
        VisualNode visualNewSourcePort = propertyNode.addChildren(newSourcePort);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(connector, oldSourcePort, newSourcePort, visualNewSourcePort);
        assertTrue(status.isValid());
        assertEquals(newSourcePort, connectorEndSource.getRole(),
                "The new source port should have changed after the reconnect.");
        assertEquals(property, connectorEndSource.getPartWithPort());
    }

    /**
     * Check the ControlFlow reconnect.
     */
    @Test
    public void testControlFlowSourceReconnect() {
        ControlFlow controlFlow = this.create(ControlFlow.class);
        this.testActivityEdgeSourceReconnect(controlFlow);
    }

    /**
     * Tests that the ControlFlow reconnect updates owner feature when the new
     * source is in a different {@link StructuredActivityNode}.
     */
    @Test
    public void testControlFlowSourceReconnectDifferentStructuredActivityNode() {
        ControlFlow controlFlow = this.create(ControlFlow.class);
        this.testActivityEdgeSourceReconnectDifferentStructuredActivityNode(controlFlow);
    }

    /**
     * Tests that the ControlFlow reconnect updates owner feature when the new
     * source is in the same {@link StructuredActivityNode} (and the previous source
     * was in a different {@link StructuredActivityNode}).
     */
    @Test
    public void testControlFlowSourceReconnectFromDifferentStructuredActivityNodeToSameStructuredActivityNode() {
        ControlFlow controlFlow = this.create(ControlFlow.class);
        this.testActivityEdgeSourceReconnectFromDifferentStructuredActivityNodeToSameStructuredActivityNode(
                controlFlow);
    }

    /**
     * Test that the ControlFlow reconnect also updates the InPartition feature.
     */
    @Test
    public void testControlFlowSourceReconnectInPartition() {
        ControlFlow controlFlow = this.create(ControlFlow.class);
        this.testActivityEdgeSourceReconnectInPartition(controlFlow);
    }

    /**
     * Test reconnect source of dependency on an other source in a different
     * package. Expected result : source changed and the dependency changes of owner
     * package
     */
    @Test
    public void testDependencySourceReconnectInDifferentPackage() {
        org.eclipse.uml2.uml.Package p1 = this.create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = this.createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = this.createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = this.createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        org.eclipse.uml2.uml.Package p2 = this.create(org.eclipse.uml2.uml.Package.class);
        org.eclipse.uml2.uml.Class c3 = this.createIn(org.eclipse.uml2.uml.Class.class, p2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(dependency, c1, c3, null);

        assertTrue(status.isValid());
        assertEquals(p2, dependency.getOwner());
        assertEquals(1, dependency.getClients().size());
        assertEquals(c3, dependency.getClients().get(0));
    }

    /**
     * Test reconnect source of dependency on an other source in the same package.
     * Expected result : source changed but the dependency remains in its owner
     * package
     */
    @Test
    public void testDependencySourceReconnectInSamePackage() {
        org.eclipse.uml2.uml.Package p1 = this.create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = this.createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = this.createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = this.createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        org.eclipse.uml2.uml.Class c3 = this.createIn(org.eclipse.uml2.uml.Class.class, p1);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(dependency, c1, c3, null);

        assertTrue(status.isValid());
        assertEquals(p1, dependency.getOwner());
        assertEquals(1, dependency.getClients().size());
        assertEquals(c3, dependency.getClients().get(0));
    }

    /**
     * Test reconnect source with null dependency or null old source or null new
     * source.
     */
    @Test
    public void testDependencySourceReconnectWithNullArguments() {
        org.eclipse.uml2.uml.Package p1 = this.create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = this.createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = this.createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = this.createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(null, c1, c2, null);

        assertFalse(status.isValid());

        status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(dependency, c1, null, null);

        assertFalse(status.isValid());

        status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(dependency, null, c1, null);

        assertFalse(status.isValid());

    }

    /**
     * Test reconnect source of {@link Deployment} on another
     * {@link DeployedArtifact} source.
     */
    @Test
    public void testDeploymentSourceReconnect() {
        Deployment deployment = this.create(Deployment.class);
        Artifact artifact1 = this.create(Artifact.class);
        Artifact artifact2 = this.create(Artifact.class);
        Node node = this.create(Node.class);
        deployment.getDeployedArtifacts().add(artifact1);
        deployment.setLocation(node);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(deployment, artifact1, artifact2, null);
        assertTrue(status.isValid());
        assertEquals(List.of(artifact2), deployment.getDeployedArtifacts());
    }

    /**
     * Test reconnect source of {@link Extend} on an other {@link UseCase} source.
     */
    @Test
    public void testExtendSourceReconnect() {
        UseCase source = this.create(UseCase.class);
        UseCase newSource = this.create(UseCase.class);

        Extend extend = this.create(Extend.class);
        extend.setExtension(source);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(extend, source, newSource, null);

        assertTrue(status.isValid());
        assertEquals(newSource, extend.getExtension());
    }

    /**
     * Test reconnect the {@link Extension} source.
     */
    @Test
    public void testExtensionSourceReconnect() {
        Profile profile = this.create(Profile.class);
        Stereotype stereotype = this.create(Stereotype.class);
        stereotype.setName(STEREOTYPE);
        Stereotype stereotype2 = this.create(Stereotype.class);
        stereotype2.setName(STEREOTYPE2);
        Class class1 = this.create(Class.class);
        class1.setName(CLASS1);
        Extension extension = this.createIn(Extension.class, profile);

        new ElementDomainBasedEdgeInitializer().initialize(extension, stereotype, class1, null, null, null);
        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(extension, extension.eContainer());
        assertEquals("E_stereotype_class11", extension.getName());
        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(extension, stereotype, stereotype2, null);
        assertTrue(status.isValid());
        assertEquals(stereotype2, extension.getStereotype());
        assertEquals("E_stereotype2_class11", extension.getName());

        // now check that the name is unchanged if the user has customized it.
        extension.setName("customName");
        status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(extension, stereotype2, stereotype, null);
        assertTrue(status.isValid());
        assertEquals(stereotype, extension.getStereotype());
        assertEquals("customName", extension.getName());
    }

    /**
     * Default source reconnection for {@link Generalization}.
     */
    @Test
    public void testGeneralizationSourceReconnect() {
        UseCase source = this.create(UseCase.class);
        UseCase source2 = this.create(UseCase.class);

        Generalization generalization = this.create(Generalization.class);
        generalization.setSpecific(source);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(generalization, source, source2, null);

        assertTrue(status.isValid());
        assertEquals(source2, generalization.getSpecific());
    }

    /**
     * Default source reconnection for {@link Include}.
     */
    @Test
    public void testIncludeSourceReconnect() {

        UseCase source = this.create(UseCase.class);
        UseCase source2 = this.create(UseCase.class);

        Include include = this.create(Include.class);
        include.setIncludingCase(source);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(include, source, source2, null);

        assertTrue(status.isValid());
        assertEquals(source2, include.getIncludingCase());
    }

    /**
     * Default source reconnection for {@link InformationFlow}.
     */
    @Test
    public void testInformationFlowSourceReconnect() {
        Package package1 = this.create(Package.class);
        Class target = this.createIn(Class.class, package1);
        Class source = this.createIn(Class.class, package1);
        Class newSource = this.createIn(Class.class, package1);
        InformationFlow informationFlow = this.createIn(InformationFlow.class, package1);
        new ElementDomainBasedEdgeInitializer().initialize(informationFlow, source, target, null, null, null);
        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(informationFlow, source, newSource, null);
        assertTrue(status.isValid());
        assertTrue(informationFlow.getInformationSources().size() == 1);
        assertTrue(informationFlow.getInformationSources().contains(newSource));
    }

    /**
     * Default source reconnection for {@link InterfaceRealization}.
     */
    @Test
    public void testInterfaceRealizationSourceReconnect() {

        Class classSource = this.create(Class.class);
        Class classSource2 = this.create(Class.class);
        Interface interfaceTarget = this.create(Interface.class);

        InterfaceRealization interfaceRealization = this.createIn(InterfaceRealization.class, classSource);
        interfaceRealization.setImplementingClassifier(classSource);
        interfaceRealization.setContract(interfaceTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(interfaceRealization, classSource, classSource2, null);
        assertTrue(status.isValid());
        assertEquals(classSource2, interfaceRealization.getImplementingClassifier());
        assertEquals(classSource2, interfaceRealization.eContainer());
    }

    /**
     * Default target reconnection for {@link Message}.
     */
    @Test
    public void testMessageSourceReconnect() {
        Lifeline target = this.create(Lifeline.class);
        Lifeline source = this.create(Lifeline.class);
        Lifeline newSource = this.create(Lifeline.class);
        Message message = this.create(Message.class);
        MessageOccurrenceSpecification sendEvent = UMLFactory.eINSTANCE.createMessageOccurrenceSpecification();
        sendEvent.setMessage(message);
        message.setSendEvent(sendEvent);
        new ElementDomainBasedEdgeInitializer().initialize(message, source, target, null, null, null);
        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(message, target, newSource, null);
        assertTrue(status.isValid());
        assertTrue(((MessageOccurrenceSpecification) message.getSendEvent()).getCovereds().get(0).equals(newSource));
    }

    /**
     * Check the ObjectFlow reconnect.
     */
    @Test
    public void testObjectFlowSourceReconnect() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        this.testActivityEdgeSourceReconnect(objectFlow);
    }

    /**
     * Tests that the ControlFlow reconnect updates owner feature when the new
     * source is in a different {@link StructuredActivityNode}.
     */
    @Test
    public void testObjectFlowSourceReconnectDifferentStructuredActivityNode() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        this.testActivityEdgeSourceReconnectDifferentStructuredActivityNode(objectFlow);
    }

    /**
     * Tests that the ControlFlow reconnect updates owner feature when the new
     * source is in the same {@link StructuredActivityNode} (and the previous source
     * was in a different {@link StructuredActivityNode}).
     */
    @Test
    public void testObjectFlowSourceReconnectFromDifferentStructuredActivityNodeToSameStructuredActivityNode() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        this.testActivityEdgeSourceReconnectFromDifferentStructuredActivityNodeToSameStructuredActivityNode(objectFlow);
    }

    /**
     * Test that the ObjectFlow reconnect also updates the InPartition feature.
     */
    @Test
    public void testObjectFlowSourceReconnectInPartition() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        this.testActivityEdgeSourceReconnectInPartition(objectFlow);
    }

    /**
     * Default source reconnection for {@link PackageImport}.
     */
    @Test
    public void testPackageImportSourceReconnect() {

        Package packSource = this.create(Package.class);
        Package packSource2 = this.create(Package.class);
        Package packTarget = this.create(Package.class);

        PackageImport packImport = this.createIn(PackageImport.class, packSource);
        packImport.setImportedPackage(packTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(packImport, packSource, packSource2, null);
        assertTrue(status.isValid());
        assertEquals(packSource2, packImport.getImportingNamespace());
        assertEquals(packSource2, packImport.eContainer());
    }

    /**
     * Default source reconnection for {@link PackageMerge}.
     */
    @Test
    public void testPackageMergeSourceReconnect() {

        Package packSource = this.create(Package.class);
        Package packSource2 = this.create(Package.class);
        Package packTarget = this.create(Package.class);

        PackageMerge merge = this.createIn(PackageMerge.class, packSource);
        merge.setMergedPackage(packTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(merge, packSource, packSource2, null);
        assertTrue(status.isValid());
        assertEquals(packSource2, merge.getReceivingPackage());
        assertEquals(packSource2, merge.eContainer());
    }

    /**
     * Default source reconnection for {@link Substitution}.
     */
    @Test
    public void testSubstitutionSourceReconnect() {
        Class oldSource = this.create(Class.class);
        Class newSource = this.create(Class.class);
        Class target = this.create(Class.class);
        Substitution substitution = this.create(Substitution.class);
        target.getSubstitutions().add(substitution);
        new ElementDomainBasedEdgeInitializer().initialize(substitution, oldSource, target, null, null, null);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(substitution, oldSource, newSource, null);

        assertTrue(status.isValid());
        assertEquals(newSource, substitution.getSubstitutingClassifier());
        assertEquals(newSource, substitution.eContainer());
    }

    /**
     * Default source reconnection for {@link Transition}.
     */
    @Test
    public void testTransitionSourceReconnect() {
        State state1 = this.create(State.class);
        Pseudostate source1 = this.createIn(Pseudostate.class, state1);
        Region region = this.createIn(Region.class, state1);
        Pseudostate source2 = this.createIn(Pseudostate.class, region);
        Pseudostate target = this.createIn(Pseudostate.class, region);

        Transition transition = this.createIn(Transition.class, region);
        transition.setSource(source1);
        transition.setTarget(target);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(transition, source1, source2, null);

        assertTrue(status.isValid());
        assertEquals(region, transition.getOwner());
        assertEquals(source2, transition.getSource());
        assertEquals(target, transition.getTarget());
    }

    private void testActivityEdgeSourceReconnect(ActivityEdge activityEdge) {
        ForkNode forkNode = this.create(ForkNode.class);
        DecisionNode decisionNode = this.create(DecisionNode.class);
        JoinNode joinNode = this.create(JoinNode.class);
        activityEdge.setSource(forkNode);
        activityEdge.setTarget(joinNode);
        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(activityEdge, forkNode, decisionNode, null);
        assertTrue(status.isValid());
        assertEquals(decisionNode, activityEdge.getSource());
    }

    private void testActivityEdgeSourceReconnectDifferentStructuredActivityNode(ActivityEdge activityEdge) {
        Activity rootActivity = this.create(Activity.class);
        StructuredActivityNode structuredActivityNode1 = this.createIn(StructuredActivityNode.class, rootActivity);
        StructuredActivityNode structuredActivityNode2 = this.createIn(StructuredActivityNode.class, rootActivity);
        DecisionNode decisionNode1 = this.createIn(DecisionNode.class, structuredActivityNode1);
        DecisionNode decisionNode2 = this.createIn(DecisionNode.class, structuredActivityNode1);
        DecisionNode decisionNode3 = this.createIn(DecisionNode.class, structuredActivityNode2);
        ElementDomainBasedEdgeInitializer initializer = new ElementDomainBasedEdgeInitializer();
        initializer.initialize(activityEdge, decisionNode1, decisionNode2, null, null, null);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(activityEdge, decisionNode1, decisionNode3, null);
        assertTrue(status.isValid());
        assertEquals(decisionNode3, activityEdge.getSource());
        assertEquals(rootActivity, activityEdge.getOwner());
    }

    private void testActivityEdgeSourceReconnectFromDifferentStructuredActivityNodeToSameStructuredActivityNode(
            ActivityEdge activityEdge) {
        Activity rootActivity = this.create(Activity.class);
        StructuredActivityNode structuredActivityNode1 = this.createIn(StructuredActivityNode.class, rootActivity);
        StructuredActivityNode structuredActivityNode2 = this.createIn(StructuredActivityNode.class, rootActivity);
        DecisionNode decisionNode1 = this.createIn(DecisionNode.class, structuredActivityNode1);
        DecisionNode decisionNode2 = this.createIn(DecisionNode.class, structuredActivityNode2);
        DecisionNode decisionNode3 = this.createIn(DecisionNode.class, structuredActivityNode2);
        ElementDomainBasedEdgeInitializer initializer = new ElementDomainBasedEdgeInitializer();
        initializer.initialize(activityEdge, decisionNode1, decisionNode3, null, null, null);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(activityEdge, decisionNode1, decisionNode2, null);
        assertTrue(status.isValid());
        assertEquals(decisionNode2, activityEdge.getSource());
        assertEquals(structuredActivityNode2, activityEdge.getOwner());
    }

    private void testActivityEdgeSourceReconnectInPartition(ActivityEdge activityEdge) {
        Activity activity = this.create(Activity.class);
        ActivityPartition activityPartition = this.createIn(ActivityPartition.class, activity);
        ActivityPartition activityPartition2 = this.createIn(ActivityPartition.class, activity);
        ForkNode forkNode = this.createIn(ForkNode.class, activity);
        forkNode.getInPartitions().add(activityPartition);
        JoinNode joinNode = this.createIn(JoinNode.class, activity);
        joinNode.getInPartitions().addAll(List.of(activityPartition, activityPartition2));
        activityEdge.setSource(forkNode);
        activityEdge.setTarget(joinNode);
        activityEdge.getInPartitions().add(activityPartition);

        // We try to reconnect on a source out of the activityPartition.
        DecisionNode decisionNode = this.create(DecisionNode.class);
        decisionNode.getInPartitions().add(activityPartition2);
        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE,
                new MockedViewQuerier()).reconnectSource(activityEdge, forkNode, decisionNode, null);
        assertTrue(status.isValid());
        assertEquals(decisionNode, activityEdge.getSource());
        assertEquals(1, activityEdge.getInPartitions().size());
        assertEquals(activityPartition2, activityEdge.getInPartitions().get(0));
    }

}
