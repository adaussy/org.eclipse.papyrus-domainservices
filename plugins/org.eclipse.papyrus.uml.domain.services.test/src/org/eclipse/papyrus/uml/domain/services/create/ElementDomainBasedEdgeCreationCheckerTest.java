/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.papyrus.uml.domain.services.utils.MockedViewQuerier;
import org.eclipse.papyrus.uml.domain.services.utils.VisualNode;
import org.eclipse.papyrus.uml.domain.services.utils.VisualTree;
import org.eclipse.uml2.uml.AcceptEventAction;
import org.eclipse.uml2.uml.ActionExecutionSpecification;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityFinalNode;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.ActivityParameterNode;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.ComponentRealization;
import org.eclipse.uml2.uml.ControlFlow;
import org.eclipse.uml2.uml.CreateObjectAction;
import org.eclipse.uml2.uml.DecisionNode;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.ExecutionSpecification;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.FlowFinalNode;
import org.eclipse.uml2.uml.ForkNode;
import org.eclipse.uml2.uml.Gate;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InitialNode;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.JoinNode;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.MergeNode;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Node;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.OpaqueAction;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Pin;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.ReclassifyObjectAction;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.StructuredActivityNode;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * This class tests the checking of DomainBasedEdge creation.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 *
 */
public class ElementDomainBasedEdgeCreationCheckerTest extends AbstractUMLTest {

    private static final String REALIZATION = "Realization";
    private static final String EXTENSION = "Extension";

    private static final String OWNED_ATTRIBUTE = "ownedAttribute";

    private static final String CONNECTOR = "Connector";

    private ElementDomainBasedEdgeCreationChecker elementDomainBasedEdgeCreationChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        this.elementDomainBasedEdgeCreationChecker = new ElementDomainBasedEdgeCreationChecker();
    }

    /**
     * Check the creation {@link Association} between {@link Classifier} and
     * {@link Classifier}.
     */
    @Test
    public void testCanCreateAssociatinOnClassifiers() {
        Actor actor1 = this.create(Actor.class);
        Actor actor2 = this.create(Actor.class);

        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(actor1, actor2, UMLPackage.eINSTANCE.getAssociation().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Association} between {@link Classifier} and non
     * {@link Classifier}.
     */
    @Test
    public void testCanCreateAssociationBetweenClassifiersAndNonClassifiers() {
        Actor actor = this.create(Actor.class);
        Comment comment = this.create(Comment.class);

        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(actor, comment, UMLPackage.eINSTANCE.getAssociation().getName(), null, null, null, null)
                .isValid());

        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getAssociation().getName(), null, null, null, null)
                .isValid());

        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, actor, UMLPackage.eINSTANCE.getAssociation().getName(), null, null, null, null)
                .isValid());
    }

    @Test
    public void testCanCreateCommunicationPathWithDeploymentTargetClassifiers() {
        Node source = this.create(Node.class);
        Node target = this.create(Node.class);
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(source, target,
                UMLPackage.eINSTANCE.getCommunicationPath().getName(), null, null, null, null).isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(source, source,
                UMLPackage.eINSTANCE.getCommunicationPath().getName(), null, null, null, null).isValid());
    }

    @Test
    public void testCanCreateCommunicationPathWithClassifierNonDeploymentTargetSource() {
        Class source = this.create(Class.class);
        Node target = this.create(Node.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(source, target,
                UMLPackage.eINSTANCE.getCommunicationPath().getName(), null, null, null, null).isValid());
    }

    @Test
    public void testCanCreateCommunicationPathWithClassifierNonDeploymentTargetTarget() {
        Node source = this.create(Node.class);
        Class target = this.create(Class.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(source, target,
                UMLPackage.eINSTANCE.getCommunicationPath().getName(), null, null, null, null).isValid());
    }

    @Test
    public void testCanCreateCommunicationPathWithDeploymentTargetNonClassifierSource() {
        InstanceSpecification source = this.create(InstanceSpecification.class);
        Node target = this.create(Node.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(source, target,
                UMLPackage.eINSTANCE.getCommunicationPath().getName(), null, null, null, null).isValid());
    }

    @Test
    public void testCanCreateCommunicationPathWithDeploymentTargetNonClassifierTarget() {
        Node source = this.create(Node.class);
        InstanceSpecification target = this.create(InstanceSpecification.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(source, target,
                UMLPackage.eINSTANCE.getCommunicationPath().getName(), null, null, null, null).isValid());
    }

    /**
     * Check the creation of a {@link ComponentRealization} with valid source and
     * target elements.
     */
    @Test
    public void testCanCreateComponentRealization() {
        Interface interfaceElement = this.create(Interface.class);
        Component component = this.create(Component.class);

        assertTrue(
                this.elementDomainBasedEdgeCreationChecker
                        .canCreate(interfaceElement, component,
                                UMLPackage.eINSTANCE.getComponentRealization().getName(), null, null, null, null)
                        .isValid());
    }

    /**
     * Checks that the creation of a {@link ComponentRealization} is forbidden on a
     * non-Classifier source.
     */
    @Test
    public void testCanCreateComponentRealizationNonClassifierSource() {
        Comment comment = this.create(Comment.class);
        Component component = this.create(Component.class);

        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(comment, component,
                UMLPackage.eINSTANCE.getComponentRealization().getName(), null, null, null, null).isValid());
    }

    /**
     * Checks that the creation of a {@link ComponentRealization} is forbidden on a
     * non-Component target.
     */
    @Test
    public void testCanCreateComponentRealizationNonComponentTarget() {
        Interface interfaceElement = this.create(Interface.class);
        Comment comment = this.create(Comment.class);

        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(interfaceElement, comment,
                UMLPackage.eINSTANCE.getComponentRealization().getName(), null, null, null, null).isValid());
    }

    /**
     * Checks that the creation of a {@link ComponentRealization} is not possible on
     * self (recursive realization).
     */
    @Test
    public void testCanCreateComponentRealizationOnSelf() {
        Component component = this.create(Component.class);

        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(component, component,
                UMLPackage.eINSTANCE.getComponentRealization().getName(), null, null, null, null).isValid());
    }

    /**
     * Check that creation is authorized when we try to create a connector between
     * two different properties not contained in each other.
     */
    @Test
    public void testCanCreateConnectorFrom2DifferentProperty() {
        // create semantic elements
        Class clazz = this.create(Class.class);
        Property property1 = this.createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        Property property2 = this.createIn(Property.class, clazz, OWNED_ATTRIBUTE);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualProperty1 = classNode.addChildren(property1);
        VisualNode visualProperty2 = classNode.addChildren(property2);

        // check creation is authorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeCreationChecker.canCreate(property1, property2,
                CONNECTOR, null, new MockedViewQuerier(), visualProperty1, visualProperty2);
        assertTrue(canCreateStatus.isValid());
    }

    /**
     * Check that creation is authorized when we try to create a realization between
     * two class, i.e. when no specific condition is necessary.
     */
    @Test
    public void testCanCreateConnectorFrom2DifferentProperty2() {
        // create semantic elements
        Model model = this.create(Model.class);
        Class clazz1 = this.create(Class.class);
        Class clazz2 = this.create(Class.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode rootNode = visualTree.addChildren(model);
        VisualNode visualClass1 = rootNode.addChildren(clazz1);
        VisualNode visualClass2 = rootNode.addChildren(clazz2);

        // check creation is authorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeCreationChecker.canCreate(clazz1, clazz2, REALIZATION,
                null, new MockedViewQuerier(), visualClass1, visualClass2);
        assertTrue(canCreateStatus.isValid());
    }

    /**
     * Check that creation is unauthorized when we try to create a port view to its
     * self.
     */
    @Test
    public void testCanCreateConnectorFromPortOnItself() {
        // create semantic elements
        Class clazz = this.create(Class.class);
        Port port = this.createIn(Port.class, clazz, OWNED_ATTRIBUTE);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualPort = classNode.addChildren(port);

        // check creation is unauthorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeCreationChecker.canCreate(port, port, CONNECTOR, null,
                new MockedViewQuerier(), visualPort, visualPort);
        assertFalse(canCreateStatus.isValid());
        assertEquals("Cannot connect a port to itself.", canCreateStatus.getMessage());
    }

    /**
     * Check that creation is unauthorized when we try to create a connector between
     * a port view and a property view with the port as border node.
     */
    @Test
    public void testCanCreateConnectorFromPortOnItsPropertyContainerNode() {
        // create semantic elements
        Class type1 = this.create(Class.class);
        Property property = this.create(Property.class);
        property.setType(type1);
        Port port = this.createIn(Port.class, type1, OWNED_ATTRIBUTE);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode propertyNode = visualTree.addChildren(property);
        VisualNode visualPort = propertyNode.addBorderNode(port);

        // check creation is unauthorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeCreationChecker.canCreate(port, property, CONNECTOR,
                null, new MockedViewQuerier(), visualPort, propertyNode);
        assertFalse(canCreateStatus.isValid());
        assertEquals("Cannot create a connector from a view representing a Part to its own Port (or the opposite).",
                canCreateStatus.getMessage());
    }

    /**
     * Check that creation is unauthorized when we try to create a connector between
     * a property view contained by an other one.
     */
    @Test
    public void testCanCreateConnectorFromPropertyContainedInAnotherOne() {
        // create semantic elements
        Class type1 = this.create(Class.class);
        Class clazz = this.create(Class.class);
        Property property1 = this.createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        property1.setType(type1);
        Property property2 = this.createIn(Property.class, type1, OWNED_ATTRIBUTE);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualProperty1 = classNode.addChildren(property1);
        VisualNode visualProperty2 = visualProperty1.addChildren(property2);

        // check creation is unauthorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeCreationChecker.canCreate(property1, property2,
                CONNECTOR, null, new MockedViewQuerier(), visualProperty1, visualProperty2);
        assertFalse(canCreateStatus.isValid());
        assertEquals(
                "Cannot connect a Part to one of its (possibly indirect) containment, must connect to one of its Port.",
                canCreateStatus.getMessage());
    }

    /**
     * Check the creation of an {@link ControlFlow} between {@link ActivityNode}.
     * This test only check the source with a target always valid.
     */
    @Test
    public void testCanCreateControlFlowSource() {
        // Check the basic case
        MergeNode mergeNode = this.create(MergeNode.class);
        OpaqueAction opaqueAction = this.create(OpaqueAction.class);
        Activity activity = this.create(Activity.class);
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(mergeNode, opaqueAction,
                UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null, null).isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreateFromSource(mergeNode, UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(activity, opaqueAction,
                UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null, null).isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreateFromSource(activity, UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null)
                .isValid());

        // Check that a FinalNode is not valid

        ActivityFinalNode activityFinalNode = this.create(ActivityFinalNode.class);
        FlowFinalNode flowFinalNode = this.create(FlowFinalNode.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(activityFinalNode, opaqueAction,
                UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null, null).isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreateFromSource(activityFinalNode,
                UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null).isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(flowFinalNode, opaqueAction,
                UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null, null).isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreateFromSource(flowFinalNode, UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null)
                .isValid());

        // Check with an ObjectNode

        InputPin inputPin = this.create(InputPin.class);
        inputPin.setIsControlType(false);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(inputPin, opaqueAction,
                UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null, null).isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreateFromSource(inputPin, UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null)
                .isValid());
        inputPin.setIsControlType(true);
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(inputPin, opaqueAction,
                UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null, null).isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreateFromSource(inputPin, UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null)
                .isValid());

    }

    /**
     * Check the creation of an {@link ControlFlow} between {@link ActivityNode}.
     * This test only check the target with a source always valid.
     */
    @Test
    public void testCanCreateControlFlowTarget() {
        // Check the basic case
        MergeNode mergeNode = this.create(MergeNode.class);
        OpaqueAction opaqueAction = this.create(OpaqueAction.class);
        Activity activity = this.create(Activity.class);
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(opaqueAction, mergeNode,
                UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null, null).isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(opaqueAction, activity,
                UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null, null).isValid());

        // Check that an InitialNode is not valid

        InitialNode initialNode = this.create(InitialNode.class);
        FlowFinalNode flowFinalNode = this.create(FlowFinalNode.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(opaqueAction, initialNode,
                UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null, null).isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(opaqueAction, flowFinalNode,
                UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null, null).isValid());

        // Check with an ObjectNode

        InputPin inputPin = this.create(InputPin.class);
        inputPin.setIsControlType(false);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(opaqueAction, inputPin,
                UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null, null).isValid());
        inputPin.setIsControlType(true);
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(opaqueAction, inputPin,
                UMLPackage.eINSTANCE.getControlFlow().getName(), null, null, null, null).isValid());
    }

    /**
     * Check the creation {@link Manifestation} between {@link NamedElement} and
     * {@link PackageableElement}.
     */
    @Test
    public void testCanCreateDependency() {
        Actor actor = this.create(Actor.class);
        Interaction interaction = this.create(Interaction.class);

        // Between {@link NamedElement} => OK
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(actor, interaction, UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null)
                .isValid());

        // Source and target can be different
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(actor, actor, UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null)
                .isValid());

        // Source or target is not a NamedElement
        Comment comment = this.create(Comment.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(actor, comment, UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, actor, UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Usage} between {@link NamedElement} and non
     * {@link NamedElement}.
     */
    @Test
    public void testCanCreateDependencyWithNonNamedElement() {
        Actor actor1 = this.create(Actor.class);
        Interaction interaction2 = this.create(Interaction.class);

        // Source or target is not a NamedElement element.
        Comment comment = this.create(Comment.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(actor1, comment, UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(comment, interaction2,
                UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null).isValid());
    }

    /**
     * Check the creation of {@link Deployment}.
     */
    @Test
    public void testCanCreateDeployment() {
        InstanceSpecification instanceSpecification = this.create(InstanceSpecification.class);
        Property property = this.create(Property.class);

        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(instanceSpecification, property,
                UMLPackage.eINSTANCE.getDeployment().getName(), null, null, null, null).isValid());
        // Source isn't a DeployedArtifact
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(property, instanceSpecification,
                UMLPackage.eINSTANCE.getDeployment().getName(), null, null, null, null).isValid());
        Comment comment = this.create(Comment.class);
        // Target isn't a DeploymentTarget
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(instanceSpecification, comment,
                UMLPackage.eINSTANCE.getDeployment().getName(), null, null, null, null).isValid());
        // Same source and target is not allowed
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(instanceSpecification, instanceSpecification,
                UMLPackage.eINSTANCE.getDeployment().getName(), null, null, null, null).isValid());
    }

    /**
     * Check the creation {@link Extend} between {@link UseCase}.
     */
    @Test
    public void testCanCreateExtend() {
        UseCase useCase1 = this.create(UseCase.class);
        UseCase useCase2 = this.create(UseCase.class);

        // Between {@link UseCase} => OK
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(useCase1, useCase2, UMLPackage.eINSTANCE.getExtend().getName(), null, null, null, null)
                .isValid());

        // Source and target should be different
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(useCase1, useCase1, UMLPackage.eINSTANCE.getExtend().getName(), null, null, null, null)
                .isValid());

        // Source or target is not a UseCase
        Comment comment = this.create(Comment.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(useCase1, comment, UMLPackage.eINSTANCE.getExtend().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getExtend().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, useCase2, UMLPackage.eINSTANCE.getExtend().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the canCreate on Extension.
     */
    @Test
    public void testCanCreateExtension() {
        // create semantic elements
        Stereotype stereotype = this.create(Stereotype.class);
        Class clazz1 = this.create(Class.class);
        PrimitiveType primitiveType = this.create(PrimitiveType.class);
        // check creation is authorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeCreationChecker.canCreate(stereotype, clazz1,
                EXTENSION, null, new MockedViewQuerier(), null, null);
        assertTrue(canCreateStatus.isValid());
        // check creation is not authorized with wrong source type
        canCreateStatus = this.elementDomainBasedEdgeCreationChecker.canCreate(primitiveType, clazz1, EXTENSION, null,
                new MockedViewQuerier(), null, null);
        assertFalse(canCreateStatus.isValid());
        // check creation is not authorized with wrong target type
        canCreateStatus = this.elementDomainBasedEdgeCreationChecker.canCreate(stereotype, primitiveType, EXTENSION,
                null, new MockedViewQuerier(), null, null);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Check the creation {@link Generalization} between classifier.
     */
    @Test
    public void testCanCreateGeneralizationOnClassifiers() {
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);

        // Between 2 Classifier => OK
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(c1, c2, UMLPackage.eINSTANCE.getGeneralization().getName(), null, null, null, null)
                .isValid());

        // Source and target should be different
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(c1, c1, UMLPackage.eINSTANCE.getGeneralization().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Generalization} between non classifier.
     */
    @Test
    public void testCanCreateGeneralizationOnNonClassifiers() {
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);

        // Source or target is not a Classifer
        Comment comment = this.create(Comment.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(c1, comment, UMLPackage.eINSTANCE.getGeneralization().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getGeneralization().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, c2, UMLPackage.eINSTANCE.getGeneralization().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Include} between {@link UseCase}.
     */
    @Test
    public void testCanCreateInclude() {
        UseCase useCase1 = this.create(UseCase.class);
        UseCase useCase2 = this.create(UseCase.class);

        // Between {@link UseCase} => OK
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(useCase1, useCase2, UMLPackage.eINSTANCE.getInclude().getName(), null, null, null, null)
                .isValid());

        // Source and target should be different
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(useCase1, useCase1, UMLPackage.eINSTANCE.getInclude().getName(), null, null, null, null)
                .isValid());

        // Source or target is not a UseCase
        Comment comment = this.create(Comment.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(useCase1, comment, UMLPackage.eINSTANCE.getInclude().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getInclude().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, useCase2, UMLPackage.eINSTANCE.getInclude().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Include} between non {@link UseCase} element.
     */
    @Test
    public void testCanCreateIncludeBetweenNonUseCase() {
        Actor actor = this.create(Actor.class);
        Package pack = this.create(Package.class);

        // Between non {@link UseCase} => KO
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(actor, pack, UMLPackage.eINSTANCE.getInclude().getName(), null, null, null, null).isValid());

        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(pack, actor, UMLPackage.eINSTANCE.getInclude().getName(), null, null, null, null).isValid());
    }

    /**
     * Check the creation of {@link InformationFlow} between {@link NamedElement}.
     */
    @Test
    public void testCanCreateInformationFlowOnNamedElements() {
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);

        // Between 2 Named element => OK
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(c1, c2, UMLPackage.eINSTANCE.getInformationFlow().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Generalization} between non {@link NamedElement}.
     */
    @Test
    public void testCanCreateInformationFlowOnNonNamedElements() {
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);

        // Source or target is not a {@link NamedElement}
        Comment comment = this.create(Comment.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(c1, comment, UMLPackage.eINSTANCE.getInformationFlow().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(comment, comment,
                UMLPackage.eINSTANCE.getInformationFlow().getName(), null, null, null, null).isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, c2, UMLPackage.eINSTANCE.getInformationFlow().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Manifestation} between {@link NamedElement} and
     * {@link PackageableElement}.
     */
    @Test
    public void testCanCreateManifestationBetweenNamedElementAndPackageableElement() {
        Class c1 = this.create(Class.class);
        Usage u1 = this.create(Usage.class);

        // Between one {@link NamedElement} and one {@link PackageableElement} => OK
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(c1, u1, UMLPackage.eINSTANCE.getManifestation().getName(), null, null, null, null)
                .isValid());

        // Source and target should be different
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(c1, c1, UMLPackage.eINSTANCE.getManifestation().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Manifestation} between non {@link NamedElement} and
     * non {@link PackageableElement}.
     */
    @Test
    public void testCanCreateManifestationBetweenNonNamedElementAndNonPackageableElement() {
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);

        // Source or target is not a NamedElement or not Packageable element.
        Comment comment = this.create(Comment.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(c1, comment, UMLPackage.eINSTANCE.getManifestation().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getManifestation().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, c2, UMLPackage.eINSTANCE.getManifestation().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Checks the creation of a {@link Message} between a
     * {@link ExecutionSpecification} and a {@link ExecutionSpecification}.
     */
    @Test
    public void testCanCreateMessageBetweenExecutionSpecificationAndExecutionSpecification() {
        ExecutionSpecification executionSpecification1 = this.create(ActionExecutionSpecification.class);
        ExecutionSpecification executionSpecification2 = this.create(ActionExecutionSpecification.class);
        assertTrue(
                this.elementDomainBasedEdgeCreationChecker.canCreate(executionSpecification1, executionSpecification2,
                        UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null).isValid());
        assertTrue(
                this.elementDomainBasedEdgeCreationChecker.canCreate(executionSpecification2, executionSpecification1,
                        UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null).isValid());
        assertTrue(
                this.elementDomainBasedEdgeCreationChecker.canCreate(executionSpecification1, executionSpecification1,
                        UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null).isValid());
    }

    /**
     * Checks the creation of a {@link Message} between a
     * {@link ExecutionSpecification} and a non {@link ExecutionSpecification}.
     */
    @Test
    public void testCanCreateMessageBetweenExecutionSpecificationAndNonExecutionSpecification() {
        ExecutionSpecification executionSpecification = this.create(ActionExecutionSpecification.class);
        Comment comment = this.create(Comment.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(executionSpecification, comment,
                UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null).isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(comment, executionSpecification,
                UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null).isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Checks the creation of a {@link Message} between a {@link Gate} and a
     * {@link Gate}.
     */
    @Test
    public void testCanCreateMessageBetweenGateAndGate() {
        Gate gate1 = this.create(Gate.class);
        Gate gate2 = this.create(Gate.class);
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(gate1, gate2, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(gate2, gate1, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(gate1, gate1, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Checks the creation of a {@link Message} between a {@link Gate} and a non
     * {@link Gate}.
     */
    @Test
    public void testCanCreateMessageBetweenGateAndNonGate() {
        Gate gate = this.create(Gate.class);
        Comment comment = this.create(Comment.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(gate, comment, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, gate, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Checks the creation of a {@link Message} between a {@link Interaction} and a
     * {@link Interaction}.
     */
    @Test
    public void testCanCreateMessageBetweenInteractionAndInteraction() {
        Interaction interaction1 = this.create(Interaction.class);
        Interaction interaction2 = this.create(Interaction.class);
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(interaction1, interaction2,
                UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null).isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(interaction2, interaction1,
                UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null).isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(interaction1, interaction1,
                UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null).isValid());
    }

    /**
     * Checks the creation of a {@link Message} between a {@link Interaction} and a
     * non {@link Interaction}.
     */
    @Test
    public void testCanCreateMessageBetweenInteractionAndNonInteraction() {
        Interaction interaction = this.create(Interaction.class);
        Comment comment = this.create(Comment.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(interaction, comment, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, interaction, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Checks the creation of a {@link Message} between a {@link Lifeline} and a non
     * {@link Lifeline}.
     */
    @Test
    public void testCanCreateMessageBetweenLifelineAndNonLifeline() {
        Lifeline lifeline = this.create(Lifeline.class);
        Comment comment = this.create(Comment.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(lifeline, comment, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, lifeline, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Checks the creation of a {@link Message} between a {@link Lifeline} and a
     * {@link Lifeline}.
     */
    @Test
    public void testCanCreateMessageBetweenLifelines() {
        Lifeline lifeline1 = this.create(Lifeline.class);
        Lifeline lifeline2 = this.create(Lifeline.class);
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(lifeline1, lifeline2, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(lifeline2, lifeline1, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(lifeline1, lifeline1, UMLPackage.eINSTANCE.getMessage().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation of an {@link ObjectFlow} between {@link ActivityNode}.
     * This test only check the source with a target always valid.
     */
    @Test
    public void testCanCreateObjectFlowSource() {
        CreateObjectAction createObjectAction = this.create(CreateObjectAction.class);
        ReclassifyObjectAction reclassifyObjectAction = this.create(ReclassifyObjectAction.class);
        OutputPin outputPin = this.createIn(OutputPin.class, createObjectAction);
        InputPin inputPin = this.createIn(InputPin.class, reclassifyObjectAction);

        // We check the basic case always authorized.
        this.checkBasicCase(outputPin, inputPin);

        // Check that if the source is an InputPin, then the parent has to be a
        // StructuredActivityNode and the target should be a Pin contained in the same
        // parent.

        this.checkInputPinAsSource(inputPin);

        // Check that only OpaqueAction is authorized.
        this.checkOpaqueAction(inputPin, true);

        // Check that the InitalNode and the FinalNode are not authorized.

        this.checkFinalNodeAsSource(inputPin);
        this.checkInitialNode(inputPin, true);

        // Check that the JoinNode is authorized only if no outgoing edges exist.
        this.checkJoinNodeAsSource(inputPin);

        // Check that the ForkNode and the DecisionNode are authorized if no ControlFlow
        // is linked to them.

        this.checkForkNodeAndDecisionNodeAsSource(inputPin);

        // Check that the MergeNode is authorized if there is no existing outgoing edges
        // and no ControlFlow is linked to it.
        this.checkMergeNodeAsSource(inputPin);

        // check that the ActivityParameterNode is authorized if there is no incoming
        // edges.

        this.checkActivityParameterNodeAsSource(inputPin);

    }

    /**
     * Check the creation of an {@link ObjectFlow} between {@link ActivityNode}.
     * This test only check the target with a source always valid.
     */
    @Test
    public void testCanCreateObjectFlowTarget() {
        CreateObjectAction createObjectAction = this.create(CreateObjectAction.class);
        OutputPin outputPin = this.createIn(OutputPin.class, createObjectAction);

        // Check that if the source is an InputPin, then the parent has to be a
        // StructuredActivityNode and the target should be a Pin contained in the same
        // parent.

        this.checkOutputPinAsTarget(outputPin);

        // Check that only OpaqueAction is authorized.
        this.checkOpaqueAction(outputPin, false);

        // Check that the InitalNode is not authorized.

        this.checkInitialNode(outputPin, false);

        // Check that the JoinNode is authorized only if no outgoing ControlFlow exist.
        this.checkJoinNodeAsTarget(outputPin);

        // Check that the ForkNode is authorized if there is no existing outgoing edges
        // and no ControlFlow is linked to it.

        this.checkForkNodeAsTarget(outputPin);

        // Check that the MergeNode is authorized if there is no existing outgoing edges
        // and no ControlFlow is linked to them.
        this.checkMergeNodeAsTarget(outputPin);

        // Check that the DecisionNode is authorized if there is no already two incoming
        // edges. It should also be unauthorized if there is already incoming or
        // outgoing ControlFlow and at least one incoming objectFlow.

        DecisionNode decisionNode = this.create(DecisionNode.class);
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(outputPin, decisionNode,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());
        ControlFlow controlFlow = this.create(ControlFlow.class);
        decisionNode.getIncomings().add(controlFlow);
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(outputPin, decisionNode,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());
        ControlFlow controlFlow2 = this.create(ControlFlow.class);
        decisionNode.getIncomings().add(controlFlow2);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(outputPin, decisionNode,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());
        decisionNode.getIncomings().clear();
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(outputPin, decisionNode,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());
        decisionNode.getOutgoings().add(controlFlow);
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        decisionNode.getIncomings().add(objectFlow);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(outputPin, decisionNode,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());

        // check that ActivityParameterNode is authorized if there is no incoming edges.

        this.checkActivityParameterNodeAsTarget(outputPin);

    }

    /**
     * Check the creation {@link PackageImport} between {@link Package}.
     */
    @Test
    public void testCanCreatePackageImport() {
        Package pack1 = this.create(Package.class);
        Package pack2 = this.create(Package.class);

        // Between {@link Package} => OK
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(pack1, pack2, UMLPackage.eINSTANCE.getPackageImport().getName(), null, null, null, null)
                .isValid());

        // Source and target can be different
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(pack1, pack2, UMLPackage.eINSTANCE.getPackageImport().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link PackageMerge} between a {@link Package} and a
     * {@link NameSpace} element.
     */
    @Test
    public void testCanCreatePackageImportWithNonPackageElement() {
        Package pack = this.create(Package.class);
        Actor actor = this.create(Actor.class);
        Comment comment = this.create(Comment.class);

        // Between {@link Package} and non {@link Package} => KO
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(pack, actor, UMLPackage.eINSTANCE.getPackageImport().getName(), null, null, null, null)
                .isValid());

        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(actor, pack, UMLPackage.eINSTANCE.getPackageImport().getName(), null, null, null, null)
                .isValid());

        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, pack, UMLPackage.eINSTANCE.getPackageImport().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link PackageMerge} between {@link Package}.
     */
    @Test
    public void testCanCreatePackageMerge() {
        Package pack1 = this.create(Package.class);
        Package pack2 = this.create(Package.class);

        // Between {@link Package} => OK
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(pack1, pack2, UMLPackage.eINSTANCE.getPackageMerge().getName(), null, null, null, null)
                .isValid());

        // Source and target can be different
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(pack1, pack2, UMLPackage.eINSTANCE.getPackageMerge().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link PackageMerge} with a non {@link Package} element.
     */
    @Test
    public void testCanCreatePackageMergeWithNonPackageElement() {
        Package pack = this.create(Package.class);
        Actor actor = this.create(Actor.class);

        // Between {@link Package} and non {@link Package} => KO
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(pack, actor, UMLPackage.eINSTANCE.getPackageMerge().getName(), null, null, null, null)
                .isValid());

        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(actor, pack, UMLPackage.eINSTANCE.getPackageMerge().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Substitution} between {@link Classifier}.
     */
    @Test
    public void testCanCreateSubstitutionOnClassifiers() {
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);

        // Between 2 Classifier => OK
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(c1, c2, UMLPackage.eINSTANCE.getSubstitution().getName(), null, null, null, null).isValid());

        // Source and target should be different
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(c1, c1, UMLPackage.eINSTANCE.getSubstitution().getName(), null, null, null, null).isValid());
    }

    /**
     * Check the creation {@link Substitution} between non {@link Classifier} source
     * and/or target.
     */
    @Test
    public void testCanCreateSubstitutionOnNonClassifiers() {
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);

        // Source or target is not a Classifer
        Comment comment = this.create(Comment.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(c1, comment, UMLPackage.eINSTANCE.getSubstitution().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getSubstitution().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(comment, c2, UMLPackage.eINSTANCE.getSubstitution().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Manifestation} between non {@link NamedElement} and
     * non {@link PackageableElement}.
     */
    @Test
    public void testCanCreateTransition() {
        Vertex v1 = this.create(State.class);
        Vertex v2 = this.create(State.class);

        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(v1, v2, UMLPackage.eINSTANCE.getTransition().getName(),
                        UMLPackage.eINSTANCE.getRegion_Transition().getName(), null, null, null)
                .isValid());

        // source is not a vertex
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(this.create(Class.class), v2, UMLPackage.eINSTANCE.getTransition().getName(),
                        UMLPackage.eINSTANCE.getRegion_Transition().getName(), null, null, null)
                .isValid());
        // target is not a vertex
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(v1, this.create(Class.class), UMLPackage.eINSTANCE.getTransition().getName(),
                        UMLPackage.eINSTANCE.getRegion_Transition().getName(), null, null, null)
                .isValid());
    }

    private void checkActivityParameterNodeAsSource(InputPin inputPin) {
        ActivityParameterNode activityParameterNode = this.create(ActivityParameterNode.class);
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(activityParameterNode, inputPin,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());
        ControlFlow controlFlow = this.create(ControlFlow.class);
        activityParameterNode.getIncomings().add(controlFlow);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(activityParameterNode, inputPin,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());
    }

    private void checkActivityParameterNodeAsTarget(OutputPin outputPin) {
        ActivityParameterNode activityParameterNode = this.create(ActivityParameterNode.class);
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(outputPin, activityParameterNode,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());
        ControlFlow controlFlow = this.create(ControlFlow.class);
        activityParameterNode.getOutgoings().add(controlFlow);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(outputPin, activityParameterNode,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());
    }

    private void checkBasicCase(OutputPin outputPin, InputPin inputPin) {
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(outputPin, inputPin, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreateFromSource(outputPin, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null)
                .isValid());
    }

    private void checkFinalNodeAsSource(InputPin inputPin) {
        FlowFinalNode finalNode = this.create(FlowFinalNode.class);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(finalNode, inputPin, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreateFromSource(finalNode, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null)
                .isValid());
    }

    private void checkForkNodeAndDecisionNodeAsSource(InputPin inputPin) {
        ControlFlow controlFlow = this.create(ControlFlow.class);
        ForkNode forkNode = this.create(ForkNode.class);
        DecisionNode decisionNode = this.create(DecisionNode.class);
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(forkNode, inputPin, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreate(decisionNode, inputPin,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());
        forkNode.getIncomings().add(controlFlow);
        ControlFlow controlFlow2 = this.create(ControlFlow.class);
        decisionNode.getIncomings().add(controlFlow2);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(forkNode, inputPin, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(decisionNode, inputPin,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());
        forkNode.getIncomings().clear();
        decisionNode.getIncomings().clear();
        forkNode.getOutgoings().add(controlFlow);
        decisionNode.getOutgoings().add(controlFlow2);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(forkNode, inputPin, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(decisionNode, inputPin,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());
    }

    private void checkForkNodeAsTarget(OutputPin outputPin) {
        ControlFlow controlFlow = this.create(ControlFlow.class);
        ForkNode forkNode = this.create(ForkNode.class);
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(outputPin, forkNode, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        forkNode.getIncomings().add(controlFlow);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(outputPin, forkNode, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        forkNode.getIncomings().clear();
        forkNode.getOutgoings().add(controlFlow);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(outputPin, forkNode, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
    }

    private void checkInitialNode(Pin pin, boolean isSource) {
        InitialNode initialNode = this.create(InitialNode.class);
        EObject source;
        EObject target;
        if (isSource) {
            target = pin;
            source = initialNode;
        } else {
            source = pin;
            target = initialNode;
        }
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(source, target, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
    }

    private void checkInputPinAsSource(InputPin inputPin) {
        StructuredActivityNode structuredActivityNode = this.create(StructuredActivityNode.class);
        InputPin inputPinInStructuredActivityNode = this.createIn(InputPin.class, structuredActivityNode);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(inputPinInStructuredActivityNode, inputPin,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreateFromSource(inputPinInStructuredActivityNode,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null).isValid());
        InputPin inputPinInStructuredActivityNode2 = this.createIn(InputPin.class, structuredActivityNode);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(inputPin, inputPinInStructuredActivityNode2,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreateFromSource(inputPin, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null)
                .isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(inputPinInStructuredActivityNode, inputPinInStructuredActivityNode2,
                        UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker.canCreateFromSource(inputPinInStructuredActivityNode,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null).isValid());
    }

    private void checkJoinNodeAsSource(InputPin inputPin) {
        JoinNode joinNode = this.create(JoinNode.class);
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(joinNode, inputPin, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        ControlFlow controlFlow = this.create(ControlFlow.class);
        joinNode.getOutgoings().add(controlFlow);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(joinNode, inputPin, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
    }

    private void checkJoinNodeAsTarget(OutputPin outputPin) {
        JoinNode joinNode = this.create(JoinNode.class);
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(outputPin, joinNode, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        ControlFlow controlFlow = this.create(ControlFlow.class);
        joinNode.getOutgoings().add(controlFlow);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(outputPin, joinNode, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        joinNode.getOutgoings().clear();
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        joinNode.getOutgoings().add(objectFlow);
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(outputPin, joinNode, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
    }

    private void checkMergeNodeAsSource(InputPin inputPin) {
        MergeNode mergeNode = this.create(MergeNode.class);
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(mergeNode, inputPin, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        ControlFlow controlFlow = this.create(ControlFlow.class);
        mergeNode.getIncomings().add(controlFlow);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(mergeNode, inputPin, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        mergeNode.getIncomings().clear();
        mergeNode.getOutgoings().add(controlFlow);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(mergeNode, inputPin, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
    }

    private void checkMergeNodeAsTarget(OutputPin outputPin) {
        MergeNode mergeNode = this.create(MergeNode.class);
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(outputPin, mergeNode, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        ControlFlow controlFlow = this.create(ControlFlow.class);
        mergeNode.getIncomings().add(controlFlow);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(outputPin, mergeNode, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        mergeNode.getIncomings().clear();
        mergeNode.getOutgoings().add(controlFlow);
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(outputPin, mergeNode, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
    }

    private void checkOpaqueAction(Pin pin, boolean isSource) {
        OpaqueAction opaqueAction = this.create(OpaqueAction.class);
        AcceptEventAction acceptEventAction = this.create(AcceptEventAction.class);
        EObject source;
        EObject target;
        if (isSource) {
            target = pin;
            source = opaqueAction;
        } else {
            source = pin;
            target = opaqueAction;
        }
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(source, target, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
        if (isSource) {
            source = acceptEventAction;
        } else {
            target = acceptEventAction;
        }
        assertFalse(this.elementDomainBasedEdgeCreationChecker
                .canCreate(source, target, UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
    }

    private void checkOutputPinAsTarget(OutputPin outputPin) {
        StructuredActivityNode structuredActivityNode = this.create(StructuredActivityNode.class);
        OutputPin outputPinInStructuredActivityNode = this.createIn(OutputPin.class, structuredActivityNode);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(outputPin, outputPinInStructuredActivityNode,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());
        OutputPin outputPinInStructuredActivityNode2 = this.createIn(OutputPin.class, structuredActivityNode);
        assertFalse(this.elementDomainBasedEdgeCreationChecker.canCreate(outputPinInStructuredActivityNode2, outputPin,
                UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null).isValid());
        assertTrue(this.elementDomainBasedEdgeCreationChecker
                .canCreate(outputPinInStructuredActivityNode2, outputPinInStructuredActivityNode,
                        UMLPackage.eINSTANCE.getObjectFlow().getName(), null, null, null, null)
                .isValid());
    }
}
