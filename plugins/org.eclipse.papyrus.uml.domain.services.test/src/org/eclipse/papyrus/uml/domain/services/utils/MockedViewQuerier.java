/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;

public class MockedViewQuerier implements IViewQuerier {

    private final Object diagram;

    public MockedViewQuerier() {
        this.diagram = null;
    }

    public MockedViewQuerier(Object diagram) {
        this.diagram = diagram;
    }

    @Override
    public Object getVisualParent(Object view) {
        if (view instanceof VisualNode) {
            VisualNode node = (VisualNode) view;
            return node.getParent();
        }
        return null;
    }

    @Override
    public List<? extends Object> getVisualAncestorNodes(Object view) {
        if (view instanceof VisualNode) {
            VisualNode node = (VisualNode) view;

            List<VisualNode> ancestors = new ArrayList<>();
            VisualNode currentParent = node.getParent();

            while (currentParent != null) {
                ancestors.add(currentParent);
                currentParent = currentParent.getParent();
            }
            return ancestors;
        }

        return null;
    }

    @Override
    public EObject getSemanticElement(Object view) {
        if (view instanceof VisualNode) {
            VisualNode node = (VisualNode) view;
            return node.getSemanticElement();
        }
        return null;
    }

    @Override
    public List<? extends Object> getChildrenNodes(Object view) {
        if (view instanceof VisualNode) {
            VisualNode node = (VisualNode) view;
            return node.getChildren();
        }
        return null;
    }

    @Override
    public List<? extends Object> getBorderedNodes(Object view) {
        if (view instanceof VisualNode) {
            VisualNode node = (VisualNode) view;
            return node.getBorderNodes();
        }
        return null;
    }

    @Override
    public Object getDiagram() {
        return diagram;
    }

}
