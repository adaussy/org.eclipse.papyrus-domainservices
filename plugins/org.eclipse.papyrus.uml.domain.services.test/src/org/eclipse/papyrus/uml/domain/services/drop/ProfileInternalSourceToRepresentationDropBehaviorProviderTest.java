/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.ProfileInternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.junit.jupiter.api.Test;

/**
 * Test class for
 * {@link ProfileInternalSourceToRepresentationDropBehaviorProvider}.
 *
 * @author fbarbin
 *
 */
public class ProfileInternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    /**
     * Test dropping a {@link Class}.
     */
    @Test
    public void testClassDrop() {
        Package oldContainer = this.create(Package.class);
        Package newContainer = this.create(Package.class);
        org.eclipse.uml2.uml.Class clazz = this.createIn(org.eclipse.uml2.uml.Class.class, oldContainer);

        assertTrue(oldContainer.getPackagedElements().contains(clazz));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(clazz, oldContainer, newContainer,
                this.getCrossRef(), this.getEditableChecker());
        assertFalse(oldContainer.getPackagedElements().contains(clazz));
        assertTrue(newContainer.getPackagedElements().contains(clazz));
    }

    /**
     * Test dropping a {@link Comment}.
     */
    @Test
    public void testCommentDrop() {
        Package oldContainer = this.create(Package.class);
        Package newContainer = this.create(Package.class);
        Comment comment = this.createIn(Comment.class, oldContainer);

        assertTrue(oldContainer.getOwnedComments().contains(comment));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(comment, oldContainer, newContainer,
                this.getCrossRef(), this.getEditableChecker());
        assertFalse(oldContainer.getOwnedComments().contains(comment));
        assertTrue(newContainer.getOwnedComments().contains(comment));
    }

    /**
     * Test dropping a {@link Constraint}.
     */
    @Test
    public void testConstraintDrop() {
        Package oldContainer = this.create(Package.class);
        Package newContainer = this.create(Package.class);
        Constraint constraint = this.createIn(Constraint.class, oldContainer);

        assertTrue(oldContainer.getOwnedRules().contains(constraint));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(constraint, oldContainer, newContainer,
                this.getCrossRef(), this.getEditableChecker());
        assertFalse(oldContainer.getOwnedRules().contains(constraint));
        assertTrue(newContainer.getOwnedRules().contains(constraint));

    }

    /**
     * Test dropping a {@link DataType}.
     */
    @Test
    public void testDataTypeDrop() {
        Package oldContainer = this.create(Package.class);
        Package newContainer = this.create(Package.class);
        DataType dataType = this.createIn(DataType.class, oldContainer);

        assertTrue(oldContainer.getPackagedElements().contains(dataType));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(dataType, oldContainer, newContainer,
                this.getCrossRef(), this.getEditableChecker());
        assertFalse(oldContainer.getPackagedElements().contains(dataType));
        assertTrue(newContainer.getPackagedElements().contains(dataType));
    }

    /**
     * Test dropping a {@link ElementImport}.
     */
    @Test
    public void testElementImportDrop() {
        Profile oldContainer = this.create(Profile.class);
        Profile newContainer = this.create(Profile.class);
        ElementImport elementImport = this.createIn(ElementImport.class, oldContainer);

        assertTrue(oldContainer.getElementImports().contains(elementImport));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(elementImport, oldContainer, newContainer,
                this.getCrossRef(), this.getEditableChecker());
        assertFalse(oldContainer.getElementImports().contains(elementImport));
        assertTrue(newContainer.getElementImports().contains(elementImport));
    }

    /**
     * Test dropping a {@link Enumeration}.
     */
    @Test
    public void testEnumerationDrop() {
        Package oldContainer = this.create(Package.class);
        Package newContainer = this.create(Package.class);
        Enumeration enumeration = this.createIn(Enumeration.class, oldContainer);

        assertTrue(oldContainer.getPackagedElements().contains(enumeration));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(enumeration, oldContainer, newContainer,
                this.getCrossRef(), this.getEditableChecker());
        assertFalse(oldContainer.getPackagedElements().contains(enumeration));
        assertTrue(newContainer.getPackagedElements().contains(enumeration));
    }

    /**
     * Test dropping a {@link EnumerationLiteral}.
     */
    @Test
    public void testEnumerationLiteralDrop() {
        Enumeration oldContainer = this.create(Enumeration.class);
        Enumeration newContainer = this.create(Enumeration.class);
        EnumerationLiteral enumerationLiteral = this.createIn(EnumerationLiteral.class, oldContainer);

        assertTrue(oldContainer.getOwnedLiterals().contains(enumerationLiteral));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(enumerationLiteral, oldContainer,
                newContainer, this.getCrossRef(), this.getEditableChecker());
        assertFalse(oldContainer.getOwnedLiterals().contains(enumerationLiteral));
        assertTrue(newContainer.getOwnedLiterals().contains(enumerationLiteral));
    }

    /**
     * Test dropping a {@link Operation}.
     */
    @Test
    public void testOperationDrop() {
        org.eclipse.uml2.uml.Class oldContainer = this.create(org.eclipse.uml2.uml.Class.class);
        org.eclipse.uml2.uml.Class newContainer = this.create(org.eclipse.uml2.uml.Class.class);
        Operation operation = this.createIn(Operation.class, oldContainer);

        assertTrue(oldContainer.getOwnedOperations().contains(operation));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(operation, oldContainer, newContainer,
                this.getCrossRef(), this.getEditableChecker());
        assertFalse(oldContainer.getOwnedOperations().contains(operation));
        assertTrue(newContainer.getOwnedOperations().contains(operation));
    }

    /**
     * Test dropping a {@link Package}.
     */
    @Test
    public void testPackageDrop() {
        Package oldContainer = this.create(Package.class);
        Package newContainer = this.create(Package.class);
        Package pack = this.createIn(Package.class, oldContainer);

        assertTrue(oldContainer.getPackagedElements().contains(pack));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(pack, oldContainer, newContainer,
                this.getCrossRef(), this.getEditableChecker());
        assertFalse(oldContainer.getPackagedElements().contains(pack));
        assertTrue(newContainer.getPackagedElements().contains(pack));

    }

    /**
     * Test dropping a {@link PrimitiveType}.
     */
    @Test
    public void testPrimitiveTypeDrop() {
        Package oldContainer = this.create(Package.class);
        Package newContainer = this.create(Package.class);
        PrimitiveType primitiveType = this.createIn(PrimitiveType.class, oldContainer);

        assertTrue(oldContainer.getPackagedElements().contains(primitiveType));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(primitiveType, oldContainer, newContainer,
                this.getCrossRef(), this.getEditableChecker());
        assertFalse(oldContainer.getPackagedElements().contains(primitiveType));
        assertTrue(newContainer.getPackagedElements().contains(primitiveType));
    }

    /**
     * Test dropping a {@link Profile}.
     */
    @Test
    public void testProfileDrop() {
        Package oldContainer = this.create(Package.class);
        Package newContainer = this.create(Package.class);
        Profile profile = this.createIn(Profile.class, oldContainer);

        assertTrue(oldContainer.getPackagedElements().contains(profile));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(profile, oldContainer, newContainer,
                this.getCrossRef(), this.getEditableChecker());
        assertFalse(oldContainer.getPackagedElements().contains(profile));
        assertTrue(newContainer.getPackagedElements().contains(profile));
    }

    /**
     * Test dropping a {@link Property}.
     */
    @Test
    public void testPropertyDrop() {
        org.eclipse.uml2.uml.Class oldContainer = this.create(org.eclipse.uml2.uml.Class.class);
        org.eclipse.uml2.uml.Class newContainer = this.create(org.eclipse.uml2.uml.Class.class);
        Property property = this.createIn(Property.class, oldContainer);

        assertTrue(oldContainer.getOwnedAttributes().contains(property));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(property, oldContainer, newContainer,
                this.getCrossRef(), this.getEditableChecker());
        assertFalse(oldContainer.getOwnedAttributes().contains(property));
        assertTrue(newContainer.getOwnedAttributes().contains(property));
    }

    /**
     * Test dropping a {@link Stereotype}.
     */
    @Test
    public void testStereotypeDrop() {
        Package oldContainer = this.create(Package.class);
        Package newContainer = this.create(Package.class);
        Stereotype stereotype = this.createIn(Stereotype.class, oldContainer);

        assertTrue(oldContainer.getPackagedElements().contains(stereotype));
        new ProfileInternalSourceToRepresentationDropBehaviorProvider().drop(stereotype, oldContainer, newContainer,
                this.getCrossRef(), this.getEditableChecker());
        assertFalse(oldContainer.getPackagedElements().contains(stereotype));
        assertTrue(newContainer.getPackagedElements().contains(stereotype));
    }

}
