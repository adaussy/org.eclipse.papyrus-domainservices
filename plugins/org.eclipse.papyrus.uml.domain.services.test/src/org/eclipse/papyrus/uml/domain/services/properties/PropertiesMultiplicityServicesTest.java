/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.properties.mock.MockEditableChecker;
import org.eclipse.papyrus.uml.domain.services.properties.mock.MockLogger;
import org.eclipse.uml2.uml.LiteralBoolean;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.LiteralNull;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.LiteralUnlimitedNatural;
import org.eclipse.uml2.uml.MultiplicityElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.ValueSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link PropertiesMultiplicityServices} service class.
 * 
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class PropertiesMultiplicityServicesTest extends AbstractPropertiesServicesTest {

	/**
	 * The 0..* multiplicity.
	 */
	private static final String ANY = "0..*"; //$NON-NLS-1$
	/**
	 * "5..4" multiplicity.
	 */
	private static final String INVALID_POSITIVE = "5..4"; //$NON-NLS-1$
	/**
	 * "-1" value, which is the equivalent of the upper bound's integer value for "0..*".
	 */
	private static final String STAR_INTEGER_VALUE = "-1"; //$NON-NLS-1$
	/**
	 * The instance of PropertiesServices being tested.
	 */
	private PropertiesMultiplicityServices propertiesService;

    @BeforeEach
	public void setUp() {
        this.propertiesService = new PropertiesMultiplicityServices(new MockLogger(), new MockEditableChecker());
	}

	@Test
	public void testSetMultiplicityCorrectValues() {
		MultiplicityElement multiplicity = create(Property.class);

        assertTrue(propertiesService.setMultiplicity(multiplicity, "1..1")); //$NON-NLS-1$
		assertEquals(1, multiplicity.lowerBound());
		assertEquals(1, multiplicity.upperBound());
        assertTrue(propertiesService.setMultiplicity(multiplicity, "5..5")); //$NON-NLS-1$
		assertEquals(5, multiplicity.lowerBound());
		assertEquals(5, multiplicity.upperBound());
        assertTrue(propertiesService.setMultiplicity(multiplicity, ANY)); // $NON-NLS-1$
		assertEquals(0, multiplicity.lowerBound());
		assertEquals(-1, multiplicity.upperBound());
        assertTrue(propertiesService.setMultiplicity(multiplicity, "1")); //$NON-NLS-1$
		assertEquals(1, multiplicity.lowerBound());
		assertEquals(1, multiplicity.upperBound());
        assertTrue(propertiesService.setMultiplicity(multiplicity, "5")); //$NON-NLS-1$
		assertEquals(5, multiplicity.lowerBound());
		assertEquals(5, multiplicity.upperBound());
        assertTrue(propertiesService.setMultiplicity(multiplicity, "*")); //$NON-NLS-1$
		assertEquals(0, multiplicity.lowerBound());
		assertEquals(-1, multiplicity.upperBound());
        assertTrue(propertiesService.setMultiplicity(multiplicity, "0..1")); //$NON-NLS-1$
		assertEquals(0, multiplicity.lowerBound());
		assertEquals(1, multiplicity.upperBound());
        assertTrue(propertiesService.setMultiplicity(multiplicity, "1..*")); //$NON-NLS-1$
		assertEquals(1, multiplicity.lowerBound());
		assertEquals(-1, multiplicity.upperBound());
	}

	@Test
	public void testSetMultiplicityIncorrectValues() {
		MultiplicityElement multiplicity = create(Property.class);

        assertFalse(propertiesService.setMultiplicity(multiplicity, "0..0")); //$NON-NLS-1$
		assertNull(multiplicity.getLowerValue());
		assertNull(multiplicity.getUpperValue());
        assertFalse(propertiesService.setMultiplicity(multiplicity, "0..-1")); //$NON-NLS-1$
		assertNull(multiplicity.getLowerValue());
		assertNull(multiplicity.getUpperValue());
        assertFalse(propertiesService.setMultiplicity(multiplicity, "-2..0")); //$NON-NLS-1$
		assertNull(multiplicity.getLowerValue());
		assertNull(multiplicity.getUpperValue());
        assertFalse(propertiesService.setMultiplicity(multiplicity, INVALID_POSITIVE));
		assertNull(multiplicity.getLowerValue());
		assertNull(multiplicity.getUpperValue());
        assertFalse(propertiesService.setMultiplicity(multiplicity, "1..-1")); //$NON-NLS-1$
		assertEquals(1, multiplicity.lowerBound());
		assertEquals(1, multiplicity.upperBound());
        assertFalse(propertiesService.setMultiplicity(multiplicity, "1..-25")); //$NON-NLS-1$
		assertEquals(1, multiplicity.lowerBound());
		assertEquals(1, multiplicity.upperBound());
	}

	@Test
	public void testSetMultiplicityNullValue() {
		MultiplicityElement multiplicity = create(Property.class);
        assertFalse(propertiesService.setMultiplicity(multiplicity, null));
		assertNull(multiplicity.getLowerValue());
		assertNull(multiplicity.getUpperValue());

        assertFalse(propertiesService.setMultiplicity(multiplicity, null));
		assertNull(multiplicity.getLowerValue());
		assertNull(multiplicity.getUpperValue());
	}

	@Test
	public void testSetIncorrectMultiplicityUpperValueInitWithLiteralNull() {
		MultiplicityElement multiplicity = create(Property.class);
		LiteralNull literalNull = create(LiteralNull.class);
		multiplicity.setUpperValue(literalNull);

        assertFalse(propertiesService.setMultiplicity(multiplicity, "-1..0")); //$NON-NLS-1$
		assertNull(multiplicity.getLowerValue());
		assertNotNull(multiplicity.getUpperValue());
	}

	@Test
	public void testSetIncorrectMultiplicityUpperValueInitWithLiteralInteger() {
		MultiplicityElement multiplicity = create(Property.class);
		LiteralInteger literalInteger = create(LiteralInteger.class);
		literalInteger.setValue(-1);
		multiplicity.setUpperValue(literalInteger);

        assertFalse(propertiesService.setMultiplicity(multiplicity, "-1..0")); //$NON-NLS-1$
		assertNull(multiplicity.getLowerValue());
		assertNotNull(multiplicity.getUpperValue());
	}

	@Test
	public void testMultiplicityValueCorrectValues() {
		MultiplicityElement multiplicity = create(Property.class);
		LiteralInteger lowerValue = create(LiteralInteger.class);
		LiteralUnlimitedNatural upperValue = create(LiteralUnlimitedNatural.class);
		multiplicity.setLowerValue(lowerValue);
		multiplicity.setUpperValue(upperValue);
		lowerValue.setValue(4);
		upperValue.setValue(5);
		assertEquals("4..5", propertiesService.getMultiplicity(multiplicity)); //$NON-NLS-1$
		lowerValue.setValue(0);
		upperValue.setValue(-1);
		assertEquals(ANY, propertiesService.getMultiplicity(multiplicity));
		lowerValue.setValue(2);
		upperValue.setValue(2);
		assertEquals("2", propertiesService.getMultiplicity(multiplicity)); //$NON-NLS-1$
	}

	@Test
	public void testMultiplicityValueIncorrectValues() {
		MultiplicityElement multiplicity = create(Property.class);
		LiteralInteger lowerValue = create(LiteralInteger.class);
		LiteralUnlimitedNatural upperValue = create(LiteralUnlimitedNatural.class);
		multiplicity.setLowerValue(lowerValue);
		multiplicity.setUpperValue(upperValue);
		lowerValue.setValue(5);
		upperValue.setValue(4);
		assertEquals(INVALID_POSITIVE, propertiesService.getMultiplicity(multiplicity));

		lowerValue.setValue(0);
		upperValue.setValue(0);
		assertEquals("0", propertiesService.getMultiplicity(multiplicity)); //$NON-NLS-1$

		lowerValue.setValue(-12);
		upperValue.setValue(-5);
		assertEquals("-12..*", propertiesService.getMultiplicity(multiplicity)); //$NON-NLS-1$

		lowerValue.setValue(2);
		multiplicity.setUpperValue(create(LiteralBoolean.class));
		assertEquals("2..1", propertiesService.getMultiplicity(multiplicity)); //$NON-NLS-1$

		LiteralString lowerString = create(LiteralString.class);
		LiteralString upperString = create(LiteralString.class);
		multiplicity.setLowerValue(lowerString);
		multiplicity.setUpperValue(upperString);
		lowerString.setValue("5"); //$NON-NLS-1$
		upperString.setValue("4"); //$NON-NLS-1$
		assertEquals(INVALID_POSITIVE, propertiesService.getMultiplicity(multiplicity));

		lowerString.setValue(STAR_INTEGER_VALUE);
		upperString.setValue(STAR_INTEGER_VALUE);
		assertEquals(STAR_INTEGER_VALUE, propertiesService.getMultiplicity(multiplicity));
	}

	@Test
	public void testMultiplicityValueDefaultValues() {
		MultiplicityElement multiplicity = create(Property.class);
		assertEquals("1", propertiesService.getMultiplicity(multiplicity)); //$NON-NLS-1$
		ValueSpecification lowerValue = create(LiteralInteger.class);
		ValueSpecification upperValue = create(LiteralUnlimitedNatural.class);
		multiplicity.setLowerValue(lowerValue);
		multiplicity.setUpperValue(upperValue);
		assertEquals("0", propertiesService.getMultiplicity(multiplicity)); //$NON-NLS-1$
	}
}
