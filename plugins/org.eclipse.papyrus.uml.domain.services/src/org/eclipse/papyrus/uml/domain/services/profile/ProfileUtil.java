/*****************************************************************************
 * Copyright (c) 2012, 2023 CEA LIST, Christian W. Damus, Esterel Technologies SAS, EclipseSource and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Camille Letavernier (CEA LIST) camille.letavernier@cea.fr - Initial API and implementation
 *  Christian W. Damus (CEA) - Handle dynamic profile applications in CDO
 *  Christian W. Damus - bug 399859
 *  Christian W. Damus - bug 451613
 *  Christian W. Damus - bug 474610
 *  Calin Glitia (Esterel Technologies SAS) - bug 497699
 *  Camille Letavernier (EclipseSource) - bug 530156
 *  Nicolas FAUVERGUE (CEA LIST) nicolas.fauvergue@cea.fr - Bug 538193
 *
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.profile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.uml2.common.util.UML2Util;
import org.eclipse.uml2.uml.AttributeOwner;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.OperationOwner;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.ProfileApplication;
import org.eclipse.uml2.uml.Property;

/**
 * A class containing static utility method regarding UML profiles.<br/>
 * 
 * Copied from {@link org.eclipse.papyrus.uml.tools.utils.ProfileUtil}
 *
 * @author Camille Letavernier
 */
public class ProfileUtil extends org.eclipse.uml2.uml.util.UMLUtil {

    /**
     * Sort a list of profiles in dependency order, such that profiles that are used
     * by other profiles sort ahead of the profiles that use them. A profile uses
     * another profile when it has classifiers that either:
     * <ul>
     * <li>specialize classifiers contained in the other profile (possibly in nested
     * packages), or</li>
     * <li>have attributes typed by classifiers in the other profile, or</li>
     * <li>have operations with parameters typed by classifiers in the other
     * profile</li>
     * </ul>
     *
     * @param profiles
     *                 a list of profiles to sort in place
     */
    public static void sortProfiles(List<Profile> profiles) {
        // Analyze profiles for inter-dependencies
        final Map<Profile, Set<Profile>> dependencies = computeProfileDependencies(profiles);

        // Expand to find transitive dependencies
        expand(dependencies);

        // Now sort by used profiles ahead of those that use them
        Collections.sort(profiles, new Comparator<Profile>() {
            @Override
            public int compare(Profile o1, Profile o2) {
                int compareResult = 0;
                if (dependencies.get(o1).contains(o2)) {
                    compareResult = 1;
                } else if (dependencies.get(o2).contains(o1)) {
                    compareResult = -1;
                }
                return compareResult;
            }
        });
    }

    /**
     * Compute a mapping of profiles to the profiles that they depend on.
     *
     * @param profiles
     *                 profiles for which to compute dependencies
     * @return the dependency mapping (one-to-many)
     */
    private static Map<Profile, Set<Profile>> computeProfileDependencies(Collection<? extends Profile> profiles) {
        Map<Profile, Set<Profile>> result = new LinkedHashMap<>();
        for (Profile next : profiles) {
            for (PackageableElement member : next.getPackagedElements()) {
                if (member instanceof Classifier) {
                    // Look for supertype dependencies
                    for (Classifier general : ((Classifier) member).allParents()) {
                        addProfileContaining(general, next, result);
                    }
                }
                if (member instanceof AttributeOwner) {
                    // Look for attribute type dependencies
                    for (Property property : ((AttributeOwner) member).getOwnedAttributes()) {
                        addProfileContaining(property.getType(), next, result);
                    }
                }
                if (member instanceof OperationOwner) {
                    // Look for operation parameter type dependencies
                    for (Operation operation : ((OperationOwner) member).getOwnedOperations()) {
                        for (Parameter parameter : operation.getOwnedParameters()) {
                            addProfileContaining(parameter.getType(), next, result);
                        }
                    }
                }
            }

            // Exclude self dependencies
            result.get(next).remove(next);
        }

        return result;
    }

    private static void addProfileContaining(PackageableElement element, Profile dependent,
            Map<Profile, Set<Profile>> dependencies) {
        if (element != null) {
            Package containingPackage = element.getNearestPackage();
            while ((containingPackage != null) && !(containingPackage instanceof Profile)
                    && (containingPackage.getOwner() != null)) {
                containingPackage = containingPackage.getOwner().getNearestPackage();
            }

            if (containingPackage instanceof Profile) {
                Set<Profile> containingProfiles = Optional.of(dependencies.get(dependent))
                        .orElse(new LinkedHashSet<>());
                containingProfiles.add((Profile) containingPackage);
                dependencies.put(dependent, containingProfiles);
            }
        }
    }

    /**
     * Checks if the profile applied has been changed since last application
     * (definition does not match.
     *
     * @param pack
     *                 on which the profile is applied
     * @param profile
     *                 the applied profile
     * @return true if the profile has changed
     */
    // Copied from org.eclipse.papyrus.uml.tools.utils.ProfileUtil.isDirty(Package,
    // Profile)
    // CHECKSTYLE:OFF Copied from Papyrus Desktop
    public static boolean isDirty(Package pack, Profile profile) {
        boolean isDirty = false;
        if (profile == null || profile.eResource() == null || pack == null || pack.eResource() == null) {
            return false;
        }

        // Retrieve model resourceSet
        ResourceSet pkgeResourceSet = pack.eResource().getResourceSet();

        if (pkgeResourceSet != null) {

            // Retrieve profile resource
            URI profURI = profile.eResource().getURI();
            Resource modelResource = pkgeResourceSet.getResource(profURI, true);

            if (modelResource != null && modelResource.getContents().get(0) instanceof Profile) {

                // ckeck applied profile application definition vs profile definition referenced
                // in file
                Profile profileInFile = (Profile) (modelResource.getContents().get(0));
                ProfileApplication profileApplication = pack.getProfileApplication(profile, true);
                if (profileApplication != null) {
                    EPackage appliedProfileDefinition = profileApplication.getAppliedDefinition();
                    EPackage fileProfileDefinition = null;

                    // Check profiles qualified names to ensure the correct profiles are compared
                    String appliedProfileName = profile.getQualifiedName();
                    String fileProfileName = profileInFile.getQualifiedName();
                    if (!appliedProfileName.equals(fileProfileName)) {

                        // The profile must be a subprofile
                        Iterator<Profile> it = getSubProfiles(profileInFile).iterator();
                        while (it.hasNext()) {
                            Profile current = it.next();
                            fileProfileName = current.getQualifiedName();
                            if (fileProfileName.equals(appliedProfileName)) {
                                profileInFile = current;
                            }
                        }
                    }

                    fileProfileDefinition = profileInFile.getDefinition();

                    // don't just test that the EPackage definitions are the
                    // same object because in the CDO context they are not, even
                    // though they are "the same package". Comparing the URIs
                    // should suffice
                    final URIConverter converter = pkgeResourceSet.getURIConverter();
                    if ((appliedProfileDefinition == null) || (fileProfileDefinition == null)
                            || !UML2Util.safeEquals(converter.normalize(EcoreUtil.getURI(appliedProfileDefinition)),
                                    converter.normalize(EcoreUtil.getURI(fileProfileDefinition)))) {

                        isDirty = true;
                    }
                }

            }
        }

        return isDirty;
    }
    // CHECKSTYLE:ON Copied from Papyrus Desktop

    /**
     * Retrieve recursively the subprofiles of package.
     *
     * @param pack
     *
     * @return an arrayList containing the subprofiles
     */
    // Copied from
    // org.eclipse.papyrus.uml.tools.utils.PackageUtil.getSubProfiles(Package)
    private static List<Profile> getSubProfiles(org.eclipse.uml2.uml.Package pack) {
        List<Profile> subProfiles = new ArrayList<Profile>();

        Iterator<Package> iter = pack.getNestedPackages().iterator();
        while (iter.hasNext()) {

            Object element = iter.next();
            if (element instanceof Profile) {
                Profile currentSubProfile = (Profile) element;
                subProfiles.add(currentSubProfile);
                subProfiles.addAll(getSubProfiles(currentSubProfile));
            } else if (element instanceof org.eclipse.uml2.uml.Package) {
                org.eclipse.uml2.uml.Package currentSubPackage = (org.eclipse.uml2.uml.Package) element;
                subProfiles.addAll(getSubProfiles(currentSubPackage));
            }
        }
        return subProfiles;
    }

    /**
     * Expands a profile dependency map to include all transitive dependencies in
     * the mapping for each profile.
     *
     * @param dependencies
     *                     a dependency map to expand
     */
    private static void expand(Map<Profile, Set<Profile>> dependencies) {
        boolean changed = true;
        while (changed) {
            changed = false;

            for (Profile next : new ArrayList<>(dependencies.keySet())) {
                for (Profile dep : new ArrayList<>(dependencies.get(next))) {
                    Set<Profile> transitive = dependencies.get(dep);

                    // Add the transitive dependencies to my own
                    if ((transitive != null) && dependencies.get(next).addAll(transitive)) {
                        changed = true;
                    }
                }
            }
        }
    }
}
