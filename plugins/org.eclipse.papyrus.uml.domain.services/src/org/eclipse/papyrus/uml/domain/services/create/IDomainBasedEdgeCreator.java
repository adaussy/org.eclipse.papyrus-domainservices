/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;

/**
 * Create of a domain based edge. A domain based edge is an edge representing a
 * semantic element (versus edges representing feature)
 *
 * @author Arthur Daussy
 */
public interface IDomainBasedEdgeCreator {

    /**
     * Creates the semantic element for a domain based edge.
     *
     * @param semanticEdgeSource
     *                           the source of the edge
     * @param semanticEdgeTarget
     *                           the target of the edge
     * @param type
     *                           the semantic type
     * @param referenceName
     *                           the name of the containment reference
     * @return a {@link CreationStatus}
     */
    CreationStatus createDomainBasedEdge(EObject semanticEdgeSource, EObject semanticEdgeTarget, String type,
            String referenceName, IViewQuerier representionQuery, Object sourceView, Object targetView);

}
