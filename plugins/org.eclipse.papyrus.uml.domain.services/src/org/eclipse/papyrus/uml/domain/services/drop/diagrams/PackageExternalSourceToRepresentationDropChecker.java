/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.text.MessageFormat;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * This class is used to check if an element from the Model Explorer can be drag
 * and drop to the Package Diagram on the targeted container.
 *
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 *
 */
public class PackageExternalSourceToRepresentationDropChecker implements IExternalSourceToRepresentationDropChecker {

    private static final String UNAUTHORIZED_DND_ERROR_MSG = "DnD is not authorized.";

    private static final String DROP_ERROR_MSG = "{0} can only be drag and drop on {1}.";

    private static final String COMA = ", ";

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        return new PackageDropInsideRepresentationCheckerSwitch(newSemanticContainer).doSwitch(elementToDrop);
    }

    static class PackageDropInsideRepresentationCheckerSwitch extends UMLSwitch<CheckStatus> {

        private final EObject newSemanticContainer;

        PackageDropInsideRepresentationCheckerSwitch(EObject target) {
            super();
            this.newSemanticContainer = target;
        }

        @Override
        public CheckStatus caseComment(Comment comment) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Package) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no(this.canOnlyDropOnPackageAndModelMessage(comment));
            }
            return result;
        }

        @Override
        public CheckStatus caseConstraint(Constraint constraint) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Package) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no(this.canOnlyDropOnPackageAndModelMessage(constraint));
            }
            return result;
        }

        @Override
        public CheckStatus casePackage(Package pkg) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Package) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no(this.canOnlyDropOnPackageAndModelMessage(pkg));
            }
            return result;
        }

        private String canOnlyDropOnPackageAndModelMessage(EObject object) {
            return MessageFormat.format(DROP_ERROR_MSG, object.eClass().getName(),
                    UMLPackage.eINSTANCE.getPackage().getName() + COMA + UMLPackage.eINSTANCE.getModel().getName());
        }

        @Override
        public CheckStatus defaultCase(EObject object) {
            return CheckStatus.no(UNAUTHORIZED_DND_ERROR_MSG);
        }
    }
}
