/*****************************************************************************
 * Copyright (c) 2009, 2023 CEA LIST and others.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Yann TANGUY (CEA LIST) yann.tanguy@cea.fr - Initial API and implementation
 *  Christian W. Damus (CEA) - bug 440263
 *  Vincent LORENZO (CEA LIST) vincent.lorenzo@cea.fr - bug 530155
 *  Obeo - Refactoring for Papyrus Web
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels.domains;

import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EMPTY;

import org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters;
import org.eclipse.uml2.uml.NamedElement;

/**
 * Utility class to get visibility sign from
 * <code>org.eclipse.uml2.uml.NamedElement</code>.
 */
// CHECKSTYLE:OFF Papyrus org.eclipse.papyrus.uml.tools.utils.NamedElementUtil
public class VisibilityLabelHelper {
    // CHECKSTYLE:ON

    private static final String PUBLIC_STRING = "+"; //$NON-NLS-1$

    private static final String PROTECTED_STRING = "#"; //$NON-NLS-1$

    private static final String PRIVATE_STRING = "-"; //$NON-NLS-1$

    /**
     * Give the visibility of the {@link NamedElement} as a string, as defined in
     * the UML2 standard.
     *
     * @return A String representing the visibility of the {@link NamedElement}.
     *         Possible values:
     *         <ul>
     *         <li>public: <code>"+"</code>
     *         <li>private: <code>"-"</code>
     *         <li>protected: <code>"#"</code>
     *         <li>package: <code>"~"</code>
     *         </ul>
     */
    public String getVisibilityAsSign(NamedElement element) {
        String vKindValue = null;

        switch (element.getVisibility().getValue()) {
        case org.eclipse.uml2.uml.VisibilityKind.PUBLIC:
            vKindValue = PUBLIC_STRING;
            break;
        case org.eclipse.uml2.uml.VisibilityKind.PRIVATE:
            vKindValue = PRIVATE_STRING;
            break;
        case org.eclipse.uml2.uml.VisibilityKind.PACKAGE:
            vKindValue = UMLCharacters.TILDE;
            break;
        case org.eclipse.uml2.uml.VisibilityKind.PROTECTED:
            vKindValue = PROTECTED_STRING;
            break;
        default:
            vKindValue = EMPTY; // $NON-NLS-1$
        }
        return vKindValue;
    }

}
