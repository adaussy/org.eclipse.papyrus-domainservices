/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.util.Collections;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.drop.DnDStatus;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Drop behavior provider of a semantic drop (from Model Explorer view) to a
 * Package Diagram Element (or the root of the diagram itself).
 *
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class PackageExternalSourceToRepresentationDropBehaviorProvider
        implements IExternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public DnDStatus drop(EObject droppedElement, EObject target, ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker) {
        return new PackageDropInsideRepresentationBehaviorProviderSwitch(target).doSwitch(droppedElement);
    }

    static class PackageDropInsideRepresentationBehaviorProviderSwitch extends UMLSwitch<DnDStatus> {

        private final EObject target;

        PackageDropInsideRepresentationBehaviorProviderSwitch(EObject target) {
            super();
            this.target = target;
        }

        @Override
        public DnDStatus caseAbstraction(Abstraction abstraction) {
            return DnDStatus.createNothingStatus(Set.of(abstraction));
        }

        @Override
        public DnDStatus caseComment(Comment comment) {
            if (this.target instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(comment));
            }
            return super.caseComment(comment);
        }

        @Override
        public DnDStatus caseConstraint(Constraint constraint) {
            if (this.target instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(constraint));
            }
            return super.caseConstraint(constraint);
        }

        @Override
        public DnDStatus caseDependency(Dependency dependency) {
            return DnDStatus.createNothingStatus(Set.of(dependency));
        }

        @Override
        public DnDStatus casePackage(Package pkg) {
            if (this.target instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(pkg));
            }
            return super.casePackage(pkg);
        }

        @Override
        public DnDStatus casePackageImport(PackageImport packageImport) {
            return DnDStatus.createNothingStatus(Set.of(packageImport));
        }

        @Override
        public DnDStatus defaultCase(EObject object) {
            return DnDStatus.createFailingStatus("DnD is forbidden.", Collections.emptySet());
        }
    }
}
