/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.destroy;

import org.eclipse.emf.ecore.EObject;

/**
 * Object in charge of destroying a semantic element from its container.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 */
public interface IDestroyer {

    /**
     * Destroys a semantic element value from its container.
     *
     * @param semanticElement
     *                        the semantic element to delete
     * @return a set destroy status that contains the set of deleted objects or the
     *         objects that failed to delete.
     */
    DestroyerStatus destroy(EObject semanticElement);

}
