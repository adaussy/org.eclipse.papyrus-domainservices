/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services;

import org.eclipse.emf.ecore.EObject;

/**
 * Object that check if an element can be edited.
 * 
 * @author Arthur Daussy
 *
 */
public interface IEditableChecker {

    /**
     * {@link IEditableChecker} that always return true.
     */
    IEditableChecker TRUE = e -> true;

    /**
     * Check if the given object can be edited.
     * 
     * @param eObject
     *                a eObject
     * @return <code>true</code> if the {@link EObject} can be edited,
     *         <code>false</code> otherwise
     */
    boolean canEdit(EObject eObject);

}
