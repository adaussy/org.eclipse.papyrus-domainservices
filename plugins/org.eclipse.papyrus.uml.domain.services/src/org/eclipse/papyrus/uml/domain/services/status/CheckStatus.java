/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.status;

/**
 * Status used to check if an action is possible.
 * 
 * @author Arthur Daussy
 *
 */
public class CheckStatus {

    public static final CheckStatus YES = new CheckStatus(true, "");

    private final boolean isValid;

    private final String message;

    public CheckStatus(boolean isValid, String message) {
        super();
        this.isValid = isValid;
        this.message = message;
    }

    public boolean isValid() {
        return isValid;
    }

    public String getMessage() {
        return message;
    }

    public static CheckStatus no(String msg) {
        return new CheckStatus(false, msg);
    }

}
