/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.util.Collections;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.drop.DnDStatus;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Relationship;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Drop behavior provider of a semantic drop (from Model Explorer view) to a
 * Profile Diagram Element (or the root of the diagram itself).
 *
 * @author <a href="mailto:florian.barbin@obeo.fr">Florian Barbin</a>
 */
public class ProfileExternalSourceToRepresentationDropBehaviorProvider
        implements IExternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public DnDStatus drop(EObject droppedElement, EObject target, ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker) {
        return new ProfileDropInsideRepresentationBehaviorProviderSwitch(target).doSwitch(droppedElement);
    }

    static class ProfileDropInsideRepresentationBehaviorProviderSwitch extends UMLSwitch<DnDStatus> {

        private EObject newSemanticContainer;

        ProfileDropInsideRepresentationBehaviorProviderSwitch(EObject target) {
            this.newSemanticContainer = target;
        }

        @Override
        public DnDStatus caseClass(Class clazz) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(clazz));
            }
            return super.caseClass(clazz);
        }

        @Override
        public DnDStatus caseComment(Comment comment) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(comment));
            }
            return super.caseComment(comment);
        }

        @Override
        public DnDStatus caseConstraint(Constraint constraint) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(constraint));
            }
            return super.caseConstraint(constraint);
        }

        @Override
        public DnDStatus caseDataType(DataType dataType) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(dataType));
            }
            return super.caseDataType(dataType);
        }

        @Override
        public DnDStatus caseElementImport(ElementImport elementImport) {
            if (this.newSemanticContainer instanceof Profile) {
                return DnDStatus.createNothingStatus(Set.of(elementImport));
            }
            // Do not call super here, ElementImport is a Relationship so it will return a
            // nothing status, although we only want to allow its drop on a Profile.
            return DnDStatus.createFailingStatus("DnD is forbidden.", Set.of(elementImport));
        }

        @Override
        public DnDStatus caseEnumeration(Enumeration enumeration) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(enumeration));
            }
            return super.caseEnumeration(enumeration);
        }

        @Override
        public DnDStatus caseEnumerationLiteral(EnumerationLiteral enumerationLiteral) {
            if (this.newSemanticContainer instanceof Enumeration) {
                return DnDStatus.createNothingStatus(Set.of(enumerationLiteral));
            }
            return super.caseEnumerationLiteral(enumerationLiteral);
        }

        @Override
        public DnDStatus caseOperation(Operation operation) {
            if (this.newSemanticContainer instanceof Class || (this.newSemanticContainer instanceof DataType
                    && !(this.newSemanticContainer instanceof Enumeration))) {
                return DnDStatus.createNothingStatus(Set.of(operation));
            }
            return super.caseOperation(operation);
        }

        @Override
        public DnDStatus casePackage(Package pack) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(pack));
            }
            return super.casePackage(pack);
        }

        @Override
        public DnDStatus casePrimitiveType(PrimitiveType primitiveType) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(primitiveType));
            }
            return super.casePrimitiveType(primitiveType);
        }

        @Override
        public DnDStatus caseProfile(Profile profile) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(profile));
            }
            return super.caseProfile(profile);
        }

        @Override
        public DnDStatus caseProperty(Property property) {
            if (this.newSemanticContainer instanceof Class || (this.newSemanticContainer instanceof DataType
                    && !(this.newSemanticContainer instanceof Enumeration))) {
                return DnDStatus.createNothingStatus(Set.of(property));
            }
            return super.caseProperty(property);
        }

        @Override
        public DnDStatus caseRelationship(Relationship relationship) {
            return DnDStatus.createNothingStatus(Set.of(relationship));
        }

        @Override
        public DnDStatus caseStereotype(Stereotype stereotype) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(stereotype));
            }
            return super.caseStereotype(stereotype);
        }

        @Override
        public DnDStatus defaultCase(EObject object) {
            return DnDStatus.createFailingStatus("DnD is forbidden.", Collections.emptySet());
        }

    }
}
