/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.status;

/**
 * State a of creation status.
 *
 * @author Arthur Daussy
 */
public enum State {

    /**
     * All work in done correctly.
     */
    DONE,
    /**
     * The operation failed.
     */
    FAILED,
    /**
     * Nothing has been done but its normal.
     */
    NOTHING

}
