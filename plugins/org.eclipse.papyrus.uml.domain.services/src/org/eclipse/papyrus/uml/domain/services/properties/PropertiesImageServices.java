/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.properties;

import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.uml2.uml.Image;

/**
 * This service class includes all services used for {@link Image} UMLmodel elements.
 * 
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class PropertiesImageServices {

    /**
     * The kind of image display Undefined.
     */
    private static final String KIND_UNDEFINED = "undefined"; //$NON-NLS-1$

	/**
	 * The kind of image display Displays the image as an Icon in the element edit
	 * part.
	 */
	private static final String KIND_ICON = "icon"; //$NON-NLS-1$

	/**
	 * The kind of image display The image replaces the element edit part.
	 */
	private static final String KIND_SHAPE = "shape"; //$NON-NLS-1$

    /**
     * ID of the EAnnotation where "expression" (used to select stereotype icon) is
     * stored on image.
     */
    private static final String IMAGE_PAPYRUS_EA = "image_papyrus"; //$NON-NLS-1$

    /**
     * KEY of the EAnnotation where "expression" (used to select stereotype icon) is
     * stored on image.
     */
    private static final String IMAGE_EXPR_KEY = "image_expr_key"; //$NON-NLS-1$

    /**
     * KEY of the EAnnotation where "kind" (kind = icon/shape) is stored on image.
     */
    private static final String IMAGE_KIND_KEY = "image_kind_key"; //$NON-NLS-1$

    /**
     * KEY of the EAnnotation where the image's name is stored.
     *
     * @see {@link #getName(Image)}
     * @see {@link #setName(Image, String)}
     */
    private static final String IMAGE_NAME_KEY = "image_name_key"; //$NON-NLS-1$

    /**
     * Removes the EAnnotation from the Image if the annotation doesn't contain any
     * valid key Copied from
     * org.eclipse.papyrus.uml.tools.utils.ImageUtil.cleanImageAnnotation(EAnnotation)
     * 
     * @param annotation
     */
    private static void cleanImageAnnotation(EAnnotation annotation) {
        EMap<String, String> details = annotation.getDetails();
        if (details.isEmpty()) {
            annotation.setEModelElement(null);
        }

    }

	/**
	 * Get the name of a given {@link Image}.
	 * 
	 * @see {@link org.eclipse.papyrus.uml.properties.databinding.ImageNameObservableValue.doGetValue()}.
	 * 
	 * @param image
	 *            the image which contains the name
	 * @return the name of a given {@link Image}.
	 */
	public String getImageName(Image image) {
        // Copied from org.eclipse.papyrus.uml.tools.utils.ImageUtil.getName(Image)
        EAnnotation eaImage = image.getEAnnotation(IMAGE_PAPYRUS_EA);

        String name = null;
        if ((eaImage != null) && (eaImage.getDetails().containsKey(IMAGE_NAME_KEY))) {
            name = eaImage.getDetails().get(IMAGE_NAME_KEY);
        }
        return name;
    }

	/**
	 * Set the name of a given {@link Image}.
	 * 
	 * @see {@link org.eclipse.papyrus.uml.properties.databinding.ImageNameObservableValue.doSetValue(Object)}.
	 * 
	 * @param image
	 *            the image with the name to set
	 * @param name
	 *            the name to set.
	 */
	public void setImageName(Image image, String name) {
        // Copied from org.eclipse.papyrus.uml.tools.utils.ImageUtil.setName(Image,
        // String)
        EAnnotation eaImage = image.getEAnnotation(IMAGE_PAPYRUS_EA);
        // Create annotation for icon selection if it does not exist
        if (eaImage == null) {
            eaImage = image.createEAnnotation(IMAGE_PAPYRUS_EA);
        }

        // If expression == "" remove the EAnnotation
        if ("".equals(name)) { //$NON-NLS-1$
            eaImage.getDetails().removeKey(IMAGE_NAME_KEY);
        } else {
            eaImage.getDetails().put(IMAGE_NAME_KEY, name);
        }

        cleanImageAnnotation(eaImage);
	}

	/**
	 * Get the kind of a given {@link Image}.
	 * 
	 * @see {@link org.eclipse.papyrus.uml.properties.databinding.ImageKindObservableValue.doGetValue()}.
	 * 
	 * @param image
	 *            the image which contains the kind
	 * @return the kind of a given {@link Image}.
	 */
	public String getImageKind(Image image) {
        // Copied from org.eclipse.papyrus.uml.tools.utils.ImageUtil.getKind(Image)
        EAnnotation eaImageExpr = image.getEAnnotation(IMAGE_PAPYRUS_EA);

        String kind = null;
        if ((eaImageExpr != null) && (eaImageExpr.getDetails().containsKey(IMAGE_KIND_KEY))) {
            kind = eaImageExpr.getDetails().get(IMAGE_KIND_KEY);
        }
        return kind;
	}

	/**
	 * Set the kind of a given {@link Image}.
	 * 
	 * @see {@link org.eclipse.papyrus.uml.properties.databinding.ImageKindObservableValue.doSetValue(Object)}.
	 * 
	 * @param image
	 *            the image with the kind to set
	 * @param kind
	 *            the kind to set.
	 */
	public void setImageKind(Image image, String kind) {
        // Copied from org.eclipse.papyrus.uml.tools.utils.ImageUtil.setKind(Image,
        // String)
        EAnnotation eaImage = image.getEAnnotation(IMAGE_PAPYRUS_EA);
        // Create annotation for icon selection if it does not exist
        if (eaImage == null) {
            eaImage = image.createEAnnotation(IMAGE_PAPYRUS_EA);
        }

        // If expression == "" remove the EAnnotation
        if ("".equals(kind)) { //$NON-NLS-1$
            eaImage.getDetails().removeKey(IMAGE_KIND_KEY);
        } else {
            eaImage.getDetails().put(IMAGE_KIND_KEY, kind);
        }

        cleanImageAnnotation(eaImage);
	}

	/**
	 * Get list of kind candidates.
	 * 
	 * @param obj
	 *            A given {@link Image} which contains kind information.
	 * @return list of kind candidates.
	 */
	public List<String> getImageKindEnumerations(Image obj) {
		return Arrays.asList(KIND_UNDEFINED, KIND_ICON, KIND_SHAPE);
	}

	/**
	 * Get the expression of a given {@link Image}.
	 * 
	 * @see {@link org.eclipse.papyrus.uml.properties.databinding.ImageExpressionObservableValue.doGetValue()}.
	 * 
	 * @param image
	 *            the image which contains the expression
	 * @return the expression of a given {@link Image}.
	 */
	public String getImageExpression(Image image) {
        // Copied from
        // org.eclipse.papyrus.uml.tools.utils.ImageUtil.getExpression(Image)
        EAnnotation eaImageExpr = image.getEAnnotation(IMAGE_PAPYRUS_EA);

        String expr = null;
        if ((eaImageExpr != null) && (eaImageExpr.getDetails().containsKey(IMAGE_EXPR_KEY))) {
            expr = eaImageExpr.getDetails().get(IMAGE_EXPR_KEY);
        }
        return expr;
	}

	/**
	 * Set the expression of a given {@link Image}.
	 * 
	 * @see {@link org.eclipse.papyrus.uml.properties.databinding.ImageExpressionObservableValue.doSetValue(Object)}.
	 * 
	 * @param image
	 *            the image with the expression to set
	 * @param expression
	 *            the expression to set.
	 */
	public void setImageExpression(Image image, String expression) {
        // Copied from
        // org.eclipse.papyrus.uml.tools.utils.ImageUtil.setExpression(Image, String)
        EAnnotation eaImage = image.getEAnnotation(IMAGE_PAPYRUS_EA);
        // Create annotation for icon selection if it does not exist
        if (eaImage == null) {
            eaImage = image.createEAnnotation(IMAGE_PAPYRUS_EA);
        }

        // If expression == "" remove the EAnnotation
        if ("".equals(expression)) { //$NON-NLS-1$
            eaImage.getDetails().removeKey(IMAGE_EXPR_KEY);
        } else {
            eaImage.getDetails().put(IMAGE_EXPR_KEY, expression);
        }

        cleanImageAnnotation(eaImage);
	}
}
