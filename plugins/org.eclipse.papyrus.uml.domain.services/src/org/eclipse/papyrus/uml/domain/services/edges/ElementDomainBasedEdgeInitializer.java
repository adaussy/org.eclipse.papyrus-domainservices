/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.edges;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.ActivityEdgeHelper;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.ClassifierUtils;
import org.eclipse.papyrus.uml.domain.services.labels.ElementDefaultNameProvider;
import org.eclipse.uml2.common.util.UML2Util;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.BehavioredClassifier;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.ComponentRealization;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.ControlFlow;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.DeployedArtifact;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.DeploymentTarget;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.ExtensionEnd;
import org.eclipse.uml2.uml.ExtensionPoint;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.LiteralBoolean;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.OpaqueAction;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Element in charge of initializing a {@link EObject} in a given context.
 *
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 */
public class ElementDomainBasedEdgeInitializer implements IDomainBasedEdgeInitializer {

    /**
     * the extension's name begins by this string.
     */
    public static final String EXTENSION = "extension_"; //$NON-NLS-1$

    /**
     * the property's name in the stereotype begins by base_.
     */
    public static final String BASE = "base_"; //$NON-NLS-1$

    /** Constant for UML nature. */
    public static final String UML_NATURE = "UML_Nature";

    /**
     * The ID for Papyrus EAnnotations.
     */
    private static final String PAPYRUS_URI = "org.eclipse.papyrus"; //$NON-NLS-1$

    /**
     * The ID for element nature in Papyrus EAnnotations.
     */
    private static final String PAPYRUS_ELEMENT_NATURE = "nature"; //$NON-NLS-1$

    /**
     * Initialize a semantic element.
     *
     * @param toInit
     *               the element to init
     * @param parent
     *               the context parent
     * @return the given element for fluent API use
     */
    @Override
    public EObject initialize(EObject toInit, EObject source, EObject target, IViewQuerier representationQuery,
            Object sourceView, Object targetView) {
        if (toInit == null) {
            return null;
        }
        new ElementEdgeInitializerSwitch(source, target, representationQuery, sourceView, targetView).doSwitch(toInit);
        return toInit;
    }

    static class ElementEdgeInitializerSwitch extends UMLSwitch<EObject> {
        private final EObject source;

        private final EObject target;

        private final Object sourceView;

        private final Object targetView;

        private final IViewQuerier representationQuery;

        ElementEdgeInitializerSwitch(EObject source, EObject target, IViewQuerier representationQuery,
                Object sourceView, Object targetView) {
            super();
            this.source = source;
            this.target = target;
            this.sourceView = sourceView;
            this.targetView = targetView;
            this.representationQuery = representationQuery;
        }

        @Override
        public EObject caseAssociation(Association association) {
            // add nature
            EMap<String, String> details = UML2Util.getEAnnotation(association, PAPYRUS_URI, true).getDetails();

            if (!details.containsKey(PAPYRUS_ELEMENT_NATURE)) {
                details.put(PAPYRUS_ELEMENT_NATURE, UML_NATURE);
            } else {
                details.removeKey(PAPYRUS_ELEMENT_NATURE);
                details.put(PAPYRUS_ELEMENT_NATURE, UML_NATURE);
            }

            // source and target are classifier (see {@link
            // ElementDomainBasedEdgeCreationChecker})
            Classifier sourceClassifier = (Classifier) this.source;
            Classifier targetClassifier = (Classifier) this.target;

            // create source property
            Property sourceProp = UMLFactory.eINSTANCE.createProperty();
            sourceProp.setType(targetClassifier);
            if (targetClassifier.getName() != null) {
                sourceProp.setName(targetClassifier.getName().toLowerCase());
            }
            association.getMemberEnds().add(sourceProp);

            // create target property
            Property targetProp = UMLFactory.eINSTANCE.createProperty();
            targetProp.setType(sourceClassifier);
            if (sourceClassifier.getName() != null) {
                targetProp.setName(sourceClassifier.getName().toLowerCase());
            }
            association.getMemberEnds().add(targetProp);

            // Add source {@link Property} in the correct container
            boolean added = ClassifierUtils.addOwnedAttribute(sourceClassifier, sourceProp);
            if (!added) {
                association.getOwnedEnds().add(sourceProp);
            }
            sourceProp.setIsNavigable(false);

            // Add target {@link Property} in the correct container
            added = ClassifierUtils.addOwnedAttribute(targetClassifier, targetProp);
            if (!added) {
                association.getOwnedEnds().add(targetProp);
            }
            targetProp.setIsNavigable(false);

            targetProp.setOwningAssociation(association);
            return association;
        }

        @Override
        public EObject caseComponentRealization(ComponentRealization componentRealization) {
            if (this.source instanceof Classifier) {
                componentRealization.getRealizingClassifiers().add((Classifier) this.source);
            }
            if (this.target instanceof Component) {
                componentRealization.setAbstraction((Component) this.target);
            }
            return super.caseComponentRealization(componentRealization);
        }

        @Override
        public EObject caseConnector(Connector connector) {
            ConnectorEnd sourceEnd = UMLFactory.eINSTANCE.createConnectorEnd();
            if (this.source instanceof ConnectableElement) {
                sourceEnd.setRole((ConnectableElement) this.source);
                connector.getEnds().add(sourceEnd);
            }

            Object sourceVisualParent = this.representationQuery.getVisualParent(this.sourceView);
            if (sourceVisualParent != null) {
                Object visualSourceSementicParent = this.representationQuery.getSemanticElement(sourceVisualParent);
                if (visualSourceSementicParent instanceof Property) {
                    Property sourceProperty = (Property) visualSourceSementicParent;
                    connector.getEnds().get(0).setPartWithPort(sourceProperty);
                }
            }

            ConnectorEnd targetEnd = UMLFactory.eINSTANCE.createConnectorEnd();

            if (this.target instanceof ConnectableElement) {
                targetEnd.setRole((ConnectableElement) this.target);
                connector.getEnds().add(targetEnd);
            }

            Object targetVisualParent = this.representationQuery.getVisualParent(this.targetView);

            if (targetVisualParent != null) {
                Object visualTargetParent = this.representationQuery.getSemanticElement(targetVisualParent);
                if (visualTargetParent instanceof Property) {
                    Property targetProperty = (Property) visualTargetParent;
                    connector.getEnds().get(1).setPartWithPort(targetProperty);
                }
            }

            return super.caseConnector(connector);
        }

        /**
         * Copied from
         * org.eclipse.papyrus.uml.service.types.helper.ControlFlowEditHelper#getConfigureCommand
         */
        @Override
        public EObject caseControlFlow(ControlFlow controlFlow) {

            if (this.source instanceof ActivityNode) {
                controlFlow.setSource((ActivityNode) this.source);
            }
            if (this.target instanceof ActivityNode) {
                controlFlow.setTarget((ActivityNode) this.target);
            }

            // Manage InPartitions
            new ActivityEdgeHelper().setInPartition(controlFlow);
            return controlFlow;
        }

        @Override
        public EObject caseDependency(Dependency dependency) {
            if (this.source instanceof NamedElement) {
                dependency.getClients().add((NamedElement) this.source);
            }
            if (this.target instanceof NamedElement) {
                dependency.getSuppliers().add((NamedElement) this.target);
            }
            return super.caseDependency(dependency);
        }

        @Override
        public EObject caseDeployment(Deployment deployment) {
            if (this.source instanceof DeployedArtifact) {
                deployment.getDeployedArtifacts().add((DeployedArtifact) this.source);
            }
            if (this.target instanceof DeploymentTarget) {
                deployment.setLocation((DeploymentTarget) this.target);
            }
            return deployment;
        }

        @Override
        public EObject caseExtend(Extend extend) {
            if (this.source instanceof UseCase) {
                extend.setExtension((UseCase) this.source);
            }
            if (this.target instanceof UseCase) {
                ExtensionPoint targetExtensionpoint = UMLFactory.eINSTANCE.createExtensionPoint();

                extend.getExtensionLocations().add(targetExtensionpoint);
                ((UseCase) this.target).getExtensionPoints().add(targetExtensionpoint);
                extend.setExtendedCase((UseCase) this.target);
            }
            return extend;
        }

        /**
         * From
         * org.eclipse.papyrus.uml.service.types.internal.ui.advice.ExtensionEditHelperAdvice.
         */
        @Override
        public EObject caseExtension(Extension extension) {
            ExtensionEnd endSource = UMLFactory.eINSTANCE.createExtensionEnd();
            endSource.setName(EXTENSION + ((NamedElement) this.source).getName());
            endSource.setType((Type) this.source);
            endSource.setAggregation(AggregationKind.COMPOSITE_LITERAL);
            extension.getOwnedEnds().add(endSource);
            Property property = UMLFactory.eINSTANCE.createProperty();
            property.setName(BASE + ((NamedElement) this.target).getName());
            property.setType((Type) this.target);
            property.setAssociation(extension);
            property.setAggregation(AggregationKind.NONE_LITERAL);
            property.setLower(0);
            extension.getMemberEnds().add(property);
            ((Stereotype) this.source).getOwnedAttributes().add(property);
            return extension;
        }

        @Override
        public EObject caseGeneralization(Generalization generalization) {
            if (this.source instanceof Classifier) {
                generalization.setSpecific((Classifier) this.source);
            }
            if (this.target instanceof Classifier) {
                generalization.setGeneral((Classifier) this.target);
            }
            return super.caseGeneralization(generalization);
        }

        @Override
        public EObject caseInclude(Include include) {
            if (this.source instanceof UseCase) {
                include.setIncludingCase((UseCase) this.source);
            }
            if (this.target instanceof UseCase) {
                include.setAddition((UseCase) this.target);
            }
            return super.caseInclude(include);
        }

        @Override
        public EObject caseInformationFlow(InformationFlow informationFlow) {
            if (this.source instanceof NamedElement) {
                informationFlow.getInformationSources().add((NamedElement) this.source);
            }
            if (this.target instanceof NamedElement) {
                informationFlow.getInformationTargets().add((NamedElement) this.target);
            }

            return super.caseInformationFlow(informationFlow);
        }

        @Override
        public EObject caseInterfaceRealization(InterfaceRealization interfaceRealization) {
            if (this.source instanceof BehavioredClassifier) {
                interfaceRealization.setImplementingClassifier((BehavioredClassifier) this.source);
            }
            if (this.target instanceof Interface) {
                interfaceRealization.setContract((Interface) this.target);
            }
            return super.caseInterfaceRealization(interfaceRealization);
        }

        @Override
        public EObject caseManifestation(Manifestation manifestation) {
            if (this.source instanceof NamedElement) {
                manifestation.getClients().add((NamedElement) this.source);
            }
            if (this.target instanceof PackageableElement) {
                manifestation.setUtilizedElement((PackageableElement) this.target);
            }
            return manifestation;
        }

        /**
         * Copied from
         * org.eclipse.papyrus.uml.service.types.helper.ObjectFlowEditHelper#getConfigureCommand
         */
        @Override
        public EObject caseObjectFlow(ObjectFlow objectFlow) {
            EObject realSource = this.source;
            EObject realTarget = this.target;

            // handle the OpaqueAction case (creation of the InputPin and OutPin).
            if (this.source instanceof OpaqueAction opaqueActionSource) {
                OutputPin outpuPin = UMLFactory.eINSTANCE.createOutputPin();
                opaqueActionSource.getOutputValues().add(outpuPin);
                outpuPin.setName(new ElementDefaultNameProvider().getDefaultName(outpuPin, this.source));
                outpuPin.getInPartitions().addAll(opaqueActionSource.getInPartitions());
                realSource = outpuPin;
            }
            if (this.target instanceof OpaqueAction opaqueActionTarget) {
                InputPin inputPin = UMLFactory.eINSTANCE.createInputPin();
                opaqueActionTarget.getInputValues().add(inputPin);
                inputPin.setName(new ElementDefaultNameProvider().getDefaultName(inputPin, this.target));
                inputPin.getInPartitions().addAll(opaqueActionTarget.getInPartitions());
                realTarget = inputPin;
            }

            if (realSource instanceof ActivityNode) {
                objectFlow.setSource((ActivityNode) realSource);
            }
            if (realTarget instanceof ActivityNode) {
                objectFlow.setTarget((ActivityNode) realTarget);
            }

            // Manage InPartitions
            new ActivityEdgeHelper().setInPartition(objectFlow);

            // Set a default guard and weight.
            // See
            // org.eclipse.papyrus.uml.diagram.activity.listeners.ObjectFlowListener.getModificationCommand(Notification)
            if (objectFlow.getGuard() == null) {
                LiteralBoolean literalBoolean = UMLFactory.eINSTANCE.createLiteralBoolean();
                literalBoolean.setValue(true);
                objectFlow.setGuard(literalBoolean);
            }
            if (objectFlow.getWeight() == null) {
                LiteralInteger literalInteger = UMLFactory.eINSTANCE.createLiteralInteger();
                literalInteger.setValue(1);
                objectFlow.setWeight(literalInteger);
            }

            return objectFlow;
        }

        @Override
        public EObject casePackageImport(PackageImport object) {
            if (this.source instanceof Namespace) {
                object.setImportingNamespace((Namespace) this.source);
            }
            if (this.target instanceof Package) {
                object.setImportedPackage((Package) this.target);
            }

            return super.casePackageImport(object);
        }

        @Override
        public EObject casePackageMerge(PackageMerge object) {
            if (this.source instanceof Package) {
                object.setReceivingPackage((Package) this.source);
            }
            if (this.target instanceof Package) {
                object.setMergedPackage((Package) this.target);
            }

            return super.casePackageMerge(object);
        }

        @Override
        public EObject caseSubstitution(Substitution substitution) {
            if (this.source instanceof Classifier) {
                substitution.setSubstitutingClassifier((Classifier) this.source);
            }
            if (this.target instanceof Classifier) {
                substitution.setContract((Classifier) this.target);
            }
            return substitution;
        }

        @Override
        public EObject caseTransition(Transition transition) {
            if (this.source instanceof Vertex) {
                transition.setSource((Vertex) this.source);
            }
            if (this.target instanceof Vertex) {
                transition.setTarget((Vertex) this.target);
            }
            return super.caseTransition(transition);
        }

        @Override
        public EObject defaultCase(EObject object) {
            return object;
        }
    }
}
