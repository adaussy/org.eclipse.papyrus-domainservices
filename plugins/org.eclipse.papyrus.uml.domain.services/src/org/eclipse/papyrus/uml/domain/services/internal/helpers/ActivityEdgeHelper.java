/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.internal.helpers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeSourceProvider;
import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeTargetsProvider;
import org.eclipse.uml2.uml.Action;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.StructuredActivityNode;

/**
 * Helper dedicated to the {@link ActivityEdge} kind concept. Copied from
 * org.eclipse.papyrus.uml.tools.utils.ActivityEdgeUtil.
 *
 * @author <a href="mailto:florian.barbin@obeo.fr">Florian Barbin</a>
 */
public class ActivityEdgeHelper {

    /**
     * Deduce the container of the ActivityEgde The first StructuredActivityNode or
     * the first Activity (16.14.55.6 in [UML 2.5]).
     *
     * @param source
     *               the edge source candidate.
     * @param target
     *               the edge target candidate.
     * @return the deduce container.
     */
    public Element deduceContainer(EObject source, EObject target) {
        Element container = null;
        // Find container element for the new link.
        // Climb up by containment hierarchy starting from the source
        // and return the first element that is instance of the container class.
        for (EObject element = source; element != null && container == null; element = element.eContainer()) {
            if (element instanceof Activity) {
                container = (Activity) element;
            } else if (element instanceof StructuredActivityNode) {
                if (((StructuredActivityNode) element).allOwnedElements().contains(target)) {
                    container = (StructuredActivityNode) element;
                }
            }
        }
        return container;
    }

    /**
     * Computes and sets the InPartition value of the given {@link ActivityEdge}.
     *
     * @param activityEdge
     *                     the {@link ActivityEdge}.
     */
    public void setInPartition(ActivityEdge activityEdge) {
        ActivityNode source = activityEdge.getSource();
        ActivityNode target = activityEdge.getTarget();
        if (source != null && target != null) {

            // Get the source InPartitions
            final List<ActivityPartition> partitions = new ArrayList<>(source.getInPartitions());
            // Keep the InPartitions contained in the source and target to add it into the
            // ActivityEdge
            partitions.retainAll(target.getInPartitions());

            // If the feature was previously set, we need to clear the old list.
            if (!activityEdge.getInPartitions().isEmpty()) {
                activityEdge.getInPartitions().clear();
            }
            activityEdge.getInPartitions().addAll(partitions);
        }
    }

    /**
     * Gets all activity edges associated to an ActivityNode. ActivityEdges linked
     * to the Pins of the specified ActivityNode are also returned.
     *
     * @param activityNode
     *                     the node used directly or indirectly as source or target.
     * @return the set of activity edges associated to the specified ActivityNode.
     */
    public Set<ActivityEdge> getAllActivityEdgesFromActivityNode(ActivityNode activityNode) {
        Set<ActivityEdge> allEdges = new HashSet<ActivityEdge>(activityNode.getIncomings());
        allEdges.addAll(activityNode.getOutgoings());
        allEdges.addAll(this.getActivityEdgeFromPin(activityNode));
        return allEdges;
    }

    /**
     * Updates the container of the provided {@code activityEdge}.
     * <p>
     * This method looks at the source and target of the edge to find which element
     * in the containment tree should own the edge.
     * </p>
     * <p>
     * This method is <b>not</b> copied from
     * org.eclipse.papyrus.uml.tools.utils.ActivityEdgeUtil.
     * </p>
     *
     * @param activityEdge
     *                     the activityEdge to update.
     *
     * @see #deduceContainer(EObject, EObject)
     */
    public void updateActivityEdgeContainer(ActivityEdge activityEdge) {
        ElementDomainBasedEdgeSourceProvider sourceProvider = new ElementDomainBasedEdgeSourceProvider();
        ElementDomainBasedEdgeTargetsProvider targetsProvider = new ElementDomainBasedEdgeTargetsProvider();
        EObject source = sourceProvider.getSource(activityEdge);
        Optional<? extends EObject> optTarget = targetsProvider.getTargets(activityEdge).stream().findFirst();
        if (optTarget.isPresent()) {
            EObject target = optTarget.get();
            Element newActivityEdgeContainer = this.deduceContainer(source, target);
            if (newActivityEdgeContainer != activityEdge.getOwner()) {
                if (newActivityEdgeContainer instanceof Activity) {
                    Activity activityContainer = (Activity) newActivityEdgeContainer;
                    activityContainer.getEdges().add(activityEdge);
                } else if (newActivityEdgeContainer instanceof StructuredActivityNode) {
                    StructuredActivityNode structuredActivityNode = (StructuredActivityNode) newActivityEdgeContainer;
                    structuredActivityNode.getEdges().add(activityEdge);
                }
            }
        }
    }

    /**
     * Gets edges linked to the Pins of an ActivityNode.
     *
     * @param activityNode
     *                     the activityNode which contains Pins
     * @return edges linked to the Pins of the ActivityNode
     */
    private Set<ActivityEdge> getActivityEdgeFromPin(ActivityNode activityNode) {
        Set<ActivityEdge> edgesFromPins = new HashSet<ActivityEdge>();
        if (activityNode instanceof Action) {
            // Action elements don't always own ActivityEdges. In the case of an ObjectFlow
            // edge, this one is referenced by the InputPin and OutputPin of its source and
            // target. So we need to use incomings/outgoings features from these
            // input/output pins to retrieve ActivityEdges.
            Action action = (Action) activityNode;
            List<OutputPin> potentialEdgeSources = action.getOutputs();
            List<InputPin> potentialEdgeTargets = action.getInputs();
            for (OutputPin outputPin : potentialEdgeSources) {
                edgesFromPins.addAll(outputPin.getIncomings());
                edgesFromPins.addAll(outputPin.getOutgoings());
            }
            for (InputPin inputPin : potentialEdgeTargets) {
                edgesFromPins.addAll(inputPin.getIncomings());
                edgesFromPins.addAll(inputPin.getOutgoings());
            }
        }
        return edgesFromPins;
    }
}
