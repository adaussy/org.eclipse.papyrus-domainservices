/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.internal.helpers;

import java.util.Optional;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Action;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.ActivityParameterNode;
import org.eclipse.uml2.uml.DecisionNode;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.FinalNode;
import org.eclipse.uml2.uml.ForkNode;
import org.eclipse.uml2.uml.InitialNode;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.JoinNode;
import org.eclipse.uml2.uml.MergeNode;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.OpaqueAction;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.Pin;
import org.eclipse.uml2.uml.StructuredActivityNode;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Helper dedicated to the {@link ObjectFlow} concept. Mainly copied from
 * org.eclipse.papyrus.uml.tools.utils.ObjectFlowUtil
 * 
 * @author <a href="mailto:florian.barbin@obeo.fr">Florian Barbin</a>
 */
public class ObjectFlowHelper {

    /**
     * Checks if we can Create an {@link ObjectFlow} between the given source and
     * target.
     * 
     * @param semanticEdgeSource
     *                           the candidate edge source.
     * @param semanticEdgeTarget
     *                           the candidate edge target.
     * @return true if we can create the {@link ObjectFlow}, false otherwise.
     */
    public boolean canCreateObjectFlow(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        if (semanticEdgeSource instanceof ActivityNode && semanticEdgeTarget instanceof ActivityNode) {
            if (canCreateFromSource((ActivityNode) semanticEdgeSource, (ActivityNode) semanticEdgeTarget)) {
                return canCreateToTarget((ActivityNode) semanticEdgeSource, (ActivityNode) semanticEdgeTarget);
            }
        }
        return false;
    }

    /**
     * Checks if we can Create an {@link ObjectFlow} from the given source to the
     * target (optional).
     * 
     * @param source
     *               the source {@link ActivityNode}.
     * @param target
     *               the target {@link ActivityNode} (optional)
     * @return true if we can create the {@link ObjectFlow}, false otherwise.
     */
    public boolean canCreateFromSource(ActivityNode source, ActivityNode target) {
        boolean canCreate = true;
        if (source instanceof Action) {
            // rule validateObjectFlow_validateNoActions
            // rule workaround by addition of pins in case of Action
            if (!canStartNewObjectFlowByAddingPin((Action) source)) {
                canCreate = false;
            }
        } else if (source instanceof InputPin) {
            // rule validateInputPin_validateOutgoingEdgesStructuredOnly
            if (isStructuredActivityNode(source.getOwner())) {
                if (target != null
                        && !isPinOwnedByStructuredActivityNode(target, (StructuredActivityNode) source.getOwner())) {
                    canCreate = false;
                }
            } else {
                canCreate = false;
            }
        } else if (source instanceof InitialNode) {
            // rule validateInitialNode_validateControlEdges
            canCreate = false;
        } else if (source instanceof FinalNode) {
            // rule validateFinalNode_validateNoOutgoingEdges
            canCreate = false;
        } else if (source instanceof JoinNode) {
            // rule validateJoinNode_validateOneOutgoingEdge
            if (!source.getOutgoings().isEmpty()) {
                canCreate = false;
            }
            /*
             * rule validateJoinNode_validateIncomingObjectFlow : We do not prevent creation
             * of an outgoing ObjectFlow even if there is no incoming ObjectFlow. We let the
             * possibility that the user intends to add an incoming ObjectFlow later.
             */
        } else if (source instanceof ForkNode) {
            // rule validateForkNode_validateEdges on source Fork node
            canCreate = checkNoIncomingOrOutgoingControlFlow(source);
        } else if (source instanceof MergeNode) {
            // rule validateMergeNode_validateOneOutgoingEdge
            if (!source.getOutgoings().isEmpty()) {
                canCreate = false;
            } else {
                canCreate = checkNoIncomingOrOutgoingControlFlow(source);
            }
        } else if (source instanceof DecisionNode) {
            canCreate = checkNoIncomingOrOutgoingControlFlow(source);
        } else if (source instanceof ActivityParameterNode) {
            // rule validateActivityParameterNode_validateIncomingOrOutgoing
            EList<ActivityEdge> incomings = source.getIncomings();
            if (!incomings.isEmpty()) {
                canCreate = false;
            }
        }
        return canCreate;
    }

    private boolean checkNoIncomingOrOutgoingControlFlow(ActivityNode source) {
        ActivityEdge outgoingControlFlow = source.getOutgoing(null, true, UMLPackage.eINSTANCE.getControlFlow());
        ActivityEdge incomingControlFlow = source.getIncoming(null, true, UMLPackage.eINSTANCE.getControlFlow());
        return outgoingControlFlow == null && incomingControlFlow == null;
    }

    private boolean canCreateToTarget(ActivityNode source, ActivityNode target) {
        boolean canCreate = true;
        if (target instanceof Action) {
            // rule validateObjectFlow_validateNoActions
            // rule workaround by addition of pins in case of Action
            if (!canEndNewObjectFlowByAddingPin((Action) target)) {
                canCreate = false;
            }
        } else if (target instanceof OutputPin) {
            // rule validateOutputPin_validateIncomingEdgesStructuredOnly
            if (isStructuredActivityNode(target.getOwner())) {
                if (source != null
                        && !isPinOwnedByStructuredActivityNode(source, (StructuredActivityNode) target.getOwner())) {
                    canCreate = false;
                }
            } else {
                canCreate = false;
            }
        } else if (target instanceof InitialNode) {
            // rule validateInitialNode_validateNoIncomingEdges
            canCreate = false;
        } else if (target instanceof JoinNode) {
            // rule validateJoinNode_validateIncomingObjectFlow
            ActivityEdge outgoingControlFlow = target.getOutgoing(null, true, UMLPackage.eINSTANCE.getControlFlow());
            if (outgoingControlFlow != null) {
                // the outgoing edge is a ControlFlow which means there must be no incoming
                // ObjectFlow
                canCreate = false;
            }
        } else if (target instanceof ForkNode) {
            // rule validateForkNode_validateOneIncomingEdge
            if (!target.getIncomings().isEmpty()) {
                canCreate = false;
            } else {
                canCreate = checkNoIncomingOrOutgoingControlFlow(target);
            }
        } else if (target instanceof MergeNode) {
            canCreate = checkNoIncomingOrOutgoingControlFlow(target);
        } else if (target instanceof DecisionNode) {
            // rule validateDecisionNode_validateIncomingOutgoingEdges
            if (target.getIncomings().size() >= 2) {
                // no more than two incoming edges
                canCreate = false;
            } else {
                // rule validateDecisionNode_validateEdges on target Decision node
                ActivityEdge outgoingControlFlow = target.getOutgoing(null, true,
                        UMLPackage.eINSTANCE.getControlFlow());
                ActivityEdge incomingControlFlow = target.getIncoming(null, true,
                        UMLPackage.eINSTANCE.getControlFlow());
                if (outgoingControlFlow != null || incomingControlFlow != null) {
                    /*
                     * There is a ControlFlow which means there must be no ObjectFlow but the
                     * decision flow itself. We let the user insert up to one ObjectFlow for being
                     * able to select the decision flow among existing input flows.
                     */
                    if (target.getIncoming(null, true, UMLPackage.eINSTANCE.getObjectFlow()) != null) {
                        // there is already an object flow which is intended to become the decision flow
                        canCreate = false;
                    }
                }
            }
        } else if (target instanceof ActivityParameterNode) {
            // rule validateActivityParameterNode_validateIncomingOrOutgoing
            EList<ActivityEdge> outgoings = target.getOutgoings();
            if (!outgoings.isEmpty()) {
                canCreate = false;
            }
        }
        return canCreate;
    }

    private boolean isStructuredActivityNode(Element owner) {
        return Optional.ofNullable(owner)//
                .filter(element -> UMLPackage.eINSTANCE.getStructuredActivityNode().isInstance(element))//
                .isPresent();
    }

    /**
     * Check if the pin is directly or indirectly owned by the
     * structuredActivityNode
     */
    private boolean isPinOwnedByStructuredActivityNode(ActivityNode pin, StructuredActivityNode owner) {
        if (pin instanceof Pin) {
            EList<ActivityNode> nodes = owner.allOwnedNodes();
            if (nodes.contains(pin)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return whether we can create a new object flow starting from this action by
     * adding an intermediate output pin at it.
     *
     * @param action
     *               the action to start from
     * @return whether action accept new output pin
     */
    private boolean canStartNewObjectFlowByAddingPin(Action action) {
        // CallOperationAction and CallBehaviorAction have pre-defined pins which are
        // all represented.
        // SendObjectAction have only two input pins which are all represented.
        // SendSignalAction has only pre-defined input pins which are all represented.
        return action instanceof OpaqueAction;
    }

    /**
     * Return whether we can create a new object flow ending to this action by
     * adding an intermediate input pin at it.
     *
     * @param action
     *               the action to end to
     * @return whether action accept new input pin
     */
    private boolean canEndNewObjectFlowByAddingPin(Action action) {
        // CallOperationAction and CallBehaviorAction have pre-defined pins which are
        // all represented.
        // SendObjectAction have only two input pins which are all represented.
        // SendSignalAction has only pre-defined input pins which are all represented.
        // AcceptEventAction has no input pin
        return action instanceof OpaqueAction;
    }
}
