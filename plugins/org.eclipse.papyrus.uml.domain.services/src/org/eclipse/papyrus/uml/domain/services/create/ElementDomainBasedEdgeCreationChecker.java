/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.create;

import java.util.Objects;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;
import org.eclipse.papyrus.uml.domain.services.UMLHelper;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.ConnectorHelper;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.ControlFlowHelper;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.ExtensionHelper;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.ObjectFlowHelper;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.BehavioredClassifier;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.DeployedArtifact;
import org.eclipse.uml2.uml.DeploymentTarget;
import org.eclipse.uml2.uml.ExecutionSpecification;
import org.eclipse.uml2.uml.Gate;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;

/**
 * Checks if the creation of a domain-based edge is possible in a given context.
 *
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 */
public class ElementDomainBasedEdgeCreationChecker implements IDomainBasedEdgeCreationChecker {
    // We disable checkstyle because of the NCSS violation. We can't use the
    // UMLSwitch here (no EObject instance since the edge is not yet created)
    // CHECKSTYLE:OFF
    @Override
    public CheckStatus canCreate(EObject semanticEdgeSource, EObject semanticEdgeTarget, String type,
            String referenceName, IViewQuerier representationQuery, Object sourceView, Object targetView) {
        final CheckStatus result;
        switch (UMLHelper.toEClass(type).getClassifierID()) {
        case UMLPackage.ASSOCIATION:
            result = this.handleAssociation(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.COMMUNICATION_PATH:
            result = this.handleCommunicationPath(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.CONNECTOR:
            result = this.handleConnector(representationQuery, sourceView, targetView);
            break;
        case UMLPackage.DEPLOYMENT:
            result = this.handleDeployment(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.USAGE:
            result = this.handleUsage(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.GENERALIZATION:
            result = this.handleGeneralization(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.INFORMATION_FLOW:
            result = this.handleInformationFlow(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.MESSAGE:
            result = this.handleMessage(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.SUBSTITUTION:
            result = this.handleSubstitution(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.MANIFESTATION:
            result = this.handleManifestation(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.PACKAGE_MERGE:
            result = this.handlePackageMerge(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.PACKAGE_IMPORT:
            result = this.handlePackageImport(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.TRANSITION:
            result = this.handleTransition(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.INCLUDE:
            result = this.handleInclude(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.DEPENDENCY:
            result = this.handleDependency(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.EXTEND:
            result = this.handleExtend(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.INTERFACE_REALIZATION:
            result = this.handleInterfaceRealization(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.COMPONENT_REALIZATION:
            result = this.handleComponentRealization(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.EXTENSION:
            result = this.handleExtension(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.OBJECT_FLOW:
            result = this.handleObjectFlow(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.CONTROL_FLOW:
            result = this.handleControlFlow(semanticEdgeSource, semanticEdgeTarget);
            break;
        default:
            result = CheckStatus.YES;
            break;
        }
        return result;
    }

    // CHECKSTYLE:ON
    @Override
    public CheckStatus canCreateFromSource(EObject semanticEdgeSource, String type, String referenceName,
            IViewQuerier representionQuery, Object sourceView) {
        final CheckStatus result;
        switch (UMLHelper.toEClass(type).getClassifierID()) {
        case UMLPackage.OBJECT_FLOW:
            result = this.handleObjectFlowSource(semanticEdgeSource);
            break;
        case UMLPackage.CONTROL_FLOW:
            result = this.handleControlFlowSource(semanticEdgeSource);
            break;
        default:
            result = CheckStatus.YES;
            break;
        }
        return result;
    }

    private CheckStatus handleAssociation(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Classifier) || !(semanticEdgeTarget instanceof Classifier)) {
            result = CheckStatus.no("Association can only be connected to Classifiers.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleCommunicationPath(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Classifier) || !(semanticEdgeSource instanceof DeploymentTarget)) {
            result = CheckStatus.no("The source of a CommunicationPath should be a DeploymentTarget and a Classifier");
        } else if (!(semanticEdgeTarget instanceof Classifier) || !(semanticEdgeTarget instanceof DeploymentTarget)) {
            result = CheckStatus.no("The target of a CommunicationPath should be a DeploymentTarget and a Classifier");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleComponentRealization(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Classifier)) {
            result = CheckStatus.no("The source of a ComponentRealization should be a Classifier");
        } else if (!(semanticEdgeTarget instanceof Component)) {
            result = CheckStatus.no("The target of a ComponentRealization should be a Component");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("The source and target of a ComponentRealization must be different");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleConnector(IViewQuerier representationQuery, Object sourceView, Object targetView) {
        ConnectorHelper connectorHelper = new ConnectorHelper();
        return connectorHelper.canCreateConnector(representationQuery, sourceView, targetView);
    }

    private CheckStatus handleControlFlow(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        if (new ControlFlowHelper().canCreateControlFlow(semanticEdgeSource, semanticEdgeTarget)) {
            return CheckStatus.YES;
        }
        return CheckStatus
                .no("A ControlFlow can only be connected between ActivityNode or ObjectNode with a Control Type.");
    }

    private CheckStatus handleControlFlowSource(EObject semanticEdgeSource) {
        if (new ControlFlowHelper().canCreateFromSource(semanticEdgeSource)) {
            return CheckStatus.YES;
        }
        return CheckStatus
                .no("A ControlFlow can only be connected from ActivityNode or ObjectNode with a Control type.");
    }

    private CheckStatus handleDependency(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof NamedElement) || !(semanticEdgeTarget instanceof NamedElement)) {
            result = CheckStatus.no("Dependency can only be connected to NamedElement.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleDeployment(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        if (!(semanticEdgeSource instanceof DeployedArtifact) || !(semanticEdgeTarget instanceof DeploymentTarget)) {
            result = CheckStatus
                    .no("Deployment can only be connected to DeployedArtifact source and DeploymentTarget target.");
        } else if (Objects.equals(semanticEdgeSource, semanticEdgeTarget)) {
            result = CheckStatus.no("The source and target of a Deployment must be different.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleExtend(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        if (!(semanticEdgeSource instanceof UseCase) || !(semanticEdgeTarget instanceof UseCase)) {
            result = CheckStatus.no("Extend can only be connected between Use Cases.");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("Extend cannot use the same element for source and target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleExtension(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        ExtensionHelper extensionHelper = new ExtensionHelper();
        if (!extensionHelper.canCreate(semanticEdgeSource, semanticEdgeTarget)) {
            result = CheckStatus.no("Extension can only be connected between a Stereotype and a Metaclass.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleGeneralization(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Classifier) || !(semanticEdgeTarget instanceof Classifier)) {
            result = CheckStatus.no("Generalization can only be connected to Classifiers.");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("Generalization cannot use the same element for source and target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleInclude(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof UseCase) || !(semanticEdgeTarget instanceof UseCase)) {
            result = CheckStatus.no("Include can only be connected between Use Cases.");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("Include cannot use the same element for source and target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleInformationFlow(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        if (!(semanticEdgeSource instanceof NamedElement) || !(semanticEdgeTarget instanceof NamedElement)) {
            return CheckStatus.no("InformationFlow can only be connected to NamedElements.");
        }
        return CheckStatus.YES;
    }

    private CheckStatus handleInterfaceRealization(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        if (!(semanticEdgeSource instanceof BehavioredClassifier)) {
            result = CheckStatus.no("The source of an InterfaceRealization should be a BehaviorClassifier");
        } else if (!(semanticEdgeTarget instanceof Interface)) {
            result = CheckStatus.no("The target of an InterfaceRealization should be an Interface");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleManifestation(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof NamedElement) || !(semanticEdgeTarget instanceof PackageableElement)) {
            result = CheckStatus.no("Manifestation can only be connected between NamedElement and PackageableElement.");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("Manifestation cannot use the same element for source and target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleMessage(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Lifeline) && !(semanticEdgeSource instanceof Interaction)
                && !(semanticEdgeSource instanceof Gate) && !(semanticEdgeSource instanceof ExecutionSpecification)) {
            result = CheckStatus.no(
                    "Message can only be connected to Lifeline or Interaction or Gate or ExecutionSpecification source.");
        } else if (!(semanticEdgeTarget instanceof Lifeline) && !(semanticEdgeTarget instanceof Interaction)
                && !(semanticEdgeTarget instanceof Gate) && !(semanticEdgeTarget instanceof ExecutionSpecification)) {
            result = CheckStatus.no(
                    "Message can only be connected to Lifeline or Interaction or Gate or ExecutionSpecification target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleObjectFlow(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        if (new ObjectFlowHelper().canCreateObjectFlow(semanticEdgeSource, semanticEdgeTarget)) {
            return CheckStatus.YES;
        }
        return CheckStatus.no("An ObjectFlow can only be connected between ActivityNode except ExecutableNode");
    }

    private CheckStatus handleObjectFlowSource(EObject semanticEdgeSource) {
        if (semanticEdgeSource instanceof ActivityNode) {
            if (new ObjectFlowHelper().canCreateFromSource((ActivityNode) semanticEdgeSource, null)) {
                return CheckStatus.YES;
            }
        }
        return CheckStatus.no("An ObjectFlow can only be connected from an ActivityNode.");
    }

    private CheckStatus handlePackageImport(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Namespace)) {
            result = CheckStatus.no("PackageImport can only be connected from a Namespace.");
        } else if (!(semanticEdgeTarget instanceof Package)) {
            result = CheckStatus.no("PackageImport can only be connected to Package.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handlePackageMerge(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Package) || !(semanticEdgeTarget instanceof Package)) {
            result = CheckStatus.no("PackageImport can only be connected to Package.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleSubstitution(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Classifier) || !(semanticEdgeTarget instanceof Classifier)) {
            result = CheckStatus.no("Substitution can only be connected to Classifiers.");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("Substitution cannot use the same element for source and target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleTransition(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        if (!(semanticEdgeSource instanceof Vertex) || !(semanticEdgeTarget instanceof Vertex)) {
            return CheckStatus.no("A Transition can only be connected to Vertex.");
        }
        return CheckStatus.YES;
    }

    private CheckStatus handleUsage(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        if (!(semanticEdgeSource instanceof NamedElement) || !(semanticEdgeTarget instanceof NamedElement)) {
            return CheckStatus.no("Usage can only be connected to NamedElements.");
        }
        return CheckStatus.YES;
    }

}
