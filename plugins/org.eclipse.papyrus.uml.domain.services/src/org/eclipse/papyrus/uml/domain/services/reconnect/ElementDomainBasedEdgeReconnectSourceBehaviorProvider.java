/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import java.text.MessageFormat;
import java.util.Objects;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;
import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeContainerProvider;
import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeInitializer;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.ActivityEdgeHelper;
import org.eclipse.papyrus.uml.domain.services.labels.ElementDefaultNameProvider;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.BehavioredClassifier;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.ComponentRealization;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.DeployedArtifact;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.ExtensionEnd;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageEnd;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Reconnect source behavior provider of a semantic element.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 *
 */
public class ElementDomainBasedEdgeReconnectSourceBehaviorProvider
        implements IDomainBasedEdgeReconnectSourceBehaviorProvider {

    private final IEditableChecker editableChecker;

    private final IViewQuerier representationQuery;

    public ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker editableChecker,
            IViewQuerier representationQuery) {
        super();
        this.editableChecker = Objects.requireNonNull(editableChecker);
        this.representationQuery = Objects.requireNonNull(representationQuery);
    }

    @Override
    public CheckStatus reconnectSource(EObject elementToReconnect, EObject oldSource, EObject newSource,
            Object newSourceView) {
        if (elementToReconnect == null || oldSource == null || newSource == null) {
            return CheckStatus.no(
                    MessageFormat.format("Invalid input for reconnexion (element ={0} oldSource ={1} newSource = {2})", //$NON-NLS-1$
                            elementToReconnect, oldSource, newSource));
        }
        return new ReconnectSourceBehaviorProviderSwitch(oldSource, newSource, newSourceView, this.editableChecker,
                this.representationQuery).doSwitch(elementToReconnect);
    }

    static class ReconnectSourceBehaviorProviderSwitch extends UMLSwitch<CheckStatus> {

        private final EObject oldSource;

        private final EObject newSource;

        private final IEditableChecker editableChecker;

        private final IViewQuerier representationQuery;

        private final Object newSourceView;

        ReconnectSourceBehaviorProviderSwitch(EObject oldSource, EObject newSource, Object newSourceView,
                IEditableChecker editableChecker, IViewQuerier representationQuery) {
            super();
            this.oldSource = oldSource;
            this.newSource = newSource;
            this.newSourceView = newSourceView;
            this.editableChecker = editableChecker;
            this.representationQuery = representationQuery;
        }

        @Override
        public CheckStatus caseActivityEdge(ActivityEdge activityEdge) {
            activityEdge.setSource((ActivityNode) this.newSource);
            ActivityEdgeHelper activityEdgeHelper = new ActivityEdgeHelper();
            activityEdgeHelper.updateActivityEdgeContainer(activityEdge);
            activityEdgeHelper.setInPartition(activityEdge);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseAssociation(Association association) {
            Classifier newType = (Classifier) this.newSource;
            Property targetProperty = association.getMemberEnds().get(1);
            targetProperty.setType(newType);

            // The reconnect of source also results in the modification of the semantic
            // source of the association (if not owned by the association) the new parent of
            // the semantic source has to be the new graphical source.
            Property sourceProperty = association.getMemberEnds().get(0);
            if (!association.getOwnedEnds().contains(sourceProperty)) {
                if (this.newSource instanceof StructuredClassifier) {
                    // Move opposite member end (sourceProperty) to its target container
                    ((StructuredClassifier) this.newSource).getOwnedAttributes().add(sourceProperty);
                }
            }
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseComponentRealization(ComponentRealization componentRealization) {
            // Remove the oldSource from the clients (which also removes it from the
            // realizingClassifiers)
            componentRealization.getClients().remove(this.oldSource);
            // Type check done in the reconnection source checker
            Classifier newClassifier = (Classifier) this.newSource;
            componentRealization.getRealizingClassifiers().add(newClassifier);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseConnector(Connector connector) {
            Object sourceVisualParent = this.representationQuery.getVisualParent(this.newSourceView);
            if (sourceVisualParent != null) {
                Object visualSourceSementicParent = this.representationQuery.getSemanticElement(sourceVisualParent);
                ConnectorEnd connectorEnd = connector.getEnds().get(0);
                connectorEnd.setRole((ConnectableElement) this.newSource);
                if (visualSourceSementicParent instanceof Property) {
                    Property sourceProperty = (Property) visualSourceSementicParent;
                    connectorEnd.setPartWithPort(sourceProperty);
                } else {
                    connectorEnd.setPartWithPort(null);
                }
                return CheckStatus.YES;
            }
            return super.caseConnector(connector);
        }

        @Override
        public CheckStatus caseDependency(Dependency dependency) {

            // 1. change the client of the dependency
            dependency.getClients().remove(this.oldSource);
            // Cast check done in Checker
            dependency.getClients().add((NamedElement) this.newSource);

            // 2. the dependency is owned by the nearest package of the source
            ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                    this.editableChecker);
            EObject newOwner = containerProvider.getContainer(this.newSource, dependency.getSuppliers().get(0),
                    dependency, null, null, null);
            Element oldOwner = dependency.getOwner();
            if (oldOwner != newOwner && oldOwner instanceof Package && newOwner instanceof Package) {
                ((Package) newOwner).getPackagedElements().add(dependency);
            }

            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseDeployment(Deployment deployment) {
            // Remove the oldSource from the clients (which also removes it from the
            // deployed artifacts)
            deployment.getSuppliers().remove(this.oldSource);
            // Type check done in the reconnection source checker
            DeployedArtifact newDeployedArtifact = (DeployedArtifact) this.newSource;
            deployment.getDeployedArtifacts().add(newDeployedArtifact);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseExtend(Extend extend) {
            extend.setExtension((UseCase) this.newSource);
            return CheckStatus.YES;
        }

        @Override
        // From
        // org.eclipse.papyrus.uml.service.types.ui.util.ExtensionHelper.reconnectSource(Extension,
        // Stereotype)
        public CheckStatus caseExtension(Extension extension) {
            String oldName = extension.getName();
            try {
                extension.setName(null); // We need to reset the name to force the getDefaultName to compute the
                                         // new one.
                ElementDefaultNameProvider elementDefaultNameProvider = new ElementDefaultNameProvider();
                String deducedName = elementDefaultNameProvider.getDefaultName(extension, extension.eContainer());

                Type targetType = extension.getMetaclass();
                // remove the corresponding property in the old stereotype
                if (this.oldSource instanceof Stereotype) {
                    EList<Property> attributes = ((Stereotype) this.oldSource).getOwnedAttributes();
                    for (Property property : attributes) {
                        if (property.getAssociation() == extension) {
                            // remove the property from the stereotype
                            ((Stereotype) this.oldSource).getOwnedAttributes().remove(property);
                            // remove the property from the extension
                            extension.getMemberEnds().remove(property);
                            break;
                        }
                    }
                }
                // change the ExtensionEnd name
                EList<Property> ends = extension.getOwnedEnds();
                for (Property endSource : ends) {
                    if (endSource instanceof ExtensionEnd) {
                        endSource.setName(ElementDomainBasedEdgeInitializer.EXTENSION
                                + ((NamedElement) this.newSource).getName());
                        endSource.setType((Type) this.newSource);
                        break;
                    }
                }
                // create the new source property (stereotype)
                Property property = UMLFactory.eINSTANCE.createProperty();
                property.setName(ElementDomainBasedEdgeInitializer.BASE + targetType.getName());
                property.setType(targetType);
                property.setAssociation(extension);
                property.setAggregation(AggregationKind.NONE_LITERAL);
                extension.getMemberEnds().add(property);
                ((StructuredClassifier) this.newSource).getOwnedAttributes().add(property);
                // change the extension name, if the user doesn't have rename the extension!
                if (oldName.contains(deducedName)) {
                    if (oldName.indexOf(deducedName) == 0) {
                        oldName = oldName.substring(deducedName.length());
                        try {
                            // if there is not exception, the name has not been edited by the user
                            extension.setName(
                                    elementDefaultNameProvider.getDefaultName(extension, extension.eContainer()));
                        } catch (NumberFormatException e) {
                            // do nothing
                        }
                    }
                }
            } finally {
                if (extension.getName() == null) {
                    extension.setName(oldName);
                }
            }
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseGeneralization(Generalization generalization) {
            generalization.setSpecific((Classifier) this.newSource);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseInclude(Include include) {
            include.setIncludingCase((UseCase) this.newSource);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseInformationFlow(InformationFlow informationFlow) {
            informationFlow.getInformationSources().remove(this.oldSource);
            informationFlow.getInformationSources().add((NamedElement) this.newSource);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseInterfaceRealization(InterfaceRealization interfaceRealization) {
            // Cast check done in Checker
            BehavioredClassifier newBehaviorSource = (BehavioredClassifier) this.newSource;
            interfaceRealization.setImplementingClassifier(newBehaviorSource);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseMessage(Message message) {
            MessageEnd sendEvent = message.getSendEvent();
            if (sendEvent instanceof MessageOccurrenceSpecification && this.newSource instanceof Lifeline
                    && this.oldSource instanceof Lifeline) {
                MessageOccurrenceSpecification os = (MessageOccurrenceSpecification) sendEvent;
                os.getCovereds().clear();
                os.getCovereds().add((Lifeline) this.newSource);
            }
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus casePackageImport(PackageImport packImport) {
            packImport.setImportingNamespace((Namespace) this.newSource);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus casePackageMerge(PackageMerge object) {
            object.setReceivingPackage((Package) this.newSource);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseSubstitution(Substitution substitution) {
            substitution.setSubstitutingClassifier((Classifier) this.newSource);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseTransition(Transition transition) {
            transition.setSource((Vertex) this.newSource);
            return CheckStatus.YES;
        }

    }
}
