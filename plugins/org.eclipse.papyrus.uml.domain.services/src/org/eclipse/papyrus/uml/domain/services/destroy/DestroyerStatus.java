/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.destroy;

import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.status.State;

/**
 * Status after a destroy request.
 *
 * @author lfasani
 */
public final class DestroyerStatus {

    private final State state;

    private final String message;

    private final Set<EObject> elements;

    private DestroyerStatus(State state, String message, Set<EObject> elements) {
        super();
        this.state = state;
        this.message = message;
        this.elements = elements;
    }

    public static DestroyerStatus createFailingStatus(String message, Set<EObject> elements) {
        return new DestroyerStatus(State.FAILED, message, elements);
    }

    public static DestroyerStatus createOKStatus(Set<EObject> elements) {
        return new DestroyerStatus(State.DONE, "", elements);
    }

    public State getState() {
        return this.state;
    }

    public String getMessage() {
        return this.message;
    }

    /**
     * Returns the full list of destroyed elements if the destroy succeeded or the
     * objects that can not be destroyed.
     *
     * @return an {@link EObject} or null
     */
    public Set<EObject> getElements() {
        return this.elements;
    }

}
